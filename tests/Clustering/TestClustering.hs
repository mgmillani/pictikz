module Main where

import Data.List

import Pictikz.Clustering
import Pictikz.TestUtils
import Pictikz.Elements
import qualified Pictikz.Geometry as G

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Array.Unboxed as U

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess


tests = TestList                         
  [ TestLabel "Pinned Clustering 1" $ TestCase
    ( do
      let (ci, xi, n) = numberAttributes (\i -> (length i, i)) [[0,0,0,0.94], [0,1.02,1.02,1.02]]
          (ax, _ , _) = clusterValuesWithPinned 2 0.1
                              (gridOnGridCurveClustering 1) n
                              (map (\(i,x) -> (i, Unbounded x)) xi)
                              [0,1,2]
      assertEqual "" [0,0,0,1,0,1,1,1] $ U.elems ax
    )
  , TestLabel "Sliding Window 1" $ TestCase
    ( do
      let pinned  = map (\v -> (-1, Pinned v)) [0,1,2]
          vals    = zip [0..] $ map Unbounded [0,0,0,0.94,0,1.02,1.02,1.02]
          (vals', c, clusterVals)   = slidingWindow (Unbounded 0.1) 0.55 $ pinned ++ vals
          realV   = [(i, getClusterValue v) | (i, (v,_)) <- vals', i /= -1]
          pinnedV = [ getClusterValue v | (i, (v, _)) <- vals', i == -1 ]
      assertEqual "" [(0,0), (1,0), (2,0), (4,0), (3,1), (5,1), (6,1), (7,1)] realV
      assertEqual "" [0,1,2] pinnedV
    )
  , TestLabel "Pinned Clustering 2" $ TestCase
    ( do
      let (ci, xi, n) = numberAttributes (\i -> (length i, i)) [[0,0,0,0.94], [0,1.02,1.02,1.02]]
          (ax, _ , _) = clusterValuesWithPinned 2 0.1
                              clusterValue n
                              (map (\(i,x) -> (i, Unbounded x)) xi)
                              [0,1,2]
      assertEqual "" [0,0,0,1,0,1,1,1] $ U.elems ax
    )
  , TestLabel "Pinned Clustering 3" $ TestCase
    ( do
      let (ci, xi, n) = numberAttributes (\i -> (length i, i)) [[0,0,0,0.94], [0,1.02,1.02,1.02]]
          (ax, _ , _) = clusterValuesWithPinned 2 0.1
                              previousNodeIndex n
                              (map (\(i,x) -> (i, Unbounded x)) xi)
                              [0,1,2]
      assertEqual "" [1,1,1,2,1,2,2,2] $ U.elems ax
    )
  , TestLabel "Pinned Clustering 4" $ TestCase
    ( do
      let (ci, xi, n) = numberAttributes (\i -> (length i, i)) [[0,0,0,0.94], [0,1.02,1.02,1.02]]
          (ax, _ , _) = clusterValuesWithPinned 2 0.1
                              valueIndex n
                              (map (\(i,x) -> (i, Unbounded x)) xi)
                              [0,1,2]
      assertEqual "" [0,0,0,0,0,0,0,0] $ U.elems ax
    )
  , TestLabel "Fit between pinned 1" $ TestCase
    ( do
      let realV = [(0,0), (1,0), (2,0), (4,0), (3,1), (5,1), (6,1), (7,1)]
          pinnedV = [0,1,2]
          vals = fitBetweenPinned 2 realV pinnedV
      assertEqual "" [1,1,1,1,2,2,2,2] $ map (previousNodeIndex . snd) vals
    )
  ]


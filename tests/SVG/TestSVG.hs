module Main where

import Pictikz.SVG.Input
import Pictikz.Text hiding (alignment)
import qualified Pictikz.Text as T (alignment)
import Pictikz.Elements hiding (CurveSegment(Line))
import qualified Pictikz.Elements as E (CurveSegment(Line))
import Pictikz.TestUtils
import qualified Pictikz.Geometry as Ge

import qualified Data.Map as M
import qualified Data.Array.IArray as A
import Data.Maybe

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

conf1 = defaultConfiguration

tests = TestList                          
  [ TestLabel "Attach Elements 1" $ TestCase
    ( do
      let groups = [Group [Object $ Element (Id 0 "v1") (Shape (Point (0,0)) Ge.Point) noStyle M.empty]]
          g' = attachElements 0.001 groups
      assertEqual "" groups g'
    )
  , TestLabel "Parse Path 1" $ TestCase
    ( do
      let Curve ps cs = parsePath "m 0,0 C 0,2 2,2 2,0"
      assertEqual "Points" [Point (0,0), Point (2,0)] ps
      assertEqual "Controls" [Cubic (0,2) (2,2)] cs
    )
  , TestLabel "Parse Path 2" $ TestCase
    ( do
      let Curve ps cs = parsePath "m 5,5 c 0,2 2,2 2,0"
      assertEqual "Points" [Point (5,5), Point (7,5)] ps
      assertEqual "Controls" [Cubic (5,7) (7,7)] cs
    )
  , TestLabel "Parse Path 3" $ TestCase
    ( do
      let Curve ps cs = parsePath "m 0,0 c 5,-10 5,-25 20,-30\
                                         \ 10,-5 20,0  20,10\
                                         \ 5,10  5,20  10,25"
      assertEqual "Points"
        [ Point (0,0)
        , Point (20,-30)
        , Point (40,-20)
        , Point (50,5)
        ] ps
      assertEqual "Controls"
        [ Cubic (5,-10) (5,-25)
        , Cubic (30,-35) (40,-30)
        , Cubic (45, -10) (45, 0)
        ] cs
    )
  , TestLabel "Nodes 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-01-single-vertex.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [(Element eid1 d1 st1 _)] = universeValues universe $ A.elems $ uElements universe
      assertEqual "" (1,1) (round sx, round sy)
      assertEqual "" (0,0) (round w, round h)
      assertEqual "" (Id 0 "v1") eid1
      assertEqual "" (Shape (Point (112,-112)) (Ge.Ellipse 110 110)) d1
      assertEqual "" (Just 45) (thickness st1)
    )
  , TestLabel "Shapes 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-02-shapes.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 _ _
           , Element eid2 d2 _ _
           , Element eid3 d3 _ _
           , Element eid4 d4 _ _
           , Element eid5 d5 _ _
           , Element eid6 d6 _ _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (102,102) (round sx, round sy)
      assertEqual "Size" (153,100) (round $ 100 * w, round $ 100 * h)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..5]] [eid1, eid2, eid3, eid4, eid5, eid6]
      assertEqual "Shape 1" (Shape (Point (872,-457)) (Ge.Ellipse 110 110)) d1
      assertEqual "Shape 2" (Shape (Point (1537,-450)) (Ge.Ellipse 110 110)) d2
      assertEqual "Shape 3" (Shape (Point (791,-1197)) (Ge.Rectangle 238 223)) d3
      assertEqual "Shape 4" (Shape (Point (1571,-1112)) (Ge.Ellipse 74 191)) d4
      assertEqual "Shape 5" (Shape (Point (870,-89)) (Ge.Ellipse 276 87)) d5
      assertEqual "Shape 6" (Shape (Point (2,-1222)) (Ge.Rectangle 673 189)) d6
    )
  , TestLabel "Shapes 2" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-17-lines.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 _ _
           , Element eid2 d2 _ _
           , Element eid3 d3 _ _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (17,17) (round sx, round sy)
      assertEqual "Size" (100,709) (round $ 100 * w, round $ 100 * h)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..2]] [eid1, eid2, eid3]
      assertEqual "Shape 1" (Shape (Point (893,-2267)) (Ge.Rectangle 315 315)) d1
      assertEqual "Shape 2" (Shape (Point (925,-1031)) (Ge.Rectangle 290 315)) d2
      assertEqual "Shape 3" (PolyLine [ ElementRef 1, ElementRef 0]) d3
    )
  , TestLabel "Fill 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-03-fill.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 st1 _
           , Element eid2 d2 st2 _
           , Element eid3 d3 st3 _
           , Element eid4 d4 st4 _
           , Element eid5 d5 st5 _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (1,1) (round sx, round sy)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..4]] [eid1, eid2, eid3, eid4, eid5]
      assertEqual "Positions" [Point (112,-112), Point (679,-112), Point (1245,-112), Point (1812,-112), Point (2379, -112)] (map getPos [d1,d2,d3,d4,d5])
      assertEqual "Fill 1" (Just $ NamedColor "purple") (fillColor st1)
      assertEqual "Fill 2" (Just $ NamedColor "orange") (fillColor st2)
      assertEqual "Fill 3" (Just $ NamedColor "blue") (fillColor st3)
      assertEqual "Fill 4" (Just $ NamedColor "green") (fillColor st4)
      assertEqual "Fill 5" (Just $ NamedColor "red") (fillColor st5)
    )
  , TestLabel "Stroke 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-04-stroke.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 st1 _ 
           , Element eid2 d2 st2 _ 
           , Element eid3 d3 st3 _ 
           , Element eid4 d4 st4 _ 
           , Element eid5 d5 st5 _ 
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (1,1) (round sx, round sy)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..4]] [eid1, eid2, eid3, eid4, eid5]
      assertEqual "Positions" [Point (112,-112),Point (655,-112),Point (1197,-112),Point (1740,-112),Point (2270,-112)] (map getPos [d1,d2,d3,d4,d5])
      assertEqual "Stroke 1" (Just $ NamedColor "orange") (strokeColor st1)
      assertEqual "Stroke 2" (Just $ NamedColor "gray")   (strokeColor st2)
      assertEqual "Stroke 3" (Just $ NamedColor "purple") (strokeColor st3)
      assertEqual "Stroke 4" (Just $ NamedColor "red")    (strokeColor st4)
      assertEqual "Stroke 5" (Just $ NamedColor "black")  (strokeColor st5)
    )
  , TestLabel "Thickness 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-05-thickness.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 st1 _
           , Element eid2 d2 st2 _
           , Element eid3 d3 st3 _
           , Element eid4 d4 st4 _
           , Element eid5 d5 st5 _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (1,1) (round sx, round sy)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..4]] [eid1, eid2, eid3, eid4, eid5]
      assertEqual "Positions" [Point (120,-135),Point (583,-135),Point (1047,-135),Point (1511,-135),Point (1975,-135)] (map getPos [d1,d2,d3,d4,d5])
      assertEqual "Thickness 1" (Just $ 100) (thickness st1)
      assertEqual "Thickness 2" (Just $ 200) (thickness st2)
      assertEqual "Thickness 3" (Just $ 300) (thickness st3)
      assertEqual "Thickness 4" (Just $ 400) (thickness st4)
      assertEqual "Thickness 5" (Just $ 500) (thickness st5)
    )
  , TestLabel "Normalize Segments 1" $ TestCase
    ( do
      let ps = [Point (0,0), Point (2,0)] 
          cs = [Cubic (0,2) (2,2)]
      assertEqual "Cubic" [Cubic (20, 90) (20, 90)] $ map roundSegment $ normalizeSegments id ps cs
    )
  , TestLabel "Normalize Segments 2" $ TestCase
    ( do
      let Curve ps cs = parsePath "m -57,190 c 4,-11 7,-25 19,-31\
                                             \ 8,-4  19,0  21,9\
                                             \ 3,8   5,17  9,24"
      assertEqual "Points"
        [ Point (-570, 1900)
        , Point (-380, 1590)
        , Point (-170, 1680)
        , Point (-80, 1920)]
        $ map roundCoordinate ps
      assertEqual "Absolute"
        [ Cubic (-530, 1790) (-500, 1650)
        , Cubic (-300, 1550) (-190, 1590)
        , Cubic (-140, 1760) (-120, 1850)
        ] $ map roundRawSegment cs
      assertEqual "Relative"
        [ Cubic (117, 290) (134, 153)
        , Cubic (89, 333) (92, 257)
        , Cubic (85, 69) (81, 240)
        ] $ map roundSegment $ normalizeSegments id ps cs
    )
  , TestLabel "Lines 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-06-lines.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 _ _
           , Element eid2 d2 _ _
           , Element eid3 d3 _ _
           , Element eid4 d4 _ _
           , Element eid5 d5 _ _
           , Element eid6 d6 _ _
           , Element eid7 d7 _ _
           , Element eid8 d8 _ _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (80,80) (round sx, round sy)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..7]] [eid1, eid2, eid3, eid4, eid5, eid6, eid7, eid8]
      assertEqual "Positions" [Point (-550,-2070),Point (-130,-2040),Point (570,-2040),Point (1060,-2050),Point (1600,-2050)]  (map getPos [d1,d2,d3,d4,d5])
      assertEqual "Line 1"
        (Curve [ElementRef 0, ElementRef 1]
               [Cubic (847,70) (91, 120)])
        d6
      assertEqual "Line 2"
        (Curve [ElementRef 2, ElementRef 3] [E.Line])
        d7
      assertEqual "Line 3"
        (Curve [Point (-650,-2700),Point (2060,-2680)] [E.Line])
        d8
    )
  , TestLabel "Lines 2" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-12-lines.svg"
      let universe = load svg conf1 :: Universe Double
          (ox, oy) = uOrigin universe
          (sx, sy) = uScale universe
          [  Element _ d1 _ _
           ] = A.elems $ uElements universe
          PolyLine cs1 = d1
      assertBool "Line 1" (isLine d1)
      assertEqual "Line 1"
        [Point (0,0)
        ,Point (0,-26)
        ,Point (18,-23)
        ,Point (18,-21)
        ,Point (19,-22)
        ,Point (20,-21)
        ,Point (21,-22)
        ,Point (32,-22)
        ,Point (32,-24)
        ,Point (47,-23)
        ,Point (47,-3)]
        (map (\(Point (x,y)) -> Point (round $ x*sx + ox, round $ y*sy + oy)) cs1)
    )
  , TestLabel "Dash Pattern 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-07-dash-pattern.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 _ _
           , Element eid2 d2 _ _
           , Element eid3 d3 _ _
           , Element eid4 d4 _ _
           , Element eid5 d5 _ _
           , Element eid6 _ st6 _
           , Element eid7 _ st7 _
           , Element eid8 _ st8 _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Scale" (79,79) (round sx, round sy)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..7]] [eid1, eid2, eid3, eid4, eid5, eid6, eid7, eid8]
      assertEqual "Positions"
        [Point (130,-496)
        ,Point (543,-466)
        ,Point (1251,-462)
        ,Point (1744,-477)
        ,Point (2287,-477)] (map getPos [d1,d2,d3,d4,d5])
      assertEqual "Line 1" (Just [26,318])  (dashPattern $ st6)
      assertEqual "Line 2" (Just [26,79])   (dashPattern $ st7)
      assertEqual "Line 3" (Just [159,159]) (dashPattern $ st8)
    )
  , TestLabel "Text 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-08-text.svg"
      let universe = load svg conf1
          (sx, sy) = uScale universe
          [  Element eid1 d1 _ _
           , Element eid2 d2 _ _
           , Element eid3 d3 _ _
           , Element eid4 d4 _ _
           , Element eid5 d5 _ _
           , Element eid6 d6 _ _
           , Element eid7 d7 _ _
           ] = universeValues universe $ A.elems $ uElements universe
      let [ Paragraph _ t1
           ,Paragraph _ t2
           ,Paragraph _ t3
           ,Paragraph _ _
           ,Paragraph _ _
           ,Paragraph _ _
           ,Paragraph _ _
           ] = [d1,d2,d3,d4,d5,d6,d7]
      let ft = Format
            {T.alignment = Just LeftAligned
            , bold = Just False
            , color = Just ""
            , italics = Just False
            , position = Just Normalscript}
      assertEqual "Scale" (113,113) (round sx, round sy)
      assertEqual "Indices"   [Id i ("v" ++ show (i+1)) | i <- [0..6]] [eid1, eid2, eid3, eid4, eid5, eid6, eid7]
      assertEqual "Positions"
        [Point (375,-80)
        ,Point (371,-349)
        ,Point (288,-950)
        ,Point (-9,-1214)
        ,Point (1380,-1097)
        ,Point (1676,-628)
        ,Point (2007,-110)] (map getPos [d1,d2,d3,d4,d5,d6,d7])
      assertEqual "Text 1" [Text "word" ft]  t1
      assertEqual "Text 2" [Text "multi" ft, Text "\nline" ft, Text "\ntext" ft] t2
      assertEqual "Text 3"
                  [Text "r" ft{color = Just "red"}
                  ,Text "a" ft{color = Just "green"}
                  ,Text "i" ft{color = Just "blue"}
                  ,Text "n" ft{color = Just "purple"}
                  ,Text "b" ft{color = Just "yellow"}
                  ,Text "o" ft{color = Just "orange"}
                  ,Text "w" ft{color = Just "cyan"}
                  ] t3
    )
  , TestLabel "Label 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-09-edge-label.svg"
      let universe = load svg conf1
          --(sx, sy) = uScale universe
          --(w, h) = uSize universe
          els = universeValues universe $ A.elems $ uElements universe
          [  Element _ _ st1 _
           , Element _ _ st2 _
           , Element _ _ st3 _
           ] = els
      let ft = Format
            {T.alignment = Just LeftAligned
            , bold = Just False
            , color = Just ""
            , italics = Just False
            , position = Just Normalscript}
      assertEqual "Positions"  [Point (318,-620),Point (1580,-604),ElementRef 0] (map getPos els)
      assertEqual "Indices"   [Id i ("v" ++ show (i+1)) | i <- [0..2]] $ map getId els
      assertEqual "Label 1" (Just $ [[Text "$v_1$" ft ]]) $ label st1
      assertEqual "Label 2" (Just $ [[Text "$v_2$" ft ]]) $ label st2
      assertEqual "Label 3" (Just $ [[Text "$e_1$" ft ]]) $ label st3
    )
  , TestLabel "Label 2" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-18-vertex-labels.svg"
      let universe = load svg conf1
          els = universeValues universe $ A.elems $ uElements universe
          [  Element _ _ st1 _
           , Element _ _ st2 _
           , Element _ _ st3 _
           ] = els
      let ft = Format
            {T.alignment = Just LeftAligned
            , bold = Just False
            , color = Just ""
            , italics = Just False
            , position = Just Normalscript}
      assertEqual "Positions"
        [ Point (89,-825)
        , Point (101,-209)
        , Point (736,-194) ] (map getPos els)
      assertEqual "Indices"   [Id i ("v" ++ show (i+1)) | i <- [0..2]] $ map getId els
      assertEqual "Label 1" (Just $ [[Text "$v_1$" ft ]]) $ label st1
      assertEqual "Label Position 1" ([ Around 1.7 6.2 ]) $ map roundPosition $ fromMaybe [] $ labelPosition st1
      assertEqual "Label 2" (Just $ [[Text "$v_2$" ft ]]) $ label st2
      assertEqual "Label Position 2" ([ Around 1.6 1.6 ]) $ map roundPosition $ fromMaybe [] $ labelPosition st2
      assertEqual "Label 3" (Just $ [[Text "$v_3$" ft ]]) $ label st3
      assertEqual "Label Position 3" ([ Around 0.1 1.6 ]) $ map roundPosition $ fromMaybe [] $ labelPosition st3
    )
  , TestLabel "Simplify 1" $ TestCase
    ( do
      let makeObject d = Object $ Element (Id 0 "") (d :: Drawing Double) noStyle M.empty
      let line1 = PolyLine [Point (0,0), Point (0.5,0), Point (1.0,0)]
          [ Object (Element _ (PolyLine cs1) _ _ )] = simplifyLines 1 (Just 1) Nothing [makeObject line1]
      assertEqual "Line 1"
        [Point (0,0), Point (1.0, 0)] $
        cs1
      let line2 = PolyLine [Point (0,0), Point (0.5,0), Point (1.5,0)]
          [Object (Element _ (PolyLine cs2) _ _)] = simplifyLines 1 (Just 1) Nothing [makeObject line2]
      assertEqual "Line 2"
        [Point (0,0), Point (1.5, 0)] $
        cs2
      let line3 = PolyLine [Point (0,0), Point (0.5,0), Point (1.5,0.0), Point (2, 0) ]
          [Object (Element _ (PolyLine cs3) _ _)] = simplifyLines 1 (Just 1) Nothing [makeObject line3]
      assertEqual "Line 3"
        [Point (0,0), Point (1.0, 0), Point (2.0, 0)] $
        cs3
      let line4 = PolyLine
            [ Point (0,0)
            , Point (0.5,0)
            , Point (1.5,0.0)
            , Point (2, 0)
            , Point (2.5, 0)
            , Point (3.5, 0)]
          [Object (Element _ (PolyLine cs4) _ _)] = simplifyLines 1 (Just 1) Nothing [makeObject line4]
      assertEqual "Line 4"
        [Point (0,0), Point (1.0, 0), Point (2.0, 0), Point (3.5, 0)]
        cs4
      let line5 = PolyLine [Point (0,0), Point (0.5,0), Point (1.0,0.0) ]
          [Object (Element _ (PolyLine cs5) _ _)] = simplifyLines 1 Nothing (Just $ pi/2) [makeObject line5]
      assertEqual "Line 5"
        [Point (0,0), Point (1.0, 0)] $
        cs5
      let line6 = PolyLine [Point (0,0), Point (0.5, 0),  Point (1.0,0.5) ]
          [Object (Element _ (PolyLine cs6) _ _)] = simplifyLines 1 Nothing (Just $ pi/2) [makeObject line6]
      assertEqual "Line 6"
        [Point (0,0), Point (1.0, 0.5)] $
        cs6
      let line7 = PolyLine [Point (0,0), Point (0.5, 0),  Point (1.0,0.5)  ]
          [Object (Element _ (PolyLine cs7) _ _)] = simplifyLines 1 (Just 1) Nothing [makeObject line7]
      assertEqual "Line 6"
        [Point (0,0), Point (1.0, 0.5)] $
        cs7
    )
  , TestLabel "Simplify 2" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-13-curve.svg"
      let universeRaw = load svg conf1{confLineMinLength = Nothing, confLineMaxAngle = Nothing }
          universe    = load svg conf1{confLineMinLength = Just 5, confLineMaxAngle = Nothing }
          elsRaw = A.elems $ uElements universeRaw
          els    = A.elems $ uElements universe
          [  Element _ d1Raw _ _
           ] = elsRaw
          [  Element _ d1 _ _
           ] = els
          PolyLine csRaw = d1Raw
          PolyLine cs1 = d1
      assertBool "Is Line 1"     (isLine d1)
      assertBool "Is Line 1 Raw" (isLine d1Raw)
      assertEqual "Line 1 Raw"
        [Point (35,-86)
        ,Point (12,-59)
        ,Point (10,-48)
        ,Point (21,-37)
        ,Point (31,-38)
        ,Point (42,-20)
        ,Point (36,-14)
        ,Point (11,-14)
        ,Point (2,-8)
        ,Point (1,-3)] $
        map (roundCoordinate . universeCoordinate universe) csRaw
      assertEqual "Line 1"
        [Point (36,-86),Point (42,-20),Point (1,0)]
        $ map (roundCoordinate . universeCoordinate universe) cs1
    )
  , TestLabel "Simplify 3" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-10-simplify-square.svg"
      let universe = load svg conf1{confLineMinLength = Just 1.5, confLineMaxAngle = Just (130*pi/180) }
          els = universeValues (universe :: Universe Double) $ A.elems $ uElements universe :: [Element Integer]
          [  Element _ d1 _ _
           , Element _ d2 _ _
           , Element _ d3 _ _
           ] = els
      assertEqual "Positions"
        [Point (111,-352)
        ,Point (652,-1032)
        ,Point (916,0)] (map getPos els)
      assertEqual "Indices"   [Id i ("v" ++ show (i+1)) | i <- [0..2]] $ map getId els
      assertEqual "Line 1"
        (PolyLine [Point (111,-352)
        ,Point (125,-1507)
        ,Point (1241,-1531)
        ,Point (1224,-364)
        ,Point (183,-359)]) d1
      assertEqual "Line 2"
        (PolyLine [Point (652,-1032)
        ,Point (701,-1485)
        ,Point (1096,-1418)
        ,Point (1139,-1036)
        ,Point (701,-1009)]) d2
      assertEqual "Line 3"
        (PolyLine [Point (916,0)
        ,Point (833,-536)
        ,Point (1497,-571)
        ,Point (1525,-31)
        ,Point (931,-53)]) d3
    )
  , TestLabel "Simplify 4" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-22-curve.svg"
      let universe = load svg conf1{confLineMinLength = Just 1.5, confLineMaxAngle = Just (130*pi/180) }
          els = universeValues (universe :: Universe Double) $ A.elems $ uElements universe :: [Element Integer]
          [  Element _ d1 _ _ ] = els
      assertEqual "Line 1"
        (Curve
          [Point (0,0)
          ,Point (700,10)]
          [Cubic (595, 270) (329, 90) ]
        ) d1
    )
  , TestLabel "Simplify 4a" $ TestCase
    ( do
      let c0 = Curve
                  [ Point (0.0 :: Double,0.0)
                  , Point (10.0,20.0)
                  , Point (35.0,9.5)
                  , Point (60.0,-6.0)
                  , Point (70.0,-1.0)]
                  [ Cubic (5.0e-2,15.0) (4.5,20.0)
                  , Cubic (15.5,20.5) (25.5,14.5)
                  , Cubic (44.0,4.0) (50.5,-1.0)
                  , Cubic (65.5,-11.0) (70.0,-11.0)] -- Cubic in absolute coordinates
          d0 = roundRawDrawing $ simplifyCurve 1 c0
      assertEqual "Line 1"
        (Curve [Point (0,0),Point (700,-10)] [Cubic (2,595) (700,-339)]
        ) d0
    )
  , TestLabel "Text 2" $ TestCase
    ( do
      let conf' = conf1{confTextAsNodes = Just True, confRenameNodes = Just False}
          objs =
            [ Group
              [ Object $ Element (Id 0 "e0") (PolyLine []) noStyle M.empty
              , Object $ Element (Id 1 "e1") (PolyLine []) noStyle M.empty
              , Object $ Element (Id 2 "t0") (Paragraph (Point (0, 0)) []) noStyle M.empty
              ]
            ]
          objs' = preprocess conf' 1 $ objs
      assertEqual "Id"
        [ Id 0 "e0"
        , Id 1 "e1"
        , Id 2 "text-1"
        ] $ concatMap getObjID objs'
    )
  , TestLabel "Fix Id 1" $ TestCase
    ( do
      let es =
            [Element (Id 0 "v1") (PolyLine []) noStyle M.empty,
            Element (Id 2 "v3") (PolyLine [Point (0,1), ElementRef 0]) noStyle M.empty]
      let table = A.array (0,3) [(0,0), (1,1), (2,2), (3,3)]
      assertEqual "Ids"
        [Id 0 "v1", Id 1 "v3"] $ map getId $ fixId table es
    )
  , TestLabel "Fix Id 2" $ TestCase
    ( do
      let es =
            [
            Element
              (Id 6 "v1") (Shape (Point (0.0,0.0)) (Ge.Ellipse 0.0 0.0))
              (noStyle)
              M.empty,
            Element
              (Id 7 "v2") (Shape (Point (0.0,1.0)) (Ge.Ellipse 0.0 0.0))
              (noStyle)
              M.empty,
            Element
              (Id 8 "v3") (PolyLine [ElementRef 6,ElementRef 7])
              (noStyle)
              M.empty,            
            Element
              (Id 9 "v4") (Shape (Point (0.0,0.0)) (Ge.Ellipse 0.0 0.0))
              (noStyle)
              M.empty,
            Element
              (Id 2 "v9") (PolyLine [ElementRef 0,ElementRef 1])
              (noStyle)
              M.empty
            ]
          idTable = A.array (0,12) [(0,6),(1,7),(2,2),(3,9),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12)]
          [  Element _ _ _ _
           , Element _ _ _ _
           , Element eid3 d3 _ _
           , Element _ _ _ _
           , Element eid5 d5 _ _
           ] = fixId idTable es
      assertEqual "Id 2" (Id 2 "v3") eid3
      assertEqual "Id 4" (Id 4 "v9") eid5
      assertEqual "Drawing 5" (PolyLine [ElementRef 0, ElementRef 1]) d5
      assertEqual "Drawing 3" (PolyLine [ElementRef 0, ElementRef 1]) d3
    )
  , TestLabel "Bug 1" $ TestCase
    ( do
      svg <- readFile "tests/SVG/test-19.svg"
      let universe = load svg conf1
          [  Element _ _ _ _
           , Element _ _ _ _
           , Element _ _ _ _
           , Element _ _ _ _
           , Element _ d5 _ _
           ] = universeValues universe $ A.elems $ uElements universe
      assertEqual "Line 1" (PolyLine [ElementRef 0, ElementRef 1]) d5
    )
  ]

main = do 
  tcounts <- runTestTT tests
  if errors tcounts + failures tcounts > 0 then exitFailure else exitSuccess

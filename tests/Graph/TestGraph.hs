module Main where

import Data.List

import Pictikz.Elements
import Pictikz.Geometry as G
import Pictikz.Graph
import Pictikz.Text as T

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S

rect1 = G.Rectangle 0 (-0) 0 0
textN = T.Text "n" []
paragraphN = Paragraph 0 1 [textN] []
textE = T.Text "e" []
paragraphE = Paragraph 1 0 [textE] []
textS = T.Text "s" []
paragraphS = Paragraph 0 (-1) [textS] []
textW = T.Text "w" []
paragraphW = Paragraph (-1) 0 [textW] []
textC = T.Text "c" []
paragraphC = Paragraph 0 0 [textC] []

nodes1 = map
  (\(x,y,i) -> Object (G.Ellipsis x y 5 5) ("c" ++ show i) ([],0,0) []) $
  zipWith (\(x,y) i -> (x,y,i))
    [ (  5.3,  -6.5)
    , ( 66.41, -5.6) 
    , (117.73, -5.3) 
    ]
    [1..]
nodes2 = map
  (\(x,y,i) -> Object (G.Rectangle x y 10 13) ("r" ++ show i) ([],0,0) []) $ 
  zipWith (\(x,y) i -> (x,y,i))
    [ (  1.2,  -22.7)
    , ( 57.46, -22.18) 
    , (111.98, -22.44) 
    ]
    [1..]
edges1 = map
  (\(x0, y0, x1, y1) -> Line x0 y0 x1 y1 [])
  [ (12,  -6,  58,  -6)
  , (74,  -5, 108,  -6)
  , (12, -29,  53, -29)
  , (70, -28, 107, -30)
  ]

layer1 = Layer 0 [Object rect1 "v1" ([],0,0) [], paragraphN]
layer2 = Layer 1 [Object rect1 "v1" ([],0,0) [], paragraphE]
layer3 = Layer 2 [Object rect1 "v1" ([],0,0) [], paragraphS]
layer4 = Layer 3 [Object rect1 "v1" ([],0,0) [], paragraphW]
layer5 = Layer 4 [Object rect1 "v1" ([],0,0) [], paragraphC]
layer6 = Layer 0 $ nodes1 ++ nodes2 ++ edges1

sortFst = sortBy (\(a0,b0) (a1, b1) -> compare a0 a1)
endpoints (Edge v1 v2 _ _) = (v1, v2)

tests = TestList                         
  [ TestLabel "Label Position N" $ TestCase
    ( do
      let Layers [Graph [n1] [] ] = makeGraph [layer1]
      assertEqual "" (Node rect1 0 0 "v1" ([textN], 1, pi/2)   [Arrow ArrowNone] (0,0)) n1
    )
  , TestLabel "Label Position E" $ TestCase
    ( do
      let Layers [Graph [n1] [] ] = makeGraph [layer2]
      assertEqual "" (Node rect1 0 0 "v1" ([textE], 1, 0)   [Arrow ArrowNone] (1,1)) n1
    )
  , TestLabel "Label Position S" $ TestCase
    ( do
      let Layers [Graph [n1] [] ] = makeGraph [layer3]
      assertEqual "" (Node rect1 0 0 "v1" ([textS], 1, 3*pi/2)   [Arrow ArrowNone] (2,2)) n1
    )
  , TestLabel "Label Position W" $ TestCase
    ( do
      let Layers [Graph [n1] [] ] = makeGraph [layer4]
      assertEqual "" (Node rect1 0 0 "v1" ([textW], 1, pi)   [Arrow ArrowNone] (3,3)) n1
    )
  , TestLabel "Label Position C" $ TestCase
    ( do
      let Layers [Graph [n1] [] ] = makeGraph [layer5]
      assertEqual "" (Node rect1 0 0 "v1" ([textC], 0, 0)   [Arrow ArrowNone] (4,4)) n1
    )
  , TestLabel "Make Graph 1" $ TestCase
    ( do
      let Layers [Graph nodes edges] = makeGraph [layer6]
      assertEqual "" [("c1", "c2"), ("c2", "c3"), ("r1", "r2"), ("r2", "r3")] $ sortFst $ map endpoints edges
    )
  ]

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess

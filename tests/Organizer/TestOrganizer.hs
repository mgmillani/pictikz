module Main where

import Data.List

import Pictikz.Clustering
import Pictikz.TestUtils
import Pictikz.Elements
import qualified Pictikz.Geometry as G
import Pictikz.Organizer

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Array.IArray as A
import qualified Data.Array.Unboxed as U

conf1 = defaultConfiguration
  { confDashedScale = Just $ Range 10 20
  , confThicknessScale = Just $ Range 10 20
  }
conf2 = defaultConfiguration
  { confDashedGrouping = Just $ Cluster 0.1
  , confDashedScale = Just $ Range 10 20
  , confThicknessGrouping = Just $ Cluster 0.1
  , confThicknessScale = Just $ Range 10 20
  }
conf3 = defaultConfiguration{confDashedGrouping = Just $ Regular 0.1, confDashedScale = Just $ Range 10 20}
conf4 = defaultConfiguration
  { confDashedGrouping = Just $ Fixed [("dashed", [20,20]), ("dotted", [10,20])]
  , confDashedScale = Just $ Range 10 20
  , confThicknessGrouping = Just $ Fixed [("very thick",20), ("thick",10)]
  , confThicknessScale = Just $ Range 10 20
  }
conf5 = defaultConfiguration{
      confNodeGrouping = Just $ Regular (0.1, 0.1)
    , confPostScale = Just $ MinDist (100, 100)
    , confPreScale = Just $ Fit (100, 100)
    }
conf6 = conf5{confPostTranslate = Just $ Shift (0,0)}
conf7 = defaultConfiguration{confNodeGrouping = Just $ Regular (0.1, 0.1)}
conf8 = defaultConfiguration{confPreScale = Just $ Fit (50, 50)}

edges1 =
  [ Element (Id 5 "") (PolyLine [ElementRef 0, ElementRef 1])
            noStyle{dashPattern = Just [1,1.5,1,1.5,1,2]} M.empty
  , Element (Id 6 "") (PolyLine [ElementRef 0, ElementRef 1])
            noStyle{dashPattern = Just [1,2]} M.empty
  , Element (Id 7 "") (PolyLine [ElementRef 0, ElementRef 1])
            noStyle{dashPattern = Just [1,1]} M.empty
  , Element (Id 8 "") (PolyLine [ElementRef 0, ElementRef 1])
            noStyle{dashPattern = Just [1,1]} M.empty
  , Element (Id 9 "") (PolyLine [ElementRef 0, ElementRef 1])
            noStyle{dashPattern = Just [1,2]} M.empty
  ]

nodes1 =
  [ Element (Id 0 "v1") (Shape (Point (0,0)) (G.Rectangle 1 1))
            noStyle{dashPattern = Just [1,1.5,1,1.5,1,2]} M.empty
  , Element (Id 1 "v2") (Shape (Point (0,1)) (G.Rectangle 1 1))
            noStyle{dashPattern = Just [1,2]} M.empty
  , Element (Id 2 "v3") (Shape (Point (1,0)) (G.Rectangle 1 1))
            noStyle{dashPattern = Just [1,1]} M.empty
  , Element (Id 3 "v4") (Shape (Point (1,1)) (G.Rectangle 1 1))
            noStyle{dashPattern = Just [2,1]} M.empty
  , Element (Id 4 "v5") (Shape (Point (2,0)) (G.Rectangle 1 1))
            noStyle{dashPattern = Just [1,2]} M.empty
  ]


nodes2 =
  [ Element (Id 0 "v1") (Shape (Point (5,6)) (G.Ellipse 5 5))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , dashPattern = Just [1.5,1.5]
                   , thickness = Just 0.2
                   , arrow = Just ArrowNone} M.empty
  , Element (Id 1 "v2") (Shape (Point (66,5)) (G.Ellipse 5 5))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , thickness = Just 0.2
                   , arrow = Just ArrowNone} M.empty
  , Element (Id 2 "v3") (Shape (Point (117,5)) (G.Ellipse 5 5))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , thickness = Just 1.0
                   , arrow = Just ArrowNone} M.empty
  , Element (Id 3 "v4") (Shape (Point (6,29)) (G.Rectangle 10 13))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , thickness = Just 0.2
                   , arrow = Just ArrowNone} M.empty
  , Element (Id 4 "v5") (Shape (Point (62,28)) (G.Rectangle 10 13))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , thickness = Just 1.0
                   , arrow = Just ArrowNone} M.empty
  , Element (Id 5 "v6") (Shape (Point (117,29)) (G.Rectangle 10 13))
            noStyle{ strokeColor = Just $ NamedColor "pictikz-black"
                   , thickness = Just 0.2
                   , dashPattern = Just [0.2, 1.5]
                   , arrow = Just ArrowNone} M.empty
  ]

nodes3 =
  [ Element (Id 0 "v1") (Shape (Point ( 0.001, 0)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 1 "v2") (Shape (Point (-0.001, 1)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 2 "v3") (Shape (Point ( 0,     2)) (G.Rectangle 1 1)) noStyle M.empty
  ]
  
nodes4 =
  [ Element (Id 0 "v1") (Shape (Point ( 90, 0)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 1 "v2") (Shape (Point ( 45, 0)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 2 "v3") (Shape (Point ( 0, 0))  (G.Rectangle 1 1)) noStyle M.empty
  ]

nodes5 =
  map (\(x,y,i) ->
    Element (Id i ("v" ++ show i)) (Shape (Point (x,y)) (G.Rectangle 1 1)) noStyle M.empty)
    [ ( -59.808,  0.343, 0) 
    , (  41.638,  1.233, 1) 
    , ( 144.864,  0.343, 2) 
    , (-144.864, -1.233, 3) 
    ]

nodes6 =
  [ Element (Id 0 "v1") (Shape (Point ( 0, 10)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 1 "v2") (Shape (Point ( 10, 0)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 2 "v3") (Shape (Point ( 0, 0))  (G.Rectangle 1 1)) noStyle M.empty
  ]

curves1 = 
  [ Element (Id 0 "c0")
      (PolyLine
        [ Point ( 0, 0.95)
        , Point (0, 1.95)
        , Point (0,3)
        , Point (0.94,3)] ) noStyle M.empty
  , Element (Id 1 "c1")
      (PolyLine
        [ Point ( 0, 0)
        , Point (1.02, 0)
        , Point (1.02, 1.05)
        , Point (1.02, 2.05)] ) noStyle M.empty
  ]  

curves2 = curves1 ++ 
  [ Element (Id 2 "v2") (Shape (Point ( 0, 0.7)) (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 3 "v3") (Shape (Point ( 1, 6))  (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 4 "v4") (Shape (Point ( 2, 6))  (G.Rectangle 1 1)) noStyle M.empty
  , Element (Id 5 "v5") (Shape (Point ( 2, 0))  (G.Rectangle 1 1)) noStyle M.empty
  ]

vs1 = [1,1.5,1,1.5,1,2,1,2,1,1,2,1,1,2]
vs2 = [0.9,1,1.1,1.9,2,2.1]

tests = TestList                         
  [ TestLabel "Style Scaling 1" $ TestCase
    ( do

      let [n1', n2', n3', n4', n5'] = A.elems $ uElements $ organize conf1 (makeUniverse nodes1)
      assertEqual "" (Just [10,15,10,15,10,20]) (dashPattern $ getStyle n1')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n2')
      assertEqual "" (Just [10,10]) (dashPattern $ getStyle n3')
      assertEqual "" (Just [20,10]) (dashPattern $ getStyle n4')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n5')
    )
  , TestLabel "Style Scaling 2" $ TestCase
    ( do

      let [n1, n2, n3, n4, n5, n6] = A.elems $ uElements $ organize conf1 (makeUniverse nodes2)
      assertEqual "" (Just 10) (thickness $ getStyle n1)
      assertEqual "" (Just 10) (thickness $ getStyle n2)
      assertEqual "" (Just 20) (thickness $ getStyle n3)
      assertEqual "" (Just 10) (thickness $ getStyle n4)
      assertEqual "" (Just 20) (thickness $ getStyle n5)
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n6)
    )
  , TestLabel "Style Scaling 3" $ TestCase
    ( do
      let universe = (makeUniverse nodes2){uAttributeScale = 2}
      let [n1, n2, n3, n4, n5, n6] = A.elems $ uElements $ organize conf1 universe
      assertEqual "" (Just 5) (thickness $ getStyle n1)
      assertEqual "" (Just 5) (thickness $ getStyle n2)
      assertEqual "" (Just 10) (thickness $ getStyle n3)
      assertEqual "" (Just 5) (thickness $ getStyle n4)
      assertEqual "" (Just 10) (thickness $ getStyle n5)
      assertEqual "" (Just [5,10]) (dashPattern $ getStyle n6)
    )
  , TestLabel "Scale Value 1" $ TestCase
    ( do
      let ts = [0.2,0.2,1.0,0.2,1,0.2]
          scaleF = scaleValue 1 (Just (Range 10 20)) ts
          ts' = map scaleF ts
      assertEqual "" [10,10,20,10,20,10] ts'
    )
  , TestLabel "Scale Value 2" $ TestCase
    ( do
      let (eti, ti, tn) =
            numberAttributes
              (\o -> case thickness $ getStyle o of
                          Nothing -> (0, [])
                          Just t -> (1, [t])
              )
              nodes2
          ts = map snd ti
          scaleF = scaleValue 1 (Just (Range 10 20)) ts
          ts' = map scaleF ts
      assertEqual "ts"  [0.2,0.2,1,0.2,1,0.2] ts
      assertEqual "ts'" [10,10,20,10,20,10] ts'
    )
  , TestLabel "Dashed Grouping Cluster" $ TestCase
    ( do

      let [n1', n2', n3', n4', n5'] = A.elems $ uElements $ organize conf2 (makeUniverse nodes1)
      assertEqual "" (Just [10,15,10,15,10,20]) (dashPattern $ getStyle n1')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n2')
      assertEqual "" (Just [10,10]) (dashPattern $ getStyle n3')
      assertEqual "" (Just [20,10]) (dashPattern $ getStyle n4')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n5')
    )
  , TestLabel "Dashed Grouping Regular" $ TestCase
    ( do

      let [n1', n2', n3', n4', n5'] = A.elems $ uElements $ organize conf3 (makeUniverse nodes1)
      assertEqual "" (Just [10,15,10,15,10,20]) (dashPattern $ getStyle n1')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n2')
      assertEqual "" (Just [10,10]) (dashPattern $ getStyle n3')
      assertEqual "" (Just [20,10]) (dashPattern $ getStyle n4')
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n5')
    )
  , TestLabel "Style Grouping Cluster 2" $ TestCase
    ( do

      let [n1, n2, n3, n4, n5, n6] = A.elems $ uElements $ organize conf2 (makeUniverse nodes2)
      assertEqual "" (Just 10) (thickness $ getStyle n1)
      assertEqual "" (Just 10) (thickness $ getStyle n2)
      assertEqual "" (Just 20) (thickness $ getStyle n3)
      assertEqual "" (Just 10) (thickness $ getStyle n4)
      assertEqual "" (Just 20) (thickness $ getStyle n5)
      assertEqual "" (Just [10,20]) (dashPattern $ getStyle n6)
    )
  , TestLabel "Style Grouping Fixed 1" $ TestCase
    ( do

      let [n1, n2, n3, n4, n5, n6] = A.elems $ uElements $ organize conf4 (makeUniverse nodes2)
      assertEqual "" [("dashed", Nothing), ("thick", Nothing)] (M.assocs $ getAttributes n1)
      assertEqual "" [( "thick", Nothing)]      (M.assocs $ getAttributes n2)
      assertEqual "" [( "very thick", Nothing)] (M.assocs $ getAttributes n3)
      assertEqual "" [( "thick", Nothing)]      (M.assocs $ getAttributes n4)
      assertEqual "" [( "very thick", Nothing)] (M.assocs $ getAttributes n5)
      assertEqual "" [("dotted", Nothing), ( "thick", Nothing)] (M.assocs $ getAttributes n6)
    )
  , TestLabel "Grouping Cluster 1" $ TestCase
    ( do
      let groups = groupValues (Just $ Cluster 0.1) vs1
      assertEqual "" [1,1.5,2] (S.toList groups)
    )
  , TestLabel "Grouping Cluster 2" $ TestCase
    ( do
      let groups = groupValues (Just $ Cluster 0.2) vs2
      assertEqual "" [1,2] (S.toList groups)
    )
  , TestLabel "Grouping Regular" $ TestCase
    ( do
      let groups = groupValues (Just $ Regular 0.1) vs1
      assertEqual "" [1,1.5,2] (S.toList groups)
    )
  , TestLabel "Node Grouping Cluster" $ TestCase
    ( do
      let universe = makeUniverse
            [ Element (Id 0 "v0") (Shape (Point (  0,   0)) (G.Rectangle 1 1)) noStyle M.empty
            , Element (Id 1 "v1") (Shape (Point (  2, 101)) (G.Rectangle 1 1)) noStyle M.empty
            , Element (Id 2 "v2") (Shape (Point (  99, 99)) (G.Rectangle 1 1)) noStyle M.empty
            , Element (Id 3 "v3") (Shape (Point ( 101,  2)) (G.Rectangle 1 1)) noStyle M.empty
            ]
          universe' = organize defaultConfiguration{confNodeGrouping = Just $ Cluster (0.1,0.1)} universe
          [p0,p1,p2,p3] = map getPos $ A.elems $  uElements universe'
      assertEqual "Point 0" (Point (  1,  1)) p0
      assertEqual "Point 1" (Point (  1,100)) p1
      assertEqual "Point 2" (Point (100,100)) p2
      assertEqual "Point 3" (Point (100,  1)) p3
    )
  , TestLabel "One dimensional grouping 0" $ TestCase
    ( do
      let universe  = makeUniverse nodes4
          universe' = organize conf7 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (90,0), Point (45,0), Point (0,0)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "One dimensional grouping 1" $ TestCase
    ( do
      let universe = (makeUniverse nodes4)
          universe' = organize conf8 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (50,0), Point (25,0), Point (0,0)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "One dimensional grouping 2" $ TestCase
    ( do
      let universe = makeUniverse nodes3
          universe' = organize conf5 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (0,0), Point (0,100), Point (0,200)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "One dimensional grouping 3" $ TestCase
    ( do
      let universe = makeUniverse nodes4
          universe' = organize conf5 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (0,0), Point (100,0), Point (200,0)] $ sortPosX $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "One dimensional grouping 4" $ TestCase
    ( do
      let universe = makeUniverse nodes4
          universe' = organize conf6 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (0,0), Point (100,0), Point (200,0)] $ sortPosX $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "One dimensional grouping 5" $ TestCase
    ( do
      let universe = makeUniverse nodes5
          universe' = organize conf6 universe
          [n1, n2, n3, n4] = A.elems $ uElements universe'
      assertEqual "" [Point (0,0), Point (100,0), Point (200,0), Point (300, 0)] $ sortPosX $ map (getPos' universe') [n1, n2, n3, n4]
    )
  , TestLabel "One dimensional grouping 6" $ TestCase
    ( do
      let universe = (makeUniverse nodes4){uScale = (2,2)}
          universe' = organize conf8 universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (50,0), Point (25,0), Point (0,0)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "Origin translation 1" $ TestCase
    ( do
      let universe = (makeUniverse nodes4)
          universe' = organize conf8{confPreTranslate = Just $ Origin (1,1)} universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (51,1), Point (26,1), Point (1,1)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "Polar 1" $ TestCase
    ( do
      let universe = (makeUniverse nodes4)
          universe' = organize defaultConfiguration
            { confCoordinateSystem = Just Polar
            } universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "" [Point (90,0), Point (45,0), Point (0,0)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "Polar 2" $ TestCase
    ( do
      let universe = makeUniverse nodes6
          universe' = organize defaultConfiguration
            { confPreTranslate = Just $ Origin (0,0)
            , confCoordinateSystem = Just Polar
            , confPostScale = Just $ Plain (3, pi)
            } universe
          [n1, n2, n3] = A.elems $ uElements universe'
      assertEqual "Ids" [Id 0 "v1", Id 1 "v2", Id 2 "v3"] $ map getId [n1,n2,n3]
      assertEqual "" [Point (-3,0), Point (3,0), Point (0,0)] $ sortPosY $ map (getPos' universe') [n1, n2, n3]
    )
  , TestLabel "Curve Clustering 1" $ TestCase
    ( do
      let universe = makeUniverse curves1
          universe' = organize defaultConfiguration
            { confCurveGrouping = Just $ Cluster (0.1, 0.1)
            , confNodeGrouping  = Just $ Regular (0.1, 0.1)
            } universe
          [ Element eid0 d0 _ _
            , Element eid1 d1 _ _ ] = A.elems $ uElements universe'

      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,1), Point (0,2), Point (0,3), Point (1,3)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1,0), Point (1,1), Point (1,2)]) d1
    )
  , TestLabel "Curve Clustering 1a" $ TestCase
    ( do
      let [  Element eid0 d0 _ _
           , Element eid1 d1 _ _ ] =
              clusterCurves (0,0, 1.02, 3) 0.1 0.1
                [] []
                clusterValue
                clusterValue
                curves1
      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,1), Point (0,2), Point (0,3), Point (1,3)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1,0), Point (1,1), Point (1,2)]) d1
    )
  , TestLabel "Curve Clustering 2" $ TestCase
    ( do
      let universe = makeUniverse curves2
          universe' = organize defaultConfiguration
            { confCurveGrouping = Just $ Regular (0.1, 0.1)
            , confNodeGrouping = Just $ Regular (0.1, 0.1)
            } universe
          [ Element eid0 d0 _ _
            , Element eid1 d1 _ _
            , _, _, _, _] = A.elems $ uElements universe'

      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,3), Point (0,4), Point (0,5), Point (1,5)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1,0), Point (1,3), Point (1,4)]) d1
    )
  , TestLabel "Curve Clustering 2a" $ TestCase
    ( do
      let [  Element eid0 d0 _ _
           , Element eid1 d1 _ _ ] =
              clusterCurves (0,0, 2, 6) 0.1 0.1
                [0,1,2] [0, 0.7,6]
                (gridOnGridCurveClustering 1)
                (gridOnGridCurveClustering 3)
                curves1
      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,3), Point (0,4), Point (0,5), Point (1,5)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1,0), Point (1,3), Point (1,4)]) d1
    )
  , TestLabel "Curve Clustering 3" $ TestCase
    ( do
      let universe = makeUniverse curves1
          universe' = organize defaultConfiguration
            { confCurveGrouping = Just $ Regular (0.1, 0.1)
            , confNodeGrouping  = Just $ Regular (0.1, 0.1)
            } universe
          [ Element eid0 d0 _ _
            , Element eid1 d1 _ _ ] = A.elems $ uElements universe'

      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,1), Point (0,2), Point (0,3), Point (1.02,3)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1.02,0), Point (1.02,1), Point (1.02,2)]) d1
    )
  , TestLabel "Curve Clustering 3a" $ TestCase
    ( do
      let [  Element eid0 d0 _ _
           , Element eid1 d1 _ _ ] =
              clusterCurves (0,0,1.02, 3) 0.1 0.1
                [0,1.02] [0, 3]
                (gridOnGridCurveClustering 1.02)
                (gridOnGridCurveClustering 3)
                curves1
      assertEqual "Ids" [Id 0 "c0", Id 1 "c1"] $ [eid0, eid1]
      assertEqual "Curve 0" (PolyLine [Point (0,1), Point (0,2), Point (0,3), Point (1.02,3)]) d0
      assertEqual "Curve 1" (PolyLine [Point (0,0), Point (1.02,0), Point (1.02,1), Point (1.02,2)]) d1
    )
  , TestLabel "Segment Clustering 1" $ TestCase
    ( do
      let curves =
            [ Curve [ElementRef 0,ElementRef 3] [Cubic (0.2, 3 * pi/2) (0.2, pi / 2)]
            , Curve [ElementRef 3,ElementRef 2] [Cubic (0.2, 2 * pi)   (0.2, pi)]
            , Curve [ElementRef 1,ElementRef 2] [Cubic (0.2, 3 * pi/2) (0.2, pi / 2)]
            , Curve [ElementRef 0,ElementRef 1] [Cubic (0.2, 0)        (0.2,pi)]
            , Curve [ElementRef 1,ElementRef 3] [Cubic (0.3,5*pi/4)    (0.3,pi/4)]
            , Curve [ElementRef 0,ElementRef 2] [Cubic (1.2,pi/5)      (1,pi/3)]
            ]
          (ci, segArr) = clusterSegments 0.1 curves
      assertEqual "Segment 0" (Cubic (0.2, 3 * pi / 2) (0.2, pi/2)) $ segArr U.! 0
      assertEqual "Segment 1" (Cubic (0.2, 2 * pi)     (0.2, pi))   $ segArr U.! 1
      assertEqual "Segment 2" (Cubic (0.2, 3 * pi / 2) (0.2, pi/2)) $ segArr U.! 2
      assertEqual "Segment 3" (Cubic (0.2, 0)          (0.2, pi))   $ segArr U.! 3
    )
  , TestLabel "Segment Clustering 1a" $ TestCase
    ( do
      let psis = zip [0..] $ map (Unbounded . toClusterAngle)
            [ 3 * pi/2
            , pi/2
            , 2 * pi
            , pi
            , 3 * pi / 2
            , pi / 2
            , 0
            , pi
            , 5 * pi / 4
            , pi / 4
            , pi / 5
            , pi / 3
            ]
          pinnedAngles = map ((*(pi/4))) [0,1,2]
          (psiArr, nPsiClusters, _) =
            clusterValuesWithPinned (pi/2) (0.1 * pi/2)
                                    regularAngleClustering
                                    (length psis) psis pinnedAngles
      assertEqual "Values" 
        [ 0
        , 0
        , 0
        , 0
        , 0
        , 0
        , 0
        , 0
        , 45
        , 45
        , 22
        , 68
        ]
        $ map roundAngle $ U.elems psiArr
    )
  , TestLabel "Segment Clustering 2" $ TestCase
    ( do
      let psis = zip [0..] $ map (Unbounded . toClusterAngle)
            [ 0
            , pi/4
            , pi/2
            , pi/6
            , pi/3
            ]
          pinnedAngles = map ((*(pi/4))) [0,1,2]
          (psiArr, nPsiClusters, _) =
            clusterValuesWithPinned (pi/2) (0.1 * pi/2)
                                    regularAngleClustering
                                    (length psis) psis pinnedAngles
      assertEqual "Values" 
        [ 0
        , 45
        , 0
        , 22
        , 68
        ]
        $ map roundAngle $ U.elems psiArr
    )
  , TestLabel "Segment Clustering 2a" $ TestCase
    ( do
      let psis = zip [0..] $ map (Unbounded . toClusterAngle)
            [ 0
            , pi/4
            , pi/6
            , pi/3
            ]
          pinnedAngles = map ((*(pi/4))) [0,1,2]
          (psiArr, nPsiClusters, _) =
            clusterValuesWithPinned (pi/2) (0.1 * pi/2)
                                    valueIndex
                                    (length psis) psis pinnedAngles
      assertEqual "Values" 
        [ 0,0,1,1 ]
        $ U.elems psiArr
    )
  ]

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess

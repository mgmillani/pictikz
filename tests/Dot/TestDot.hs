module Main where

import Data.List

import Pictikz.Dot.Input
import Pictikz.Text
import Pictikz.Elements hiding (alignment)
import Pictikz.TestUtils
import qualified Pictikz.Geometry as Ge

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Array.IArray as A

conf1 = defaultConfiguration

tests = TestList                          
  [ TestLabel "Nodes 1" $ TestCase
    ( do
      dot <- readFile "tests/Dot/test-1-single-vertex.gv"
      let Right universe = load dot conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [(Element eid1 d1 st1 att1)] = map roundElement $ A.elems $ uElements universe
      assertEqual "" (1,1) (round sx, round sy)
      assertEqual "" (0,0) (round w, round h)
      assertEqual "" (Id 0 "v1") eid1
      assertEqual "" (Shape (Point (0,0)) (Ge.Ellipse 10 10)) d1
      assertEqual "Label" (Just  [[Text "v1" noFormat]]) $ label st1
    )
  , TestLabel "Shapes 1" $ TestCase
    ( do
      dot <- readFile "tests/Dot/test-2-shapes.gv"
      let Right universe = load dot conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 st1 att1
           , Element eid2 d2 st2 att2
           , Element eid3 d3 st3 att3
           , Element eid4 d4 st4 att4
           , Element eid5 d5 st5 att5
           ] = map roundElement $ A.elems $ uElements universe
      assertEqual "Scale" (1,1) (round sx, round sy)
      assertEqual "Size" (0,0) (round $ 100 * w, round $ 100 * h)
      assertEqual "Indices" [Id i ("v" ++ show (i+1)) | i <- [0..4]] [eid1, eid2, eid3, eid4, eid5]
      assertEqual "Shape 1" (Shape (Point (0,0)) (Ge.Ellipse 10 10)) d1
      assertEqual "Shape 2" (Shape (Point (0,0)) (Ge.Rectangle 20 20)) d2
      assertEqual "Shape 3" (Shape (Point (0,0)) (Ge.Ellipse 25 25)) d3
      assertEqual "Shape 4" (Shape (Point (0,0)) (Ge.Rectangle 50 50)) d4
      assertEqual "Shape 5" (Shape (Point (0,0)) (Ge.Rectangle 100 100)) d5
    )
  , TestLabel "Fill 1" $ TestCase
    ( do
      dot <- readFile "tests/Dot/test-3-color.gv"
      let Right universe = load dot conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 st1 att1
           , Element eid2 d2 st2 att2
           , Element eid3 d3 st3 att3
           ] = map roundElement $ A.elems $ uElements universe
      assertEqual "Indices" [Id 0 "v1", Id 1 "v2"] [eid1, eid2]
      assertEqual "Index edge" (Id 2 "v1--v2") eid3
      assertEqual "Fill 1" (Just $ NamedColor "red") (fillColor st1)
      assertEqual "Fill 2" (Just $ NamedColor "blue")   (fillColor st2)
      assertEqual "Fill 3" (Just $ NamedColor "green") (strokeColor st3)
    )
  , TestLabel "Edges 1" $ TestCase
    ( do
      dot <- readFile "tests/Dot/test-4-edge.gv"
      let Right universe = load dot conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 st1 att1
           , Element eid2 d2 st2 att2
           , Element eid3 d3 st3 att3
           ] = map roundElement $ A.elems $ uElements universe
      assertEqual "Indices" [Id 0 "a", Id 1 "b", Id 2 "a--b"] [eid1, eid2, eid3]
      assertEqual "Edge points" (PolyLine [ElementRef 0, ElementRef 1]) d3
    )
  , TestLabel "Edges 2" $ TestCase
    ( do
      dot <- readFile "tests/Dot/test-5-edge-label.gv"
      let Right universe = load dot conf1
          (sx, sy) = uScale universe
          (w, h) = uSize universe
          [  Element eid1 d1 st1 att1
           , Element eid2 d2 st2 att2
           , Element eid3 d3 st3 att3
           ] = map roundElement $ A.elems $ uElements universe
      assertEqual "Indices" [Id 0 "a", Id 1 "b", Id 2 "a--b"] [eid1, eid2, eid3]
      assertEqual "Label" (Just  [[Text "edge label" noFormat]]) $ label st3
    )
  ]

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess

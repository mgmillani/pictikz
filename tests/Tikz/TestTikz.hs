module Main where

import Data.List

import Pictikz.Tikz.Output
import Pictikz.TestUtils
import Pictikz.Text
import Pictikz.Elements hiding (alignment)
import qualified Pictikz.Geometry as Ge

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Array.IArray as A

conf1 = defaultConfiguration

getPos' universe el = 
  let (sx,sy) = uScale universe
      (ox, oy) = uOrigin universe
  in case getPos el of
    Point (x,y) -> Point (fromIntegral $ round $ ox + sx*x, fromIntegral $ round $ oy + sy*y)
    pos -> pos
sortPosX :: Ord a => [Coordinate a] -> [Coordinate a]
sortPosX = sortBy (\(Point (x0,y0)) (Point (x1,y1)) -> compare x0 x1)
sortPosY :: Ord a => [Coordinate a] -> [Coordinate a]
sortPosY = sortBy (\(Point (x0,y0)) (Point (x1,y1)) -> compare y0 y1)

-- roundCoordinate :: (RealFrac a, Integral b) => Coordinate a -> Coordinate b
-- roundCoordinate (Point (x,y)) = Point (round $ 100* x, round $ 100* y)
-- roundCoordinate (ElementRef x) = ElementRef x
-- roundShape (Ge.Rectangle w h)  = Ge.Rectangle (round $ 100 * w) (round $ 100 * h)
-- roundShape (Ge.Ellipse rx ry) = Ge.Ellipse (round $ 100 * rx) (round $ 100 * ry)
-- roundDrawing (Shape p s) = Shape (roundCoordinate p) (roundShape s)
-- roundDrawing (PolyLine cs) = PolyLine $ map roundCoordinate cs
-- roundDrawing (Paragraph p t) = Paragraph (roundCoordinate p) t
-- roundElement (Element eid d st att) = Element eid (roundDrawing d) st att

defaultUniverse = Universe
  {uScale = (1,1)
  , uOrigin = (0,0)
  , uSize = (1,1)
  , uAttributeScale = 1
  , uElements = A.array (0,0) []}

universe1 = defaultUniverse
  { uElements = A.array (0,2)
      [ (0, Element (Id 0 "v0") (Shape (Point (0,0)) (Ge.Rectangle 1 1)) noStyle M.empty)
      , (1, Element (Id 1 "e1") (PolyLine [ElementRef 0, ElementRef 2]) noStyle M.empty)
      , (2, Element (Id 2 "v2") (Shape (Point (1,1)) (Ge.Ellipse 1 1)) noStyle M.empty)
      ]
  } :: Universe Double

p0 = Point (0,0) :: Coordinate Double
p1 = Point (5,10) :: Coordinate Double

tests = TestList                          
  [ TestLabel "Nodes 1" $ TestCase
    ( do
      assertEqual "" "\\node[rectangle]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Rectangle 1 1)) noStyle M.empty)
      assertEqual "" "\\node[circle]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle M.empty)
      assertEqual "" "\\node[circle, line width = 1]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle{thickness = Just 1} M.empty)
      assertEqual "" "\\node[circle]\n\t(v1) at (5, 10){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p1 (Ge.Ellipse 1 1)) noStyle M.empty)
      assertEqual "" "\\node[circle, triangle]\n\t(v1) at (5, 10){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p1 (Ge.Ellipse 1 1)) noStyle (M.fromList $ [("triangle", Nothing)]))
      assertEqual "" "\\node[circle, inner sep = 8pt]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle (M.fromList $ [("inner sep", Just "8pt")]))
      assertEqual "" "\\node[circle]\n\t(v1) at (0, 0) {{$v_1$}};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle{label = Just [[Text "$v_1$" noFormat]], labelPosition = Just [Around 0 0]} M.empty)
      assertEqual "" "\\node[circle, label = right:{{$v_1$}}]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle{label = Just [[Text "$v_1$" noFormat]], labelPosition = Just [Around 2 0]} M.empty)
      assertEqual "" "\\node[circle, label = above right:{{$v_1$}}]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle{label = Just [[Text "$v_1$" noFormat]], labelPosition = Just [Around 2 (pi/4)]} M.empty)
      assertEqual "" "\\node[circle, label = below left:{{$v_1$}}]\n\t(v1) at (0, 0){};" $
        draw universe1 conf1 (Element (Id 0 "v1") (Shape p0 (Ge.Ellipse 1 1)) noStyle{label = Just [[Text "$v_1$" noFormat]], labelPosition = Just [Around 2 (5.1*pi/4)]} M.empty)
    )
  , TestLabel "Line 1" $ TestCase
    ( do
      assertEqual "Path 1" "\\path\n\t(0, 0) to (5, 10);" $
        draw universe1 conf1 (Element (Id 1 "e1") (PolyLine [p0, p1]) noStyle M.empty)
      assertEqual "Path 2" "\\path[dash pattern = on 1 off 1]\n\t\
      \(0, 0) to (5, 10);" $
        draw universe1 conf1 (Element (Id 1 "e1") (PolyLine [p0, p1]) noStyle{dashPattern = Just [1,1]} M.empty)
      assertEqual "Path 3" "\\path\n\t(v0) to (v2);" $
        draw universe1 conf1 (Element (Id 1 "e1") (PolyLine [ElementRef 0, ElementRef 2]) noStyle M.empty :: Element Double)
      assertEqual "Path 4" "\\path\n\t(v0) to node[above] {{$e_1$}} (v2);" $
        draw universe1 conf1 (Element (Id 1 "e1") (PolyLine [ElementRef 0, ElementRef 2]) noStyle{label = Just [[Text "$e_1$" noFormat]], labelPosition = Just [OnSegment 0 (pi/2)]} M.empty :: Element Double )
      assertEqual "Path 5" "\\path\n\t(0, 0) to (1, 1) to (2, 2);" $
        draw universe1 conf1 (Element (Id 1 "e1") (PolyLine [Point (0,0), Point (1,1), Point (2.0 :: Double, 2.0)]) noStyle M.empty)
    )
  , TestLabel "Universe 1" $ TestCase
    ( do
      let universe = defaultUniverse
            { uElements = A.array (0,0) [(0, Element (Id 0 "v1") (Shape (Point (0,0)) (Ge.Ellipse 1 1)) noStyle M.empty)]
            }
          str = output universe conf1
      assertEqual "" "\\node[circle]\n\t(v1) at (0, 0){};\n" str
    )
  , TestLabel "Universe 2" $ TestCase
    ( do
      let universe = defaultUniverse
            { uElements = A.array (0,1)
                [ (0, Element (Id 0 "v1") (Shape (Point (0,0)) (Ge.Ellipse 1 1))
                        noStyle{ label = Just [[Text "a" noFormat{italics = Just False}]]
                                , labelPosition = Just [Around 0 0]} M.empty)
                , (1, Element (Id 1 "v2") (Shape (Point (0,0)) (Ge.Ellipse 1 1))
                        noStyle{ label = Just [[Text "b" noFormat{italics = Just True}]]
                               , labelPosition = Just [Around 0 0]} M.empty)
                ]
            }
          str = output universe conf1
      assertEqual "" "\\node[circle]\n\t(v1) at (0, 0) {{a}};\n\\node[circle]\n\t(v2) at (0, 0) {\\itshape{}{b}};\n" str
    )
  , TestLabel "Show Float 1" $ TestCase
    ( do
      assertEqual "" "-21.217" (showPFloat (-21.217) 3)
      assertEqual "" "-0.217" (showPFloat (-0.217) 3)
      assertEqual "" "-0.9" (showPFloat (-0.9) 3)
      assertEqual "" "0.9" (showPFloat (0.9) 1)
      assertEqual "" "1" (showPFloat (1.0) 1)
    )
  , TestLabel "Draw Curve 1" $ TestCase
    ( do
      let c1 = (Curve [Point (0,0), Point (2,0)] [Arc (0, -pi/2) 0 True])
          e1 = Element (Id 0 "c1") c1 noStyle M.empty
          c2 = (Curve [Point (0,0), Point (2,0)] [Cubic (1,pi/2) (1,pi/2)])
          e2 = Element (Id 1 "c2") c2 noStyle M.empty
          c3 = (Curve [Point (0,0), Point (2,0), Point (4,0)] [Cubic (1,pi/2) (1,pi/2), Cubic (1,pi/2) (1,pi/2)])
          e3 = Element (Id 2 "c3") c3 noStyle M.empty
          u = makeUniverse [e1, e2, e3] :: Universe Double
      assertEqual "Cubic 1"
        "\\path\n\t(0, 0) .. controls (0, 1) and (2, 1) .. (2, 0);" $
        draw u defaultConfiguration e2
      assertEqual "Cubic 2"
        "\\path\n\t(0, 0) .. controls (0, 1) and (2, 1) .. (2, 0) .. controls (2, 1) and (4, 1) .. (4, 0);" $
        draw u defaultConfiguration e3
      --assertEqual "Arc 1"
      --  "\\path\n\t(0, 0) arc [start angle = 180, end angle = 360, x radius = 1, y radius = 1, rotate = 0];" $
      --  draw u defaultConfiguration e1
    )
  , TestLabel "Draw Curve 2" $ TestCase
    ( do
      let c2 = (Curve [Point (0,0), Point (2,0)] [Cubic (1,pi/2) (1,pi/2)])
          e2 = Element (Id 0 "c2") c2 noStyle M.empty
          c3 = (Curve [Point (0,0), Point (2,0), Point (4,0)] [Cubic (1,pi/2) (1,pi/2), Cubic (1,pi/2) (1,pi/2)])
          e3 = Element (Id 1 "c3") c3 noStyle M.empty
          u = (makeUniverse [e2, e3]) {uScale = (3,2), uOrigin = (2,1)} :: Universe Double
      assertEqual "Cubic 1"
        "\\path\n\t(2, 1) .. controls (2, 3) and (8, 3) .. (8, 1);" $
        draw u defaultConfiguration e2
      assertEqual "Cubic 2"
        "\\path\n\t(2, 1) .. controls (2, 3) and (8, 3) .. (8, 1) .. controls (8, 3) and (14, 3) .. (14, 1);" $
        draw u defaultConfiguration e3
    )
  ]

main = do 
  counts <- runTestTT tests
  if errors counts + failures counts > 0 then exitFailure else exitSuccess

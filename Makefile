HS_FILES = $(wildcard src/*.hs) ${wildcard src/*/*.hs}
NAME = pictikz
Program = dist/build/${NAME}/${NAME}

all: ${Program} examples/picture-examples.pdf

${NAME}.1: docs/README.body docs/README.header
	cd docs ; make ${NAME}.1
	cp docs/${NAME}.1 ${NAME}.1

README.md: docs/README.body docs/README.header
	cd docs ; make README.md
	cp docs/README.md README.md

${Program}: ${HS_FILES} ${NAME}.cabal
	cabal build
  
examples/picture-examples.pdf: ${Program}
	cd examples ; make -B Program="${shell pwd}/${Program}" -j16 picture-examples.pdf

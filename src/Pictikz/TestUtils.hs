module Pictikz.TestUtils where

import Data.List

import Pictikz.Elements
import qualified Pictikz.Geometry as Ge
import qualified Data.Array.IArray as A

makeUniverse elements = 
  let points = [(x,y) | n <- elements, let Point (x,y) = getPos n, isPoint $ getPos n] ++
        (concat [map (\(Point (x,y)) -> (x,y)) $ filter isPoint ps | Element _ d@(PolyLine ps) _ _ <- elements, isLine d])
      (x0, y0) = (minimum $ map fst points, minimum $ map snd points)
      fixCoord (Point (px, py)) = Point ((px - x0), (py - y0))
      fixCoord p = p
  in Universe
      { uOrigin = (0,0)
      , uScale = (1,1)
      , uSize = ((maximum $ map fst points) - x0, (maximum $ map snd points) - y0)
      , uElements = A.array (0, (length elements) - 1) $ zip [0..] $ map (fPos fixCoord) elements
      , uAttributeScale = 1
      }

getPos' universe el = 
  let (sx,sy) = uScale universe
      (ox, oy) = uOrigin universe
  in case getPos el of
    Point (x,y) -> Point (fromIntegral $ round $ ox + sx*x, fromIntegral $ round $ oy + sy*y)
    pos -> pos
sortPosX :: Ord a => [Coordinate a] -> [Coordinate a]
sortPosX = sortBy (\(Point (x0,y0)) (Point (x1,y1)) -> compare x0 x1)
sortPosY :: Ord a => [Coordinate a] -> [Coordinate a]
sortPosY = sortBy (\(Point (x0,y0)) (Point (x1,y1)) -> compare y0 y1)

roundStyle st = st
  { dashPattern   = fmap (map roundF) $ dashPattern st
  , thickness     = fmap roundF $ thickness st
  }
  where
    roundF = round . (*100)

roundF = round . (10*)
roundAngle phi = round $ (180 / pi) * phi
roundPosition (Around r phi) = Around ((fromIntegral $ round $ 10 * r)/10) ((fromIntegral $ round $ 10 * phi)/10)
roundPosition x = x
roundCoordinate :: (RealFrac a, Integral b) => Coordinate a -> Coordinate b
roundCoordinate (Point (x,y)) = Point (round $ 10 * x, round $ 10 * y)
roundCoordinate (ElementRef x) = ElementRef x
roundRawSegment (Cubic (x1, y1) (x2, y2)) = Cubic (roundF x1, roundF y1) (roundF x2, roundF y2)
roundRawSegment s = roundSegment s
roundSegment (Cubic (rho1, theta1) (rho2, theta2)) =
  Cubic (roundF rho1, roundAngle theta1) (roundF rho2, roundAngle theta2)
roundSegment (Arc (rho, theta) phi largeArc) =
  Arc (roundF rho, roundAngle theta) (roundAngle phi) largeArc
roundSegment Line = Line
roundShape (Ge.Rectangle w h)  = Ge.Rectangle (round $ 10 * w) (round $ 10 * h)
roundShape (Ge.Ellipse rx ry) = Ge.Ellipse (round $ 10 * rx) (round $ 10 * ry)
roundRawDrawing (Curve ps cs) = Curve (map roundCoordinate ps) (map roundRawSegment cs)
roundRawDrawing d = roundDrawing d
roundDrawing (Shape p s) = Shape (roundCoordinate p) (roundShape s)
roundDrawing (PolyLine cs) = PolyLine $ map roundCoordinate cs
roundDrawing (Curve ps cs) = Curve (map roundCoordinate ps) (map roundSegment cs)
roundDrawing (Paragraph p t) = Paragraph (roundCoordinate p) t
roundElement (Element eid d st att) = Element eid (roundDrawing d) (roundStyle st) att

universeCoordinate universe (ElementRef x) = ElementRef x
universeCoordinate universe (Point (x,y)) = 
  Point (x*sx + ox, y*sy + oy)
  where
    (ox, oy) = uOrigin universe
    (sx, sy) = uScale universe

universeShape universe shape =
  case shape of
    Ge.Ellipse rx ry -> Ge.Ellipse (rx * s) (ry * s)
    Ge.Rectangle w h -> Ge.Rectangle (w * s) (h * s)
    Ge.Point -> Ge.Point
    where
      s = uAttributeScale universe

universeSegment universe (Cubic (rho1, theta1) (rho2,theta2)) = Cubic (rho1', theta1') (rho2', theta2')
  where
    (sx,sy) = uScale universe
    x1 = rho1 * cos theta1
    y1 = rho1 * sin theta1
    x2 = rho2 * cos theta2
    y2 = rho2 * sin theta2
    (rho1', theta1') = Ge.polarDistance (0,0) (x1 * sx, y1 * sy)
    (rho2', theta2') = Ge.polarDistance (0,0) (x2 * sx, y2 * sy)
universeSegment universe s = s

universeValues universe els = 
  [ roundElement e
  | e' <- els
  , let Element eid d st att = e'
  , let d' =
          case d of
            Shape p sh -> Shape (universeCoordinate universe p) (universeShape universe sh)
            PolyLine cs -> PolyLine $ map (universeCoordinate universe) cs
            Curve ps cs -> Curve (map (universeCoordinate universe) ps) (map (universeSegment universe) cs)
            Paragraph p t -> Paragraph (universeCoordinate universe p) t            
  , let e = Element eid d' st{ thickness = fmap (*s) $ thickness st
                             , dashPattern = fmap (map (*s)) $ dashPattern st} att
  , isElement e
  ]
  where
    s = uAttributeScale universe

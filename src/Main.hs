--  Copyright 2017-2020 Marcelo Garlet Millani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Main where

import qualified Pictikz.Organizer as O
-- import Pictikz.Graph
import Pictikz.Elements
import qualified Pictikz.Tikz.Output as TikzOut
import qualified Pictikz.SVG.Input as SVGIn
import qualified Pictikz.Dot.Input as DotIn
import qualified Pictikz.Config as C

import System.Environment
import System.IO
import Data.List
import Text.Read

appname = "pictikz"
appVersion = "2.7.0.0"
applicense = "released under GPLv3"

data Action =
  Action
    { configFile    :: FilePath
    , svgInConfig   :: SVGIn.Configuration
    , tikzOutConfig :: TikzOut.Configuration
    , dotInConfig   :: DotIn.Configuration
    , orgConfig     :: O.Configuration
    , help          :: Bool
    , inF           :: FilePath
    , outputF       :: String -> IO ()
    , latexColors   :: Bool
    , version       :: Bool
    }

defaultPercent = 0.2
defaultAction = Action
  { configFile = ""
  , svgInConfig = SVGIn.noConfiguration
  , dotInConfig = DotIn.noConfiguration
  , tikzOutConfig = TikzOut.noConfiguration
  , orgConfig = O.noConfiguration
  , help = False
  , inF = ""
  , outputF = putStrLn
  , latexColors = False
  , version = False
  }

parseArgs action args = case args of
  "--colors" :f:r -> parseArgs (action{svgInConfig = (svgInConfig action){SVGIn.confColours = Just $ Right f}}) r
  "--colours":f:r -> parseArgs (action{svgInConfig = (svgInConfig action){SVGIn.confColours = Just $ Right f}}) r
  "-c"       :f:r -> parseArgs (action{svgInConfig = (svgInConfig action){SVGIn.confColours = Just $ Right f}}) r
  "--config" :f:r -> parseArgs (action{configFile = f}) r
  "-h"    :r  -> parseArgs (action{help = True}) r
  "--help":r  -> parseArgs (action{help = True}) r
  "-v"       :r -> parseArgs (action{version = True}) r
  "--version":r -> parseArgs (action{version = True}) r
  "-o"      :f:r  -> parseArgs (action{outputF = writeFile f}) r
  "--output":f:r  -> parseArgs (action{outputF = writeFile f}) r
  "-f"   :ws:hs:r  ->
    let w = read ws
        h = read hs
    in parseArgs (action{orgConfig = (orgConfig action){O.confPreScale = Just $ O.Fit (w,h)}}) r
  "--fit":ws:hs:r  ->
    let w = read ws
        h = read hs
    in parseArgs (action{orgConfig = (orgConfig action){O.confPreScale = Just $ O.Fit (w,h)}}) r
  "--min-dist":xs:ys:r ->
    let x = read xs
        y = read ys
    in parseArgs (action{orgConfig = (orgConfig action){O.confPreScale = Just $ O.MinDist (x,y)}}) r
  "--latex-colours":r -> parseArgs action{latexColors = True} r
  "--latex-colors" :r -> parseArgs action{latexColors = True} r
  "--rename":r    -> parseArgs action{svgInConfig = (svgInConfig action){SVGIn.confRenameNodes = Just True}} r
  "--no-rename":r -> parseArgs action{svgInConfig = (svgInConfig action){SVGIn.confRenameNodes = Just False}} r
  "-s"     :ws:hs:r  ->
    let w = read ws
        h = read hs
    in parseArgs (action{orgConfig = (orgConfig action){O.confPreScale = Just $ O.Plain (w,h) }}) r
  "--scale":ws:hs:r  ->
    let w = read ws
        h = read hs
    in parseArgs (action{orgConfig = (orgConfig action){O.confPreScale = Just $ O.Plain (w,h) }}) r
  "--text-as-nodes":r -> parseArgs action{svgInConfig = (svgInConfig action){SVGIn.confTextAsNodes = Just True}} r
  "-u"       :r   -> parseGrouping (Just . O.Cluster) action r
  "--uniform":r   -> parseGrouping (Just . O.Cluster) action r
  "-t"        :r  -> parseTemporal action r
  "--temporal":r  -> parseTemporal action r
  "--standalone":r -> parseArgs action{tikzOutConfig = (tikzOutConfig action){TikzOut.confStandalone = Just True}} r
  "-g"    :r      -> parseGrouping (Just . O.Regular) action r
  "--grid":r      -> parseGrouping (Just . O.Regular) action r
  f:r   -> parseArgs action{inF = f} r
  [] -> action
  where
    parseGrouping g action [] = action{orgConfig = (orgConfig action){ O.confNodeGrouping = g (defaultPercent, defaultPercent)}}
    parseGrouping g action (f:r) =
      let ps = readMaybe f :: Maybe Double
      in case ps of
        Just x -> parseArgs (action{orgConfig = (orgConfig action){O.confNodeGrouping = g (x,x) }}) r
        Nothing -> parseArgs (action{orgConfig = (orgConfig action){O.confNodeGrouping = g (defaultPercent, defaultPercent) }}) (f:r)
    parseTemporal action (st0:r) =
      let t0 = readMaybe st0 :: Maybe Int
      in case t0 of
        Just t  -> parseArgs (action{svgInConfig = (svgInConfig action){ SVGIn.confStartTime = Just t}}) r
        Nothing -> parseArgs (action{svgInConfig = (svgInConfig action){ SVGIn.confStartTime = Just 1}}) (st0:r)
    parseTemporal action [] = action

showHelp =
  mapM_ putStrLn
    [ appname ++ " " ++ appVersion ++ ", " ++ applicense
    , "usage: pictikz [OPTION...] <FILE>"
    , ""
    , "where"
    , " OPTION:"
    , "  -c, --colours <FILE>         Load colour definitions from FILE. See manpage for more information."
    , "      --config <FILE>          Load configuration form FILE."
    , "  -f, --fit WIDTH HEIGHT       Fit coordinates into a box of size WIDTH x HEIGHT without"
    , "                             changing aspect ratio."
    , "  -g, --grid [PERCENT]         Fit coordinates into a grid (implies --uniform [PERCENT])."
    , "                             By default PERCENT = " ++ show (floor $ defaultPercent * 100) ++ "."
    , "  -h, --help                   Show help."
    , "      --latex-colours          Output colours in LaTeX."
    , "      --min-dist X Y           Scale coordinates such that the minimum distance in the x-axis is X"
    , "                             and the minimum distance in the y-axis is Y."
    , "  -o, --output FILE            Write output into FILE instead of stdout."
    , "      --rename                 Rename vertices to v1, v2,... (default)."
    , "      --no-rename              Do not rename vertices, using the same IDs as in the SVG file."
    , "  -s, --scale WIDTH HEIGHT     Scale coordinates into a box of size WIDTH x HEIGHT."
    , "  -t, --temporal [START]       Treat SVG layers as frames, using overlay specifications in the output."
    , "      --text-as-nodes          Treat text as individual nodes instead of labels."
    , "      --standalone             Output standalone Tikz, ready to compile."
    , "  -u, --uniform [PERCENT]      Group coordinates by distance. Maximum distance for grouping"
    , "                             is PERCENT of the axis in question."
    , "  -v, --version                Output version and exit."
    ]

showVersion = putStrLn $ appname ++ " " ++ appVersion ++ " " ++ applicense

defaultColors =
  [ (RGB    0    0    0, "pictikz-black")
  , (RGB  255  255  255, "pictikz-white")
  , (RGB  128  128  128, "pictikz-gray")
  , (RGB  255   83   83, "pictikz-red")
  , (RGB  101  237  101, "pictikz-green")
  , (RGB  123  168  255, "pictikz-blue")
  , (RGB  246  246   82, "pictikz-yellow")
  , (RGB   87  239  239, "pictikz-cyan")
  , (RGB  225  103  255, "pictikz-purple")
  , (RGB  178  255   66, "pictikz-light-green")
  , (RGB  255  166   61, "pictikz-orange")
  , (RGB  255  141  255, "pictikz-pink")
  ]

updateConfig conf action = do
  svgConf  <- SVGIn.parseConfig conf
  dotConf  <- DotIn.parseConfig conf
  tikzConf <- TikzOut.parseConfig conf
  orgConf  <- O.parseConfig conf
  return $ action{
      svgInConfig   = SVGIn.updateConfig   svgConf  (svgInConfig action)
    , dotInConfig   = DotIn.updateConfig   dotConf  (dotInConfig action)
    , tikzOutConfig = TikzOut.updateConfig tikzConf (tikzOutConfig action)
    , orgConfig     = O.updateConfig       orgConf  (orgConfig action)
    }

execute action
  | help action      = showHelp
  | version action   = showVersion
  | configFile action /= "" = do
    conf <- C.loadConfig (configFile action)
    let conf' = updateConfig conf action{configFile = ""}
    case conf' of
      Left errors -> mapM_ putStrLn errors
      Right config -> execute config
  | inF action == "" = showHelp
  | ".gv" `isSuffixOf` (inF action) = do
    dotF <- readFile $ inF action
    let eUniverse  = DotIn.load dotF (dotInConfig action)
    case eUniverse of
      Right universe -> do
        let universe' = O.organize (orgConfig action) universe
        outputF action $ TikzOut.output universe' (tikzOutConfig action)
      Left msg -> hPutStrLn stderr msg
  | otherwise = do
    svg <- readFile $ inF action
    let universe  = SVGIn.load svg (svgInConfig action)
        universe' = O.organize (orgConfig action) universe
    outputF action $ TikzOut.output universe' (tikzOutConfig action)
  where
    -- toLatex (color, cname) = "\\definecolor{" ++ cname ++"}" ++ (TikzOut.localDraw TikzOut.defaultConfiguration) color
    -- shiftTime t (t0, t1) = (t0 + t, t1 + t)

main :: IO ()
main = do
  args <- getArgs
  let action = parseArgs defaultAction args
  execute action

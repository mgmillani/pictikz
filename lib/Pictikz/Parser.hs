--  Copyright 2017-2020 Marcelo Garlet Millani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.Parser where

import Control.Monad.Trans.State
import Control.Monad
import Data.Char
import Text.Read (readMaybe, readEither)

import Pictikz.Elements

splitBy :: (Char -> Bool) -> String -> [String]
splitBy _ [] = []
splitBy p xs =
  let (w, r) = break p (dropWhile p xs)
  in w : splitBy p r

consumeWhile :: (Char -> Bool) -> State String String
consumeWhile f = do
  str <- get
  let (x,r) = span f str
  put r
  return x

whenNotEmpty e x = do
  s <- get
  if s == "" then return e else x

readHexa xyz =
  let hexa = map digitToInt xyz
  in case hexa of
    [xh,xl,yh,yl,zh,zl] -> (xh*16 + xl, yh*16 + yl, zh*16 + zl)
    [x,y,z] -> (x*17, y*17, z*17)
readColor ('#':color) =
  let (r,g,b) = readHexa color
  in RGB r g b

onParseError newMsg = either (\oldMsg -> Left $ concat ["while parsing \"", newMsg, "\","] : oldMsg) Right

parseError parsing found expected = Left [concat ["Error parsing \"", parsing, "\";\n found \"", found, "\",\n expected \"", expected, "\"."]]

readIntDoubleRange minV maxV v =
  let vI = readMaybe v :: Maybe Integer
      vD = readMaybe v :: Maybe Double
      v' = case (vI, vD) of
        (Nothing, Just x) -> Just $ ceiling $ minV + maxV * x
        (a, _) -> a
  in case v' of
    Nothing -> Left [ concat ["value \"", v, "\" is outside the range ", show minV, "-", show maxV, "."] ]
    Just x -> Right x

readQuotedString :: State String String
readQuotedString = do
  ss <- whenNotEmpty "" get
  case (ss) of
    '\\':x:rs -> do
      put rs
      s1 <- readQuotedString
      return $ x:s1
    '"':rs -> do
      put rs
      return ""
    x:rs -> do
      put rs
      s1 <- readQuotedString
      return $ x:s1
    "" -> do
      return ""

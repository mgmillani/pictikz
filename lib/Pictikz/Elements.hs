--  Copyright 2017-2019 Marcelo Garlet Millani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.Elements where

import qualified Pictikz.Geometry as Ge
import qualified Pictikz.Text  as T
import           Pictikz.Utils

import Data.Matrix
import qualified Data.Matrix as Ma
import qualified Data.Array.IArray as A
import qualified Data.Map as M
import Data.Fixed

-- RGB color with one byte per channel (0 to 255).
data Color = RGB Int Int Int | NamedColor String deriving (Eq, Show)

rgbDist (RGB r0 g0 b0) (RGB r1 g1 b1) = sqrt $ dr*dr + dg*dg + db*db
  where
    dr = fromIntegral $ r1 - r0
    dg = fromIntegral $ g1 - g0
    db = fromIntegral $ b1 - b0

-- | Convert HSL to RGB. Use formula from Wikipedia.
fromHSL h s l =
  case h' of
    0 -> RGB c x m
    1 -> RGB x c m
    2 -> RGB m c x
    3 -> RGB m x c
    4 -> RGB x m c
    5 -> RGB c m x
  where
    c' = (1 - abs (2*l - 1)) * s
    (h', h'') = properFraction $ h / 60
    m = round $ 255 * (l - 0.5*c')
    x = round ( 255 * c' * (1 - abs ( h'' - 1))) + m
    c = round ( 255 * c') + m

defaultColors =
  [ (RGB    0    0    0, "black")
  , (RGB  255  255  255, "white")
  , (RGB  128  128  128, "gray")
  , (RGB  255   83   83, "red")
  , (RGB  101  237  101, "green")
  , (RGB  123  168  255, "blue")
  , (RGB  246  246   82, "yellow")
  , (RGB   87  239  239, "cyan")
  , (RGB  225  103  255, "purple")
  , (RGB  178  255   66, "light-green")
  , (RGB  255  166   61, "orange")
  , (RGB  255  141  255, "pink")
  ]

data ArrowType = ArrowTo | ArrowFrom | ArrowNone | ArrowBoth deriving (Eq, Ord, Enum)
joinArrow a ArrowNone = a
joinArrow ArrowBoth b = ArrowBoth
joinArrow ArrowTo ArrowFrom = ArrowBoth
joinArrow ArrowFrom ArrowTo = ArrowBoth
joinArrow a b = b

instance Show ArrowType where
  show ArrowTo   = "->"
  show ArrowFrom = "<-"
  show ArrowBoth = "<->"
  show ArrowNone = ""

data Position =
    Around Double Double -- | Radius (distance) and angle.
  | OnSegment Int Double -- | Index of segment and angle.
  deriving (Eq, Show)

data Style a =
  Style
  { dashPattern   :: Maybe [a]
  , thickness     :: Maybe a
  , fillColor     :: Maybe Color
  , strokeColor   :: Maybe Color
  , alignment     :: Maybe T.AlignmentType
  , arrow         :: Maybe ArrowType
  , label         :: Maybe [[T.Text]]
  , labelPosition :: Maybe [Position] -- In polar coordinates or as reference to a segment of a line.
  , time          :: Maybe (Maybe Int, Maybe Int)
  }
  deriving (Show, Eq)

noStyle = Style
  { alignment     = Nothing
  , arrow         = Nothing
  , dashPattern   = Nothing
  , fillColor     = Nothing
  , label         = Nothing
  , labelPosition = Nothing
  , strokeColor   = Nothing
  , thickness     = Nothing
  , time          = Nothing
  }

updateStyle old new = 
  Style 
  { dashPattern = combineMaybe (dashPattern old) (dashPattern new)
  , thickness = combineMaybe (thickness old) (thickness new)
  , fillColor = combineMaybe (fillColor old) (fillColor new)
  , strokeColor = combineMaybe (strokeColor old) (strokeColor new)
  , alignment = combineMaybe (alignment old) (alignment new)
  , arrow = combineMaybe (arrow old) (arrow new)
  , label = combineMaybe (label old) (label new)
  , labelPosition = combineMaybe (labelPosition old) (labelPosition new)
  , time = combineMaybe (time old) (time new)
  }

class Similar a where
  similar :: a -> a -> Bool

data CurveSegment a = 
    Cubic (a,a) (a,a) -- ^ Control points at begin and end of the segment (relative to corresponding point on segment)
  | Arc
    (a,a) -- ^ (dc/d, theta): center of the ellipse in polar coordinates relative to midpoint of the segment between both endpoints, which has lenght d
    a     -- ^ Ellipse rotation relative to normal of the line between both endpoints
    Bool  -- ^ Take long arc of the ellipse
  | Line deriving (Eq, Show)

isArc (Arc _ _ _) = True
isArc _ = False
isCubic (Cubic _ _) = True
isCubic _ = False

arcFromEndpoints
  (x1, y1)
  (x2, y2)
  (rx,ry) phi largeArc sweep = Arc (dc / d, theta) phi largeArc
  where
    d = Ge.distance (x1,y1) (x2,y2)
    xm = (x1 + x2) / 2
    ym = (y1 + y2) / 2
    dc = Ge.distance (xm,ym) (cx, cy)
    -- angle between normal of the segment (x1,y1) -- (x2,y2) and the segment going to ellipse center
    theta = Ge.vectorAngle d (y1 - y2, x2 - x1) dc (cx - xm, cy - ym)
    -- convert to center parametrization
    [x1', y1', _] = Ma.toList $ Ge.rotate phi * (Ma.fromList 3 1 [ (x1 - x2) / 2, (y1 - y2) / 2, 1 ])
    rx2 = rx*rx
    ry2 = ry*ry
    x1'2 = x1'*x1'
    y1'2 = y1'*y1'
    a = sqrt $ (rx2*rx2 - rx2 * y1'2 - ry2 * x1'2) / (rx2 * y1'2 + ry2 * x1'2)
    b = rx * y1' / ry
    c = -(ry * x1' / rx)
    cx'
      | largeArc == sweep = (-a) * b
      | otherwise = a * b
    cy'
      | largeArc == sweep = (-a) * c
      | otherwise = a * c
    [cx, cy, _] = Ma.toList $ Ge.rotate phi * (Ma.fromList 3 1 [cx', cy', 1]) + (Ma.fromList 3 1 [(x1 + x2) / 2, (y1 + y2) / 2, 1])

arcAngles (x1, y1) (x2, y2) (Arc (dc, theta) phi largeArc) =
  (theta1, theta2, rx, ry)
  where
    d = Ge.distance (x1,y1) (x2,y2)
    xm = (x1 + x2) / 2
    ym = (y1 + y2) / 2
    -- normal vector with length equal to the distance between (xm,ym) and (cx,cy)
    normalX = (y1 - y2) * dc
    normalY = (x2 - x1) * dc
    [nx,ny,_] = Ma.toList $ Ge.rotate theta * Ma.fromList 3 1 [normalX, normalY, 1]
    cx = xm + nx
    cy = ym + ny
    -- make (cx,cy) the origin of the coordinate system and then rotate points by phi
    [x1', x2', y1', y2', _, _] = Ma.toList $ Ge.rotate phi * Ma.fromList 3 2 [x1 - cx, x2 - cx, y1 - cy, y2 - cy, 1, 1]
    -- compute rx and ry by using the ellipse equation x^2/rx^2 + y^2/ry^2 = 1
    Just (a,b) = Ge.solveLinearSystem2 (x1'*x1') (y1'*y1') 1 (x2'*x2') (y2'*y2') 1
    rx = sqrt $ 1 / a
    ry = sqrt $ 1 / b
    -- compute angles
    -- (x1',y1') = (rx * cos theta1, ry * sin theta1)
    theta1 = (if y1' < 0 then -1 else 1) * acos (x1' / rx)
    theta2' = (if y2' < 0 then -1 else 1) * acos (x2' / rx)
    theta2
      | largeArc && (theta1 - theta2' `mod'` 2*pi) < pi = theta2' - 2*pi
      | not largeArc && (theta1 - theta2' `mod'` 2*pi) > pi = theta2' - 2 * pi
      | otherwise = theta2'

isCubicSegment (Cubic _ _) = True
isCubicSegment _           = False
isLineSegment Line = True
isLineSegment _    = False
isArcSegment (Arc _ _ _) = True
isArcSegment _           = False

evalSegment (x0,y0) (x1,y1) Line t = (x1 * t + x0 * (1 - t), y1 * t + y0 * (1 - t))
evalSegment (x0,y0) (x3,y3) (Cubic (x1,y1) (x2,y2)) t  = 
  (a * x0 + b*x1 + c*x2 + d*x3
  ,a * y0 + b*y1 + c*y2 + d*y3)
  where
    t1 = 1 - t
    t12 = t1 * t1
    t13 = t12 * t1
    a = t13
    b = 3 * t * t12
    c = 3 * t * t * t1
    d = t * t * t

data Id = Id Int String deriving (Eq, Show, Ord)
data Coordinate a = ElementRef Int | Point (a, a) deriving (Eq, Show, Ord)
data Drawing a =
    PolyLine  [Coordinate a]
  | Curve     [Coordinate a] [CurveSegment a]
  | Shape     (Coordinate a) (Ge.Shape a)
  | Paragraph (Coordinate a) [T.Text]
  deriving (Eq, Show)
data Element a =
    Element   Id (Drawing a)   (Style a) (M.Map String (Maybe String))
  | Container Id [Int] (Style a) (M.Map String (Maybe String))
  deriving (Eq, Show)
data Universe a = 
  Universe
  { uOrigin :: (a, a) -- | The position of all elements is relative to this origin.
  , uScale  :: (a, a) -- | Scale of each axis.
  , uAttributeScale :: a -- | Scale of each attribute.
  , uSize   :: (a, a) -- | Range of the coordinates of the elements (before applying scale).
  , uElements :: A.Array Int (Element a) -- | All elements of the universe.
  }
  deriving (Eq, Show)

isElement (Element _ _ _ _) = True
isElement _ = False
isShape (Shape _ _) = True
isShape _ = False
isLine (PolyLine _) = True
isLine (Curve _ _) = True
isLine _ = False
isPoint (Point _) = True
isPoint _ = False

fromPoint (Point (x,y)) = (x,y)

class Positionable t where
  getPos :: t a -> Coordinate a
  fPos :: (Coordinate a -> Coordinate a) -> t a -> t a
  setPos :: Coordinate a -> t a -> t a
  setPos p x = fPos (\_ -> p) x

instance Positionable Coordinate where
  getPos c = c
  fPos f c = f c

instance Positionable Drawing where
  getPos (PolyLine coords) = head coords
  getPos (Curve coords _) = head coords
  getPos (Shape p _) = p
  getPos (Paragraph p _) = p
  fPos f (PolyLine coords) = PolyLine $ map f coords
  fPos f (Curve coords segs) = Curve (map f coords) segs
  fPos f (Shape p shape) = Shape (f p) shape
  fPos f (Paragraph p text) = Paragraph (f p) text

instance Positionable Element where
  getPos (Element _ drawing _ _) = getPos drawing
  getPos (Container _ elements _ _) = undefined
  fPos f (Element eid drawing st atts) = Element eid (fPos f drawing) st atts
  fPos f (Container eid els st atts) = Container eid els st atts

getAbsolutePos universe e = 
  case e of
    Point p -> p
    ElementRef i -> getAbsolutePos universe $ getPos $ (uElements universe) A.! i

class Temporal a where
  getTime :: a -> (Int, Int)
  fTime   :: ((Int, Int) -> (Int, Int)) -> a -> a

getStyle (Element _ _ st _) = st
getStyle (Container _ _ st _) = st
setStyle (Element eid d st att) st' = Element eid d st' att
setStyle (Container eid els st att) st' = Container eid els st' att
getAttributes (Element _ _ _ att) = att
getAttributes (Container _ _ _ att) = att
setAttributes (Element eid d st _) att = Element eid d st att
setAttributes (Container eid els st _) att = Container eid els st att
getId (Element eid _ _ _) = eid
getId (Container eid _ _ _) = eid

transformCoordinate matrix (Point (x,y)) = 
  let [x', y', _] = toList $ matrix * (fromList 3 1 [x,y,1])
  in Point (x', y')
transformCoordinate _ pos = pos

transformControlPoint _ Line = Line
transformControlPoint matrix (Cubic (x1, y1) (x2, y2)) = 
  let [x1', y1', _] = toList $ matrix * (fromList 3 1 [x1,y1,1])
      [x2', y2', _] = toList $ matrix * (fromList 3 1 [x2,y2,1])
  in Cubic (x1', y1') (x2', y2')

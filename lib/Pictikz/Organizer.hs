--  Copyright 2017-2020 Marcelo Garlet Millani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.Organizer where

import qualified Data.Set as S
import qualified Data.Map as M
import qualified Pictikz.Geometry as Ge
import Pictikz.Elements hiding (updateStyle)
import Pictikz.Parser
import Pictikz.Utils
import Pictikz.Clustering
import Data.List
import Data.Maybe
import Data.Either
import Control.Monad
import Control.Applicative
import Data.Char
import Text.Read (readEither, readMaybe)
import qualified Data.DescriLo as De
import Data.Matrix as Mx
import qualified Data.Array.IArray as A
import qualified Data.Array.Unboxed as U


--TODO: Add a grouping option where an infinite fixed sequence is informed in the style [v0, v0+d..]
data Grouping a b = Regular a | Cluster a | Fixed [(String, b)] deriving (Eq, Show)
data Translation a = Origin a | Shift a deriving (Eq, Show)
data Scaling a  = Plain a | MinDist a | Fit a | Clip a a | Range a a deriving (Eq, Show)
data CoordinateSystem = Polar deriving (Eq, Show)

data Configuration = Configuration
  { confPreScale          :: Maybe (Scaling (Double, Double))
  , confPostScale         :: Maybe (Scaling (Double, Double))
  , confRotate            :: Maybe Double
  , confPreTranslate      :: Maybe (Translation (Double, Double))
  , confPostTranslate     :: Maybe (Translation (Double, Double))
  , confNodeGrouping      :: Maybe (Grouping (Double, Double) (Double, Double))
  , confCurveGrouping     :: Maybe (Grouping (Double, Double) (Double, Double))
  , confCoordinateSystem  :: Maybe CoordinateSystem
  , confThicknessScale    :: Maybe (Scaling Double)
  , confThicknessGrouping :: Maybe (Grouping Double Double)
  , confDashedScale       :: Maybe (Scaling Double)
  , confDashedGrouping    :: Maybe (Grouping Double [Double])
  } deriving (Show)

defaultConfiguration = Configuration
  { confPreScale          = Nothing
  , confPostScale         = Nothing
  , confRotate            = Nothing
  , confPreTranslate      = Nothing
  , confPostTranslate     = Nothing
  , confNodeGrouping      = Nothing
  , confCurveGrouping     = Nothing
  , confCoordinateSystem  = Nothing
  , confThicknessScale    = Nothing 
  , confThicknessGrouping = Nothing 
  , confDashedScale       = Nothing 
  , confDashedGrouping    = Nothing 
  }

noConfiguration = defaultConfiguration

updateConfig old new =
  Configuration
  { confPreScale = combineMaybe (confPreScale old) (confPreScale new)
  , confPostScale = combineMaybe (confPostScale old) (confPostScale new)
  , confRotate = combineMaybe (confRotate old) (confRotate new)
  , confPreTranslate = combineMaybe (confPreTranslate old) (confPreTranslate new)
  , confPostTranslate = combineMaybe (confPostTranslate old) (confPostTranslate new)
  , confNodeGrouping = combineMaybe (confNodeGrouping old) (confNodeGrouping new)
  , confCurveGrouping = combineMaybe (confCurveGrouping old) (confCurveGrouping new)
  , confCoordinateSystem = combineMaybe (confCoordinateSystem old) (confCoordinateSystem new)
  , confThicknessScale = combineMaybe (confThicknessScale old) (confThicknessScale new)
  , confThicknessGrouping = combineMaybe (confThicknessGrouping old) (confThicknessGrouping new)
  , confDashedScale = combineMaybe (confDashedScale old) (confDashedScale new)
  , confDashedGrouping = combineMaybe (confDashedGrouping old) (confDashedGrouping new)
  }

parseFixed [] = return []
parseFixed (name:values) = do
  let (vs, rs') = span (\s -> isRight $ (readEither' s :: Either [String] Double)) values
  if null vs then
    Left ["found \"" ++ (concat (name:values)) ++ "\", expected <name> <value> ..."]
  else do
    v <- mapM readEither' vs :: Either [String] [Double]
    vs' <- parseFixed rs'
    return $ (name, v) : vs'

parseConfig :: [De.Description] -> Either [String] Configuration
parseConfig conf = foldM parseConfig' noConfiguration orgD
  where
    orgD = concatMap De.values $ filter (\c -> map toLower (De.name c) == "organizer") conf
    parseConfig' cf (attr, value) = onParseError (attr ++ " = " ++ value) $ 
      case attr of
        "pre scale" -> case map (map toLower) $ words value of
          ["plain", xStr,yStr] -> do
            x <- readEither' xStr
            y <- readEither' yStr
            return cf{confPreScale = Just $ Plain (x,y)}
          ["min", "dist", xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            return cf{confPreScale = Just $ MinDist (x,y)}
          ["fit", xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            return cf{confPreScale = Just $ Fit (x,y)}
          x -> parseError attr value "<plain | min dist | fit> <number> <number>"
        "post scale" -> case map (map toLower) $ words value of
          ["plain", xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            return cf{confPostScale = Just $ Plain (x,y)}
          ["min", "dist", xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            return cf{confPostScale = Just $ MinDist (x,y)}
          ["fit", xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            return cf{confPostScale = Just $ Fit (x,y)}
          x -> parseError attr value "<plain | min dist | fit> <number> <number>"
        "rotate" -> case words $ map toLower value of
          [degStr, "degrees"] -> do
            deg <- either (\x -> Left [x]) Right $ readEither degStr
            return $ cf{confRotate = Just (deg * pi / 180)}
          [radStr, "radians"] -> do
            rad <- either (\x -> Left [x]) Right $ readEither radStr
            return $ cf{confRotate = Just rad}
          x -> parseError attr value "<angle> degrees | <angle> radians"
        "pre translate" -> case words value of
          [typeStr, xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            case map toLower typeStr of
              "shift" -> return cf{confPreTranslate = Just $ Shift (x,y)}
              "origin" -> return cf{confPreTranslate = Just $ Origin (x,y)}
              _ -> parseError attr value "origin <number> <number> | shift <number> <number>"
          x -> parseError attr value "origin <number> <number> | shift <number> <number>"
        "post translate" -> case words value of
          [typeStr, xStr,yStr] -> do
            x <- either (\x -> Left [x]) Right $ readEither xStr
            y <- either (\x -> Left [x]) Right $ readEither yStr
            case map toLower typeStr of
              "shift" -> return cf{confPostTranslate = Just $ Shift (x,y)}
              "origin" -> return cf{confPostTranslate = Just $ Origin (x,y)}
              _ -> parseError attr value "origin <number> <number> | shift <number> <number>"
          x -> parseError attr value "origin <number> <number> | shift <number> <number>"
        "node grouping" -> case map (map toLower) $ words value of
          ["regular", percentStr] -> do
            percent <- either (\x -> Left [x]) Right $ readEither percentStr :: Either [String] Double
            return cf{confNodeGrouping = Just $ Regular (percent / 100, percent / 100)}
          ["cluster", percentStr] -> do
            percent <- either (\x -> Left [x]) Right $ readEither percentStr :: Either [String] Double
            return cf{confNodeGrouping = Just $ Cluster (percent / 100, percent / 100)}
          ["none"] -> Right cf{confNodeGrouping = Nothing}
          x -> parseError attr value "none | regular <percent>| cluster <percent>"
        "curve grouping" -> case map (map toLower) $ words value of
          ["regular", percentStr] -> do
            percent <- either (\x -> Left [x]) Right $ readEither percentStr :: Either [String] Double
            return cf{confCurveGrouping = Just $ Regular (percent / 100, percent / 100)}
          ["cluster", percentStr] -> do
            percent <- either (\x -> Left [x]) Right $ readEither percentStr :: Either [String] Double
            return cf{confCurveGrouping = Just $ Cluster (percent / 100, percent / 100)}
          ["none"] -> Right cf{confCurveGrouping = Nothing}
          x -> parseError attr value "none | regular <percent>| cluster <percent>"
        "coordinate system" -> case map toLower value of
          "cartesian" -> Right $ cf{confCoordinateSystem = Nothing}
          "polar" -> Right $ cf{confCoordinateSystem = Just Polar}
          x -> parseError attr x "cartesian | polar"
        "thickness scale" -> case map (map toLower) $ words value of
          "range":minV:maxV:[] -> do
            v0 <- readEither' minV :: Either [String] Double
            v1 <- readEither' maxV :: Either [String] Double
            return $ cf{confThicknessScale = Just $ Range v0 v1}
          "clip":minV:maxV:[] -> do
            v0 <- readEither' minV :: Either [String] Double
            v1 <- readEither' maxV :: Either [String] Double
            return $ cf{confThicknessScale = Just $ Clip v0 v1}
          "min dist":d:[] -> do
            v0 <- readEither' d :: Either [String] Double
            return $ cf{confThicknessScale = Just $ MinDist v0}
          x -> parseError attr value "range <min> <max> | clip <min> <max> | min dist <d>"
        "dashed scale" -> case map (map toLower) $ words value of
          "range":minV:maxV:[] -> do
            v0 <- readEither' minV :: Either [String] Double
            v1 <- readEither' maxV :: Either [String] Double
            return $ cf{confDashedScale = Just $ Range v0 v1}
          "clip":minV:maxV:[] -> do
            v0 <- readEither' minV :: Either [String] Double
            v1 <- readEither' maxV :: Either [String] Double
            return $ cf{confDashedScale = Just $ Clip v0 v1}
          "min dist":d:[] -> do
            v0 <- readEither' d :: Either [String] Double
            return $ cf{confDashedScale = Just $ MinDist v0}
          x -> parseError attr value "range <min> <max> | clip <min> <max> | min dist <d>"
        "thickness grouping" -> case map (map toLower) $ words value of
          "fixed":vs -> do
            fixedList <- parseFixed $ tail $ words value
            return $ cf{confThicknessGrouping = Just $ Fixed $ map (\(n,vs) -> (n, head vs)) fixedList}
          "cluster":percentStr:[] -> do
            percent <- readEither' percentStr :: Either [String] Double
            return $ cf{confThicknessGrouping = Just $ Cluster $ percent / 100}
          "regular":percentStr:[] -> do
            percent <- readEither' percentStr :: Either [String] Double
            return $ cf{confThicknessGrouping = Just $ Regular $ percent / 100}
          ["none"] -> Right cf{confThicknessGrouping = Nothing}
          x -> parseError attr value "none | uniform <percent> | regular <percent> | fixed <name> <value> [<name> <value>..."
        "dashed grouping" -> case map (map toLower) $ words value of
          "fixed":vs -> do
            fixedList <- parseFixed $ tail $ words value
            return $ cf{confDashedGrouping = Just $ Fixed fixedList}
          "cluster":percentStr:[] -> do
            percent <- readEither' percentStr :: Either [String] Double
            return $ cf{confDashedGrouping = Just $ Cluster $ percent / 100}
          "regular":percentStr:[] -> do
            percent <- readEither' percentStr :: Either [String] Double
            return $ cf{confDashedGrouping = Just $ Regular $ percent / 100}
          ["none"] -> Right cf{confDashedGrouping = Nothing}
          x -> parseError attr value "none | cluster <percent> | regular <percent> | fixed <name> <value> [<name> <value>..."
        x -> parseError "Organizer" x "pre scale | post scale | rotate | pre translate | post translate | node grouping | coordinate system | thickness scale | thickness grouping | dashed scale | dashed grouping"

-- | Computes the mininmum distance greater than zero between objects in each axis.
minDist objects =
  let positions = [(x,y) | o <- objects, let Point (x,y) = getPos o, isPoint $ getPos o]
      xs = sort $ map fst positions
      ys = sort $ map snd positions
      minDistXs = filter (> 0.0) $ zipWith (flip (-)) xs (tail xs)
      minDistYs = filter (> 0.0) $ zipWith (flip (-)) ys (tail ys)
      minDX = if null minDistXs then 0 else minimum minDistXs
      minDY = if null minDistYs then 0 else minimum minDistYs
  in (minDX, minDY)


clusterCoordinates (x0,y0,x1,y1) px py clusterX clusterY objects = 
  let dx = max (px * (x1 - x0)) (px * px * (y1 - y0))
      dy = max (py * (y1 - y0)) (py * py * (x1 - x0))
      (points, noPoints) = partition (isPoint . getPos) objects
      (_,xi,n) = numberAttributes (\o -> (1 :: Int, [fst $ fromPoint $ getPos o ])) points
      (_,yi,_) = numberAttributes (\o -> (1 :: Int, [snd $ fromPoint $ getPos o ])) points
      (xArr, cx, xVals) = clusterValues dx clusterX n xi
      (yArr, cy, yVals) = clusterValues dy clusterY n yi
  in ( noPoints ++ [ setPos (Point (x,y)) o
       | (i, o) <- zip (map fst xi) points
       , let x = xArr U.! (i :: Int)
       , let y = yArr U.! i]
     , cx, cy, xVals, yVals)


getCurveCoordinate f c = getCurveCoordinate' 0 ps
  where
    ps = case c of
      PolyLine ps -> ps
      Curve ps _ -> ps
    getCurveCoordinate' i (Point p : ps) = (n, f p:rs)
      where
        (n, rs) = getCurveCoordinate' (i+1) ps
    getCurveCoordinate' i (_ : ps) = getCurveCoordinate' i ps
    getCurveCoordinate' i [] = (i, [])

getCurveSegments (PolyLine _) = (0,[])
getCurveSegments (Curve _ cs) = getCurveSegments' 0 cs
  where
    getCurveSegments' i (Line:cs) = getCurveSegments' i cs
    getCurveSegments' i (c:cs) = (n, c:rs)
      where
        (n, rs) = getCurveSegments' (i+1) cs
    getCurveSegments' i [] = (i, [])

clusterCurves (x0,y0,x1,y1) px py fixedX fixedY clusterX clusterY curves = 
  updateCoordinates segmentArr updatePos curves ci si
  where
    dx = max (px * (x1 - x0)) (px * px * (y1 - y0))
    dy = max (py * (y1 - y0)) (py * py * (x1 - x0))
    curves' = [d | Element _ d _ _ <- curves]
    (si, segmentArr) = clusterSegments (max px py) curves'
    (ci, xi, n) = numberAttributes (getCurveCoordinate fst) curves'
    (_, yj, _)  = numberAttributes (getCurveCoordinate snd) curves'
    (ax, _, _) = clusterValuesWithPinned (x1 - x0) dx clusterX n (map (\(i,x) -> (i, Unbounded x)) xi) fixedX
    (ay, _, _) = clusterValuesWithPinned (y1 - y0) dy clusterY n (map (\(j,y) -> (j, Unbounded y)) yj) fixedY
    updatePos [] _ = []
    updatePos ((Point _) : ps) (i:is) = 
      let x = ax U.! (i :: Int)
          y = ay U.! (i :: Int)
      in Point (x, y) : updatePos ps is
    updatePos (x : ps) is = x : updatePos ps is

updateCoordinates ::
     U.Array Int (CurveSegment a)
  -> ([Coordinate a]
  -> [Int]
  -> [Coordinate a])
  -> [Element a]
  -> [[Int]]
  -> [[Int]]
  -> [Element a]
updateCoordinates _ _ [] _ _ = []
updateCoordinates segmentArr updatePos (Element eid curve style attrs:es) (i:is) (j:js) =
  Element eid curve' style attrs : updateCoordinates segmentArr updatePos es is js
  where
    curve' = case curve of
      PolyLine ps -> PolyLine $ updatePos ps i
      Curve ps ss -> Curve (updatePos ps i) (updateSegment segmentArr ss j)
      s -> s

updateSegment :: U.Array Int (CurveSegment a) -> [CurveSegment a] -> [Int] -> [CurveSegment a]
updateSegment _ [] _ = []
updateSegment segArr (Cubic _ _   : cs) (j:js) = segArr U.! (j :: Int) : updateSegment segArr cs js
updateSegment segArr (Arc _ _ _ : cs) (j:js) = segArr U.! j : updateSegment segArr cs js
updateSegment segArr (Line : cs) js = Line : updateSegment segArr cs js

-- TODO: Let each clustering function be configurable.
clusterSegments :: (Floating a, Real a, Floating a, Eq a, Ord a) => a -> [Drawing a] -> ([[Int]], U.Array Int (CurveSegment a))
clusterSegments p curves = (ci, segmentArr)
  where
    (ci, si, n) = numberAttributes getCurveSegments curves
    cubic = filter (isCubic . snd) si
    arcs = filter (isArc . snd) si
    --
    pi2 = pi/2
    ppi2 = p * pi / 2
    -- obtain values
    (ai, rotations, na) =
      numberAttributes (\(Arc _ phi _) -> (1, [Unbounded $ toClusterAngle phi])) $ map snd arcs
    (_, dcs, _) =
      numberAttributes (\(Arc (dc, _) _ _) -> (1, [dc])) $ map snd arcs
    (_, thetas, _) =
      numberAttributes (\(Arc (_, theta) _ _) -> (1, [Unbounded $ toClusterAngle theta])) $ map snd arcs
    (cbi, rhos, nc) =
      numberAttributes (\(Cubic (rho1, _) (rho2, _)) ->
                           (2, [rho1, rho2]))
                           $ map snd cubic
    (_, psis, _) =
      numberAttributes (\(Cubic (_, psi1) (_, psi2)) -> 
                           (2, [Unbounded $ toClusterAngle psi1, Unbounded $ toClusterAngle psi2]))
                       $ map snd cubic
    -- pinned angles for clustering
    pinnedAngles = map (*(pi/4)) [0,1,2]
    -- cluster arc attributes
    (rotArr, nRotationClusters, _) =
      clusterValuesWithPinned pi2 ppi2
                              regularAngleClustering
                              na rotations pinnedAngles
    dc0 = minimum $ map snd dcs
    dc1 = maximum $ map snd dcs
    rho0 = minimum $ map snd rhos
    rho1 = maximum $ map snd rhos
    (dcArr, _, _) = clusterValues (p * (dc1 - dc0)) simpleClustering na dcs
    (thetaArr, nThetaClusters, _) =
      clusterValuesWithPinned pi2 ppi2
                              regularAngleClustering
                              na thetas pinnedAngles
    -- cluster cubic Bézier attributes
    (rhoArr, nRhoClusters, _) =
      clusterValues (p * (rho1 -  rho0))
                    (regularClustering nRhoClusters rho0 rho1) nc rhos
    (psiArr, nPsiClusters, _) =
      clusterValuesWithPinned pi2 ppi2
                              regularAngleClustering nc psis pinnedAngles
    -- create final clustered segments
    segmentArr = U.array (0, n - 1 :: Int) $
         [ (i, Arc (dcArr U.! i, qTheta * pi/2 + thetaArr U.! i)
                   (qPhi * pi/2 + rotArr U.! i)
                   largeArc
           )
         | (i, a) <- arcs
         , let Arc (_, theta) phi largeArc = a
         , let qTheta = quadrant theta
         , let qPhi = quadrant phi
         ]
      ++ [ (i, Cubic (rhoArr U.! i1, qPsi1 * pi / 2 + psiArr U.! i1)
                     (rhoArr U.! i2, qPsi2 * pi / 2 + psiArr U.! i2))
         | (j, (i, Cubic (_, psi1) (_, psi2))) <- zip [0..] cubic
         , let i1 = 2*j
         , let i2 = i1 + 1
         , let qPsi1 = quadrant psi1
         , let qPsi2 = quadrant psi2
         ]

numberCoordinates f cs = numberCoordinates' 0 cs
  where
    numberCoordinates' i [] = ([], [], i)
    numberCoordinates' i (c:cs) = case c of
      Element eid (PolyLine ps) style attrs -> 
        let (j, ps') = numberPoints f i ps
            (ci, pi, n) = numberCoordinates' j cs
        in ([i..j-1] : ci, ps' ++ pi, n)
      Element eid (Curve ps _) style attrs -> 
        let (j, ps') = numberPoints f i ps
            (ci, pi, n) = numberCoordinates' j cs
        in ([i..j-1] : ci, ps' ++ pi, n)
      _ -> numberCoordinates' i cs

numberPoints _ i [] = (i, [])
numberPoints f i ((Point p):ps) =
  let (n, pr) = numberPoints f (i+1) ps
  in (n, (i, f p) : pr)
numberPoints f i (_:ps) = numberPoints f i ps

groupValues (Just (Regular d)) vs = clusterValuesBy isometricGroup d vs
groupValues (Just (Cluster d)) vs = clusterValuesBy (genGroup distanceGroup) d vs
groupValues Nothing vs = S.fromList vs

cartesianToPolar (Point (x,y)) =
  let r = sqrt $ x*x + y*y
      theta = if asin (y / r) >= 0 then acos $ x / r else (2*pi) - acos (x / r)
  in if r == 0 then Point (0,0) else Point (r, theta)
cartesianToPolar pos = pos

polarToCartesian (Point (r,theta)) = Point (r * cos theta, r * sin theta)
polarToCartesian pos = pos

scalePos universe (Point (x,y)) = Point (sx * x, sy * y)
  where
    (sx, sy) = uScale universe
scalePos _ pos = pos

fromCartesianCoordinates conf universe
  | confCoordinateSystem conf == Just Polar =
      let elements = map (fPos (cartesianToPolar . (scalePos universe))) $ A.elems $ uElements universe
          points = map (\(Point (x,y)) -> (x,y)) $ filter isPoint $ map getPos elements
          maxr = maximum $ map fst points
          phi1 = maximum $ map snd points
      in universe
      { uOrigin = let Point (ox,oy) = cartesianToPolar (Point $ uOrigin universe) in (ox,oy)
      , uScale = (1,1)
      , uSize = (maxr, phi1)
      , uElements = A.array (A.bounds $ uElements universe)
          [(i, e)
          | e <- elements
          , let Id i str = getId e]
      }
  | otherwise = universe

toCartesianCoordinates conf universe
  | confCoordinateSystem conf == Just Polar = 
    let elements = map (fPos (polarToCartesian . (scalePos universe))) $ A.elems $ uElements universe
        points = map (\(Point (x,y)) -> (x,y)) $ filter isPoint $ map getPos elements
        (x0, y0) = (minimum $ map fst points, minimum $ map snd points)
        (x1, y1) = (maximum $ map fst points, maximum $ map snd points)
        (w, h) = (x1 - x0, y1 - y0)
    in universe
        { uScale = (1,1)
        , uSize = (w,h)
        , uElements = A.array (A.bounds $ uElements universe) [(i, e) | e <- elements, let (Id i str) = getId e]  
        }
  | otherwise = universe

transformCoordinates cfTranslate cfRotate cfScale universe = 
  universe{
      uOrigin = uOrigin'
    , uScale = uScale'
    , uElements = (uElements universe) A.// [(i, el')
                                            | el <- A.elems $ uElements universe
                                            , let Id i str = getId el
                                            , let el' = fRotate $ fScale  el]
  }
  where
    (sx, sy) = uScale universe
    (w,h) = uSize universe
    (ox, oy) = uOrigin universe
    uOrigin' = 
        case cfTranslate of
          Nothing -> (ox, oy)
          Just (Origin (cx, cy)) -> (cx, cy)
          Just (Shift (dx, dy)) -> (dx + ox, dy + oy)
    uScale' = case cfScale of
      Just (Plain (sx', sy')) ->
        let sx'' = sx' / w
            sy'' = sy' / h
        in (sx'', sy'')
      Just (Fit (bx, by))  -> 
        let sx' = bx / w
            sy' = by / h
            s = min sx' sy'
        in (s, s)
      Just (Clip (x0, y0) (x1, y1)) -> ((x1 - x0)/w, (y1 - y0)/h)
      Just (MinDist (mx, my)) -> 
        let (mdx, mdy) = minDist $ A.elems $ uElements universe
            mdx' = if mdx == 0 then mx else mdx
            mdy' = if mdy == 0 then my else mdy
        in (mx/mdx', my/mdy')
      _ -> uScale universe
    fScale = case cfScale of
      Just (Clip (x0, y0) (x1, y1)) ->
        fPos (\pos ->
           case pos of
            Point (x,y) -> 
              let x' = if ox + x * sx >= x1 then w else if ox + x*sx <= x0 then 0 else x
                  y' = if oy + y * sy >= y1 then h else if oy + y*sy <= y0 then 0 else y
              in Point (x', y')
            _ -> pos
           )
      _ -> id
    fRotate = case cfRotate of
      Just phi ->
        let f = Ge.rotatePos phi
        in fPos
            (\pos -> case pos of
              Point (x,y) -> Point $ f (x,y)
              _ -> pos)
      Nothing -> id

organize conf universe = 
  toCartesianCoordinates conf $
    transformCoordinates (confPostTranslate conf) Nothing (confPostScale conf) groupedUniverse
  where
    universe' = fromCartesianCoordinates conf $ transformCoordinates (confPreTranslate conf) (confRotate conf) (confPreScale conf) universe
    universe'' = organizeStyle universe' conf
    groupedUniverse = clusterElements (confNodeGrouping conf) (confCurveGrouping conf) universe''

clusterElements ::
     (Floating a, Real a)
  => (Maybe (Grouping (a,a) (a,a)))
  -> (Maybe (Grouping (a,a) (a,a)))
  -> Universe a -> Universe a
clusterElements (Just (Regular (px, py))) curveClustering universe = 
  universe{uElements = (uElements universe) A.// [(i,e) | e <- clusteredNodes ++ clusteredCurves, let (Id i _) = getId e]}
  where
    (curves, nodes) = partition (\(Element _ d _ _) -> isLine d) $ A.elems $ uElements universe
    (w,h) = uSize universe
    stepX = if cx == 1 then 0 else w / (cx - 1)
    stepY = if cy == 1 then 0 else h / (cy - 1)
    (clusteredNodes, cx, cy, xVals, yVals)
      | null nodes = ([], 1, 1, [], [])
      | otherwise = 
        clusterCoordinates (0, 0, w, h) px py
          (regularClustering cx 0 w)
          (regularClustering cy 0 h)
          nodes
    clusteredCurves = case curveClustering of
      Just (Regular (pcx, pcy))
        | null nodes ->
          clusterCurves (0,0,w,h) pcx pcy
            [0, w] [0, h]
            (gridOnGridCurveClustering w)
            (gridOnGridCurveClustering h)
            curves
        | otherwise -> 
          clusterCurves (0,0,w,h) pcx pcy
            xVals yVals
            (gridOnGridCurveClustering stepX)
            (gridOnGridCurveClustering stepY)
            curves
      Just (Cluster (pcx, pcy)) ->
        clusterCurves (0,0,w,h) pcx pcy xVals yVals
          clusterValue
          clusterValue
          curves
      Nothing -> curves
clusterElements nodeClustering curveClustering universe = 
  universe{uElements = (uElements universe) A.// [(i,e) | e <- clusteredNodes ++ clusteredCurves, let (Id i _) = getId e]}
  where
    (curves, nodes) = partition (\(Element _ d _ _) -> isLine d) $ A.elems $ uElements universe
    (w, h) = uSize universe
    (clusteredNodes, _, _, xVals, yVals)
      | null nodes = ([], 0, 0, [], []) 
      | otherwise = case nodeClustering of 
        Just (Cluster (px, py)) ->
          clusterCoordinates (0, 0, w, h) px py
            simpleClustering
            simpleClustering
            nodes
        Nothing ->
          let points = [(x,y) | p <- map getPos nodes, let Point (x,y) = p, isPoint p]
          in (nodes, 0, 0, map head $ group $ map fst points, map head $ group $ map snd points)
    clusteredCurves = case curveClustering of
      Just (Regular (pcx, pcy)) ->
        clusterCurves (0, 0, w, h) pcx pcy xVals yVals
          gridOnClusterCurveClustering
          gridOnClusterCurveClustering
          curves
      Just (Cluster (pcx, pcy)) ->
        clusterCurves (0, 0, w, h) pcx pcy xVals yVals
          clusterValue
          clusterValue
          curves
      Nothing -> curves


organizeStyle universe conf = 
  let (e0, en) = A.bounds $ uElements universe
      elements = A.elems $ uElements universe
      even xs = case xs of
         (x:y:ys) -> x : even ys
         [x] -> [x]
         [] -> []
      attributeScale = uAttributeScale universe
      (eti, ti, tn) =
        numberAttributes
          (\o -> case thickness $ getStyle o of
                      Nothing -> (0, [])
                      Just t -> (1, [t])
          )
          elements
      t0 = minimum $ map snd ti
      t1 = maximum $ map snd ti
      (edi, di, dn) =
        numberAttributes
          (\o -> case dashPattern $ getStyle o of
                      Nothing -> (0, [])
                      Just p -> let p' = even p in (length p', p')
          )
          elements
      (egi, gi, gn) =
        numberAttributes
          (\o -> case dashPattern $ getStyle o of
                      Nothing -> (0, [])
                      Just p -> let p' = even $ tail p in (length p', p')
          )
          elements
      -- functions for scaling thickness and length of dashes and gaps.
      scaleThickness vals = scaleValue attributeScale (confThicknessScale conf) vals
      scaleDashed vals = scaleValue attributeScale (confDashedScale conf) vals
      -- arrays with clustered values
      getThickness = case confThicknessGrouping conf of
        Just (Fixed styles) ->
           let styleMap = M.fromList $ map (\(x,y) -> (y,x)) styles
               scaleF = scaleThickness $ map snd ti
           in (\_ v -> (Nothing, lookupClosest styleMap $ scaleF v))
        Just (Regular d) ->
          let (arr, nClustersT, tVals) =
                clusterValues (d * (t1 - t0))
                              (regularClustering nClustersT t0 t1)
                              tn
                              ti
              scaleF = scaleThickness tVals
              arr' = U.amap scaleF arr
          in (\i _ -> (Just $ arr' U.! i, Nothing))
        Just (Cluster d) ->
          let (arr, _, tVals) =
                clusterValues (d * (t1 - t0))
                              simpleClustering
                              tn
                              ti
              scaleF = scaleThickness tVals
              arr' = U.amap scaleF arr
          in (\i _ -> ( Just $ arr' U.! i, Nothing))
        Nothing -> 
          let scaleF = scaleThickness $ map snd ti
              arr = (U.array (0, tn - 1) :: [(Int, a)] -> U.Array Int a) $ map (\(i, t) -> (i, scaleF t)) ti
          in (\i _ -> (Just $ arr U.! i, Nothing))
      getDashPattern = case confDashedGrouping conf of
          Just (Fixed styles) ->
            let scaleF = scaleDashed $ map snd $ di ++ gi
            in (\_ _ pattern ->
                let pattern' = map scaleF pattern
                    styleDist = map (\(name, style) -> (name, dashDistance style pattern')) styles
                    s = minimumBy (\(n1, d1) (n2,d2) -> compare d1 d2) styleDist
                in (Nothing, Just $ fst s)
               )
          Just grouping -> 
            let d0 = minimum $ map snd di
                d1 = maximum $ map snd di
                g0 = minimum $ map snd gi
                g1 = maximum $ map snd gi
                (clusterD, clusterG, p) = case grouping of 
                  Regular d -> (regularClustering nClustersD d0 d1, regularClustering nClustersG g0 g1, d)
                  Cluster d -> (simpleClustering, simpleClustering, d)
                (dashArr, nClustersD, dashVals) =
                  clusterValues (p * (d1 - d0)) clusterD dn di
                (gapArr, nClustersG, gapVals) =
                  clusterValues (p * (g1 - g0)) clusterG gn gi
                scaleF = scaleDashed $ dashVals ++ gapVals
                dashArr' = U.amap scaleF dashArr
                gapArr'  = U.amap scaleF gapArr
            in (\ds gs _ -> (Just $ interleave (map (dashArr' U.!) ds) (map (gapArr' U.!) gs), Nothing))
          Nothing -> 
            let scaleF = scaleDashed $ map snd $ di ++ gi
                dashArr = (U.array (0, dn - 1) :: ([(Int, a)] -> U.Array Int a)) $ map (\(i,d) -> (i, scaleF d)) di
                gapArr  = (U.array (0, gn - 1) :: ([(Int, a)] -> U.Array Int a)) $ map (\(i,g) -> (i, scaleF g)) gi
            in (\ds gs _ -> (Just $ interleave (map (dashArr U.!) ds) (map (gapArr U.!) gs), Nothing))
  in universe
     { uElements = U.array (e0, en) $ map indexElement $
                           updateStyle getThickness getDashPattern elements eti edi egi
     }

indexElement e = let Id i _ = getId e in (i, e)

updateStyle _ _ [] _ _ _ = []
updateStyle gt gdp (c@(Container _ _ _ _) : es) (_:ts) (_:ds) (_:gs) = c : updateStyle gt gdp es ts ds gs
updateStyle getThickness getDashPattern (Element eid d st att : es) (ti:ts) (di:ds) (gi:gs) =
  Element eid d st' att' : updateStyle getThickness getDashPattern es ts ds gs
  where
    mt = thickness st
    t = fromJust mt
    (mt', newAtt) = getThickness (head ti) t
    attT
      | isNothing mt = att 
      | isJust newAtt = M.insert (fromJust newAtt) Nothing att
      | otherwise = att
    mdp = dashPattern st
    dp = fromJust mdp
    (mdp', newAttD) = getDashPattern di gi dp
    st' =
      st{ thickness   = if isNothing mt then Nothing else mt'
        , dashPattern = if isNothing mdp then Nothing else mdp'
        }
    att'
      | isNothing mdp = attT
      | isJust newAttD = M.insert (fromJust newAttD) Nothing attT
      | otherwise = attT

{-
      getThickness objs = [ fromJust $ thickness st
                          | o <- objs, let st = getStyle o, isJust $ thickness st]
      -- Functions for scaling thickness and length of dashes and gaps.
      thicknessScale vals   = scaleValue attributeScale (confThicknessScale conf) vals
      dashedScale vals = fmap $ map $ scaleValue attributeScale (confDashedScale conf) vals
      dashPatternVals = concat [fromJust dp | e <- elements, let dp = dashPattern $ getStyle e, isJust dp]
      thicknessVals   =        [fromJust tk | e <- elements, let tk = thickness $ getStyle e, isJust tk]
      -- Given an element, decide which cluster its thickness and dash pattern belongs to.
      -- Already applies scale.
      thicknessGroups :: Int -> Style Double -> M.Map String (Maybe String) -> (Style Double, M.Map String (Maybe String))
      thicknessGroups =
        case confThicknessGrouping conf of
          Just (Fixed styles) -> let styleMap = M.fromList $ map (\(x,y) -> (y,x)) styles
                                     thicknessScale' = thicknessScale thicknessVals
                                 in (\i st att ->
                                      let v' = thicknessScale' $ thickness st
                                      in case v' >>= lookupClosest styleMap of
                                          Nothing -> (st{thickness = v'}, att)
                                          Just x -> (st{thickness = Nothing}, M.insert x Nothing att))
          Just grouping ->
            let vals = [(x, fromJust t)
                       | e <- elements
                       , let t = thickness $ getStyle e
                       , let (Id x str) = getId e
                       , isJust t]
                vals' = case grouping of
                    Regular d -> regularClusterValues d vals
                    Cluster d -> let (vs, _, _) = clusterValues d id vals in [(x,v) | (x, (v,_)) <- vs]
                valT = A.accumArray (\o n -> Just n) Nothing (e0, en) vals' :: A.Array Int (Maybe Double)
                thicknessScale' = thicknessScale $ map snd vals'
            in (\i st att -> (st{thickness = thicknessScale' $ valT A.! i}, att))
          Nothing -> 
            let thicknessScale' = thicknessScale thicknessVals
            in (\i st att -> (st{thickness = thicknessScale' $ thickness st}, att))
      dashedGroups :: Int -> Style Double -> M.Map String (Maybe String) -> (Style Double, M.Map String (Maybe String))
      dashedGroups = 
        case confDashedGrouping conf of
          Just (Fixed styles) -> (\i st att ->
            let dashedScale' = dashedScale dashPatternVals
                vs' = dashedScale' $ dashPattern st
                styleDist = map (\(name, style) -> (name, dashDistance style $ fromJust vs' )) styles
                s = minimumBy (\(n1, d1) (n2,d2) -> compare d1 d2) styleDist
            in if isNothing vs' then (st, att) else (st{dashPattern = Nothing}, M.insert (fst s) Nothing att))
          Just grouping -> 
            -- Lengths of gaps, together with the index of the element owning the dash pattern as well as the index of the gap
            let gapVals =
                  concat
                    [ zipWith (\v i -> ((x,i), v)) (even $ tail $ fromJust dp) [0..]
                    | e <- elements
                    , let dp = dashPattern $ getStyle e
                    , let (Id x str) = getId e
                    , isJust dp]
                dashVals =
                  concat [ zipWith (\v i -> ((x,i), v)) (even $ fromJust dp) [0..]
                         | e <- elements
                         , let dp = dashPattern $ getStyle e
                         , let (Id x str) = getId e
                         , isJust dp]
                (gapVals', dashVals') = case grouping of 
                  Regular d -> (regularClusterValues d gapVals, regularClusterValues d dashVals)
                  Cluster d -> 
                    let (gvs', _, _) = clusterValues d id gapVals
                        (dvs', _, _) = clusterValues d id dashVals
                        gvs'' = [ (i,v) | (i, (v,_)) <- gvs']
                        dvs'' = [ (i,v) | (i, (v,_)) <- dvs']
                    in (gvs'', dvs'')
                (g0,gn) = (minimum $ map (snd . fst) gapVals' , maximum $ map (snd . fst) gapVals' )
                (d0,dn) = (minimum $ map (snd . fst) dashVals', maximum $ map (snd . fst) dashVals' )
                gapT  = A.accumArray (\_ n -> Just n) Nothing ((e0, g0), (en, gn)) gapVals' :: A.Array (Int, Int) (Maybe Double)
                dashT = A.accumArray (\_ n -> Just n) Nothing ((e0, d0), (en, dn)) dashVals' :: A.Array (Int, Int) (Maybe Double)
                dp' i st = zipWith (\t j ->  fromJust $ t A.! (i, j)) (cycle [dashT, gapT]) [j `div` 2 | j <- [0..(length $ fromMaybe [] $ dashPattern st) - 1]]
                dashedScale' = dashedScale $ map snd (gapVals' ++ dashVals')
            in (\i st att -> (st{dashPattern = dashedScale' $ if isNothing $ dashPattern st then Nothing else Just $ dp' i st}, att))
          Nothing -> 
            let dashedScale' = dashedScale dashPatternVals
            in (\_ st att -> (st{dashPattern = dashedScale' $ dashPattern st}, att))
      -- Just go through every element and adjust its thickness and dash pattern.
      updateStyle o = 
          let st = getStyle o
              att = getAttributes o
              (Id ix _) = getId o
              (st1, att1) = thicknessGroups ix st att
              (st2, att2) = dashedGroups ix st1 att1
          in setAttributes (setStyle o st2) att2
      in universe
         { uElements = A.array (A.bounds $ uElements universe)
                               [(i, updateStyle e) | e <- A.elems $ uElements universe, let (Id i _) = getId e] }
-}
dashDistance d1 d2 = dashDistance' 0 0 d1 d2
  where
    dashDistance' n delta (x:xs) (y:ys) = dashDistance' (n+1) (delta + (x-y)^2) xs ys
    dashDistance' n delta [] [] = delta / n
    dashDistance' n delta [] ys = dashDistance' n delta d1 ys
    dashDistance' n delta xs [] = dashDistance' n delta xs d2

scaleValue attributeScale scaling values = 
  let sortedValues = sort values
      minV = minimum values -- take minimum instead of (head sortedValues) because we don't always need to sort the values
      maxV = maximum values
      scaleF x = case scaling of
          Just (Plain s) -> x * s / attributeScale
          Just (MinDist d) -> let ds = filter (> 0.01 * maxV) $ zipWith (flip (-)) sortedValues (tail sortedValues)
                                  s = if null ds then 1 else d / (attributeScale * head ds)
                              in x * s
          Just (Fit w)   -> if maxV - minV < (0.01*maxV) then x else x * w / (attributeScale * (maxV - minV))
          Just (Clip mn mx) -> if x < mn / attributeScale then mn / attributeScale else if x > mx / attributeScale then mx / attributeScale else x
          Just (Range mn mx) -> if maxV - minV < (0.01*maxV)
            then (mx + mn) / (2 * attributeScale)
            else ((x - minV) / (maxV - minV)) * ((mx - mn) / attributeScale) + (mn / attributeScale)
          Nothing -> x
  in \v -> scaleF v

getElementStyle f (Element _ _ st _) = case f st of
  Nothing -> []
  Just x -> x
getElementStyle _ (Container _ _ _ _) = []

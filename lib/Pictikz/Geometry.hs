--  Copyright 2017-2020 Marcelo Garlet Milani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.Geometry where

import Data.Matrix
import Pictikz.Utils (epsilon, average)

data Shape a =
    Rectangle a a
  | Ellipse a a
  | Point
  deriving (Show, Read, Eq)

instance Enum (Shape a) where
  toEnum x = [Rectangle undefined undefined, Ellipse undefined undefined, Point ]!!(x `mod` 3)
  fromEnum (Rectangle _ _) = 0
  fromEnum (Ellipse _ _) = 1
  fromEnum (Point) = 2

rotatePos phi = 
  let cp = cos phi
      sp = sin phi
  in (\(x,y) -> (x * cp - y*sp, x*sp + y*cp))

rotate a =
  fromList 3 3 [cos a, - sin a, 0
               ,sin a,   cos a, 0
               ,    0,       0, 1]
translate x y =
  fromList 3 3 [1,0,x
               ,0,1,y
               ,0,0,1]
skewx a =
  fromList 3 3 [1, tan a, 0
               ,0,     1, 0
               ,0,     0, 1]
skewy a =
  fromList 3 3 [1    , 0, 0
               ,tan a, 1, 0
               ,0    , 0, 1]
scale x y =
  fromList 3 3 [x,0,0
               ,0,y,0
               ,0,0,1]

squareDistance (x0,y0) (x,y) (Rectangle w h) =
  let dx
        | x0 >= x && x0 <= x + w = 0
        | x0 >= x + w = x0 - (x+w)
        | x0 < x = x - x0
      dy
        | y0 >= y && y0 <= y + h = 0
        | y0 >= y + h = y0 - (y+h)
        | y0 < y = y - y0
  in dx*dx + dy*dy
squareDistance (x0,y0) (x1, y1) (Ellipse rx ry) =
  let rx2 = rx / 2
      ry2 = ry / 2
      dx
        | x0 >= x1 - rx2 && x0 <= x1 + rx2 = 0
        | x0 >= x1 + rx2 = x0 - (x1 + rx2)
        | x0 <  x1 - rx2 = x1 - rx2 - x0
      dy
        | y0 >= y1 - ry2 && y0 <= y1 + ry2 = 0
        | y0 >= y1 + ry2 = y0 - (y1 + ry2)
        | y0 <  y1 = y1 - y0
  in dx*dx + dy*dy
squareDistance (x0, y0) (x1,y1) Point = (x1-x0)^2 + (y1-y0)^2

distance p0 p1 = sqrt $ squareDistance p0 p1 Point

-- Square distance from p2 to the line segment p1 -- p2
squareDistanceToLine p0@(x0,y0) p1@(x1,y1) p2@(x2,y2) = t
  where
    (lx,ly) = (x1-x0,y1-y0)
    l = sqrt $ lx*lx + ly*ly
    (nx,ny) = (-ly/l, lx/l)
    Just (tl, tn) = solveLinearSystem2 lx (-nx) (x2 - x0) ly (-ny) (y2 - y0)
    t
      | tl > 1 = squareDistance p1 p2 Point
      | tl < 0 = squareDistance p0 p2 Point
      | otherwise = tn * tn

averageSquareDistanceToLine p0@(x0,y0) p1@(x1,y1) ps = average $ map dst ps
  where
    (lx,ly) = (x1-x0,y1-y0)
    l = sqrt $ lx*lx + ly*ly
    (nx,ny) = (-ly/l, lx/l)
    dst (x,y) 
      | tl > 1 = squareDistance p1 (x,y) Point
      | tl < 0 = squareDistance p0 (x,y) Point
      | otherwise = tn * tn
      where
        Just (tl, tn) = solveLinearSystem2 lx (-nx) (x - x0) ly (-ny) (y - y0)

boundingCircle shape = 
  case shape of 
    Point -> Ellipse 0 0
    Ellipse rx ry -> let r = max rx ry in Ellipse r r
    Rectangle w h -> let r = sqrt $ w*w + h*h in Ellipse r r

polarDistance (x0,y0) (x1,y1) = 
  let dx = x1 - x0
      dy = y1 - y0
      r = sqrt $ dx*dx + dy*dy
      phi' = acos $ dx / r
      phi = if dy >= 0 then phi' else 2*pi - phi'
  in if r == 0 then (0,0) else (r, phi)

polarDistanceToShape p0 p Point = polarDistance p0 p
polarDistanceToShape (x0,y0) (x,y) (Rectangle w h) = 
  let dx
        | x0 >= x && x0 <= x + w = 0
        | x0 >= x + w = x0 - (x+w)
        | otherwise = x - x0 -- x0 < x
      dy
        | y0 >= y && y0 <= y + h = 0
        | y0 >= y + h = y0 - (y+h)
        | otherwise = y - y0 -- y0 < y
      d = sqrt $ dx * dx + dy * dy
      phi
        | dy == 0 && dx == 0 = 0
        | dy == 0 && dx > 0  = 0
        | dy == 0 && dx < 0  = pi
        | dy > 0  && dx == 0 = pi/2
        | dy > 0  && dx > 0  = pi/4
        | dy > 0  && dx < 0  = 3*pi/4
        | dy < 0  && dx == 0 = 3*pi/2
        | dy < 0  && dx > 0  = 7*pi/4
        | dy < 0  && dx < 0  = 5*pi/4
  in (d, phi)
-- | Polar distance to an ellipsis is only an approximate.
polarDistanceToShape (x0,y0) (x,y) (Ellipse rx ry) = 
  let r' = (rx + ry)/2
      dx = x0 - x
      dy = y0 - y
      r = sqrt $ dx*dx + dy*dy
  in (r - r'
     , if r < epsilon
       then 0
       else if dy >= 0 
         then acos $ dx / r
         else 2*pi - (acos $ dx / r))

-- | Distance from p to the line segment given by the points p1 p2.
polarDistanceToSegment (x,y) (x1,y1) (x2,y2) = 
  -- First construct one line segment given by the equation
  -- x' = x1 + t1*ux
  -- y' = y1 + t1*uy
  -- The vector of the line starting at (x,y) is
  -- obtained by rotation (ux,uy) by 90 degrees counter-clockwise.
  -- Hence, the second equation is given by
  -- x' = x + t0*uy
  -- y' = y - t0*ux
  let dx = x2 - x1
      dy = y2 - y1
      l = sqrt $ dx*dx + dy*dy
      ux = dx / l
      uy = dy / l
      phi = if dx > 0 then pi/2 else 3*pi/2
      (t0,t1) = if abs uy <= epsilon then
                  let t1 = (x - x1) / ux
                      t0 = (y - y1 - t1) / ux
                  in (t0, t1)
                else
                  -- Construct matrix corresponding to line equations.
                  -- The last row of the matrix is given by | 0 b c |
                  let a = ux / uy
                      b = uy + ux*a
                      c = (y - y1) - a * (x1 - x)
                      t1 = c / b
                      t0 = (x1 - x + t1*ux) / uy
                  in (t0, t1)
  in if t1 < 0 then
        polarDistance (x,y) (x1,y1)
     else if t1 > l then
        polarDistance (x,y) (x2,y2)
     else
       (abs t0, if t0 < 0 then 2*pi - phi else phi)

vectorAngle lu (ux,uy) lv (vx,vy) 
  | a < 0 = pi + phi
  | otherwise = phi
  where
    a = ux*vy - uy*vx
    phi = acos $ (ux*vx + uy*vy) / (lu * lv)

-- | Solve the linear system with two variables and two equations:
--   ax + by = c
--   dx + ey = f
--   If there are infinitely many solutions, return only one of them.
solveLinearSystem2 a b c d e f
  | (abs a) > (abs b) = 
    let r = d / a
        s = e - r * b
        y = (f - r*c) / s
        x = (c - b*y) / a
        result
          | s /= 0 = Just (x,y)
          | (f - r * c) == 0 = Just (c / a,0)
          | otherwise = Nothing
    in result
  | b /= 0 = 
    let r = e / b
        s = d - r * a
        x = (f - r * c) / s
        y = (c - a * x) / b
        result
          | s /= 0 = Just (x,y)
          | (f - r * c) == 0 = Just (0, c / b)
          | otherwise = Nothing
    in result
  | c == 0 && f == 0 = Just (0,0)
  | otherwise = Nothing
    

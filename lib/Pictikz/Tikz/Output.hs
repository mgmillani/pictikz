module Pictikz.Tikz.Output where

import qualified Pictikz.Geometry as Ge
import           Pictikz.Elements
import           Pictikz.Parser
import           Pictikz.Utils
import qualified Pictikz.Text as T

import Numeric
import Data.Maybe
import Data.DescriLo as De
import Data.Char
import Data.List
import Text.Read (readEither, readMaybe)
import qualified Data.Map as M
import qualified Data.Array.IArray as A
import Control.Monad

data RenameNodes = ByLabel | ByOrder deriving (Eq, Show)
-- data Position = Inside | West | East | North | South | Smart Double deriving (Eq)

data Configuration = Configuration
  { confStandalone  :: Maybe Bool
  , confBeamer      :: Maybe Bool
  , confLabelPosition :: Maybe Position
  , confShapeMap    :: Maybe (M.Map Int String)
  , confArrowMap    :: Maybe (M.Map Int String)
  , confRenameNodes :: Maybe RenameNodes
  , confNodeName    :: Maybe (Int -> String)
  , confFloatPrecision :: Maybe Int
}

defaultConfiguration = Configuration
  { confStandalone  = Just False
  , confBeamer      = Just False
  , confShapeMap    = Nothing
  , confLabelPosition = Nothing
  , confArrowMap    = Nothing
  , confRenameNodes = Nothing
  , confNodeName    = Just $ (\i -> "v" ++ show i)
  , confFloatPrecision = Just 3
  }
noConfiguration = Configuration
  { confStandalone    = Nothing
  , confBeamer        = Nothing
  , confLabelPosition = Nothing
  , confShapeMap      = Nothing
  , confArrowMap      = Nothing
  , confRenameNodes   = Nothing
  , confNodeName      = Nothing
  , confFloatPrecision = Nothing
  }

updateConfig old new =
  Configuration
  { confStandalone     = combineMaybe (confStandalone old)     (confStandalone new)
  , confBeamer         = combineMaybe (confBeamer old)         (confBeamer new)
  , confLabelPosition  = combineMaybe (confLabelPosition old)  (confLabelPosition new)
  , confShapeMap       = combineMaybe (confShapeMap old)       (confShapeMap new)
  , confArrowMap       = combineMaybe (confArrowMap old)       (confArrowMap new)
  , confRenameNodes    = combineMaybe (confRenameNodes old)    (confRenameNodes new)
  , confNodeName       = combineMaybe (confNodeName old)       (confNodeName new)
  , confFloatPrecision = combineMaybe (confFloatPrecision old) (confFloatPrecision new)
  }

class LocalDrawable a where
  localDraw :: Configuration -> a -> String

instance LocalDrawable a => LocalDrawable [a] where
  localDraw conf xs = concatMap (localDraw conf) xs

instance LocalDrawable ArrowType where
  localDraw conf a = 
    let defaultArrow = ["->", "<-", "--", "<->"]!! (fromEnum a)
    in case confArrowMap conf of
        Nothing -> defaultArrow
        Just m -> fromMaybe defaultArrow $ M.lookup (fromEnum a) m

--instance LocalDrawable Color where
--  localDraw conf (RGB r g b) = "{rgb:red," ++ show r ++ ";green,"  ++ show g ++ ";blue," ++ show b ++ "}"
--  localDraw conf (NamedColor n) = n

instance LocalDrawable T.Format where
  localDraw conf f = 
    concat
    [ if fromMaybe False $ T.bold f then "\\bfseries{}" else ""
    , if fromMaybe False $ T.italics f then "\\itshape{}" else ""
    , case T.position f of Just (T.Subscript) -> "\\textsubscript" ; Just (T.Superscript) -> "\\textsuperscript" ; _ -> ""
    -- As far as I know, it is not possible to align each line individually inside a TikZ node.
    --, case T.alignment f of Just (T.LeftAligned) -> "" ; Just alType -> localDraw conf alType ; _ -> ""
    , case T.color f of Just "" -> "" ; Just c -> "\\color{" ++ c ++ "}" ; _ -> ""
    ]

instance LocalDrawable T.Text where
  localDraw conf (T.Text str format) =
    concat
      [ take 1 $ filter (=='\n') str
      , localDraw conf format{T.position = Nothing}
      , "{"
      , localDraw conf T.noFormat{T.position = T.position format}
      , filter (/='\n') str
      , "}"
      ]

instance LocalDrawable T.AlignmentType where
  localDraw conf (T.LeftAligned) = "left"
  localDraw conf (T.RightAligned) = "right"
  localDraw conf (T.Centered) = "center"

instance Show a => LocalDrawable (Ge.Shape a) where
  localDraw conf shape = case getEnumStr (confShapeMap conf) shape of
    Just str -> str
    Nothing -> case shape of
      (Ge.Rectangle _ _ ) -> "rectangle"
      (Ge.Ellipse _ _) -> "circle"
      (Ge.Point) -> ""

escapeLines "\n" = []
escapeLines ('\n':r) = "\\\\ " ++ escapeLines r
escapeLines (a:r) = a : escapeLines r
escapeLines [] = []

drawAttribute var val = var ++ " = " ++ val

showPFloat f d = 
  let i = truncate $ f
      sign = if f <0 && f > -1 then "-" else ""
      dc = abs $ f - (fromIntegral i)
      dcStr' = cutTrailingZeroes $ drop 2 $ showFFloat (Just d) dc ""
      dcStr = if null dcStr' then "0" else dcStr'
      cutTrailingZeroes "" = ""
      cutTrailingZeroes ('0':ds) = 
        let zs = cutTrailingZeroes ds
        in if null zs then "" else '0':zs
      cutTrailingZeroes (d:ds) = d : cutTrailingZeroes ds
      dcStr'' = take d dcStr
  in if dcStr'' == "0" then sign ++ (show i) else sign ++ (show i) ++ "." ++ (take d dcStr)

--instance Show a, b => LocalDrawable (a, b) where
--  localDraw conf (var, val) = var ++ " = " ++ val
instance LocalDrawable Float where
  localDraw conf f = showPFloat f (fromJust $ confFloatPrecision conf)
instance LocalDrawable Double where
  localDraw conf f = showPFloat f (fromJust $ confFloatPrecision conf)

drawDashPattern conf dp = drop 1 $ (concat $ zipWith (\w g -> w ++ localDraw conf g) (cycle [" on ", " off "]) dp)

drawPath :: Configuration -> String -> [String] -> [([T.Text], (Int, Double))] -> String
drawPath conf edge [] _ = ""
drawPath conf edge (c:cs) labels = c ++ drawPath' 0 (cs) labels
  where
    edge' = ' ':edge++" "
    drawPath' :: Int -> [String] -> [([T.Text], (Int, Double))] -> String
    drawPath' _ [] _ = ""
    drawPath' _ (c:cs) [] = edge' ++ intercalate edge' (c:cs)
    drawPath' i (c:cs) ((t, (j, phi)):labels)
      | i == j = 
        concat
        [ edge'
        , "node["
        , if phi >= 0 && phi < pi then "above" else "below"
        , "] {"
        , concatMap (localDraw conf) t
        , "} "
        , c
        , drawPath' (i+1) cs labels
        ]
      | otherwise = edge' ++ c ++ drawPath' (i+1) cs ((t,(j, phi)):labels)

drawCurve :: (LocalDrawable a, Floating a, Show a, Real a) => Universe a -> Configuration -> [Coordinate a] -> [CurveSegment a] -> [([T.Text], (Int, Double))] -> String
drawCurve _ conf _ [] _ = ""
drawCurve universe conf ps cs labels = drawCurve' universe 0 ps cs labels
  where
    pStr = draw universe conf $ head ps
    drawCurve' ::(LocalDrawable a, Floating a, Show a, Real a) => Universe a -> Int -> [Coordinate a] -> [CurveSegment a] -> [([T.Text], (Int, Double))] -> String
    drawCurve' universe _ [p] [] _ = ' ':draw universe conf p
    drawCurve' _ _ [] [] _ = ""
    drawCurve' universe i (p:ps) (c:cs) labels
      | null labels = pStr ++ cStr ++ drawCurve' universe (i+1) ps' cs labels
      | i == j = 
        concat
        [ pStr
        , cStr
        , "node["
        , if phi >= 0 && phi < pi then "above" else "below"
        , "] {"
        , concatMap (localDraw conf) t
        , "} "
        , drawCurve' universe (i+1) ps' cs (tail labels)
        ]
      | otherwise = pStr ++ cStr ++ drawCurve' universe (i+1) ps' cs labels
      where
        (t, (j, phi)) = head labels
        pStr
          | i == 0 = draw universe conf p
          | otherwise = ' ':draw universe conf p
        cStr = case c of
          a@(Arc _ phi _) ->
            let (theta1, theta2, rx, ry) = arcAngles p0 p3 a
                p0 = getAbsolutePos universe p
                p3 = getAbsolutePos universe $ head ps
            in   " arc [start angle = "
              ++ localDraw conf ( (180 / pi) * theta1)
              ++ ", end angle = "
              ++ localDraw conf ( (180 / pi) * theta2)
              ++ ", x radius = " ++ localDraw conf rx
              ++ ", y radius = " ++ localDraw conf ry
              ++ ", rotate = " ++ localDraw conf ((180 / pi) * phi)
              ++ "]"
          Cubic (rho1, psi1) (rho2, psi2) ->
               " .. controls " ++ (draw universe conf $ Point p1)
            ++ " and " ++ (draw universe conf $ Point p2) ++ " .."
            where
              (x0,y0) = getAbsolutePos universe p
              (x3,y3) = getAbsolutePos universe $ head ps
              p1 = (x0 + rho1 * (cos psi1), y0 + rho1 * (sin psi1))
              p2 = (x3 + rho2 * (cos psi2), y3 + rho2 * (sin psi2))
          Line -> " to"
        ps' = case c of
          Arc _ _ _ -> tail ps
          _ -> ps

drawColor (RGB r g b) = "{rgb:red," ++ show r ++ ";green,"  ++ show g ++ ";blue," ++ show b ++ "}"
drawColor (NamedColor n) = n

drawLabel conf st t = 
  let alignAtt = case alignment st of 
        Nothing -> case t of
          ((T.Text _ f):_) -> case T.alignment f of
            Nothing -> " "
            Just alType -> " [align = " ++ localDraw conf alType ++ "]"
          _ -> " "
        Just (T.LeftAligned) -> " "
        Just alType -> " [align = " ++ localDraw conf alType ++ "]"
  in alignAtt ++ "{" ++ escapeLines (concatMap (localDraw conf) t) ++ "}"

listAttributes att = map (\(var, mval) -> if isJust mval then var ++ " = " ++ fromJust mval else var) $ M.assocs att

listStyle conf st =
  map fromJust $ filter isJust $ 
  [ fmap (drawAttribute "dash pattern" . (drawDashPattern conf)) $ dashPattern st
  , fmap (drawAttribute "line width" . (localDraw conf) ) $ thickness st
  , fmap (drawAttribute "fill" . drawColor) $ fillColor st
  , fmap (drawAttribute "draw" . drawColor) $ strokeColor st
  ]

escapeTikzId [] = []
escapeTikzId (c:cs) = case c of
  '(' -> '/':'o':'p': escapeTikzId cs
  ')' -> '/':'c':'p': escapeTikzId cs
  '.' -> '/':'d': escapeTikzId cs
  ',' -> '/':'c': escapeTikzId cs
  ':' -> '/':'o': escapeTikzId cs
  ';' -> '/':'s': escapeTikzId cs
  '/' -> '/':'/': escapeTikzId cs
  '#' -> '/':'h': escapeTikzId cs
  '$' -> '/':'m': escapeTikzId cs
  _   -> c : escapeTikzId cs

class Drawable t where
  draw        :: (Show a, LocalDrawable a, Floating a, Real a) => Universe a -> Configuration -> t a -> String
  drawOverlay :: (Show a, LocalDrawable a, Floating a, Real a) => Universe a -> Configuration -> t a -> String
  drawOverlay = draw

--instance Drawable a => Drawable [a] where
--  draw u c xs = concatMap (draw u c) xs

instance Drawable Coordinate where
  draw universe conf (Point (x,y)) =
    let (sx,sy) = uScale universe
        (ox, oy) = uOrigin universe
    in "(" ++ localDraw conf (x * sx + ox) ++ ", " ++ localDraw conf (y * sy + oy) ++ ")"
  draw universe conf (ElementRef i) = 
    let (Id _ str) = getId $ (uElements universe) A.! i
    in "(" ++ (escapeTikzId str) ++ ")" 

instance Drawable Element where
  draw universe conf (Container _ _ _ _) = ""
  draw universe conf (Element eid (PolyLine cs) st att) =
    let arrowHead =  fmap (localDraw conf) $ arrow st
        tikzAtts' = listStyle conf st ++ listAttributes att
        tikzAtts = fromMaybe (tikzAtts') $ fmap (:tikzAtts') $ arrowHead
        tikz = concat
          ["\\path"
          , if null tikzAtts then "" else "[" ++ csl tikzAtts ++ "]"
          , "\n"
          , "\t" ++
            drawPath conf "to" (map (draw universe conf) cs)
                     (zip (fromMaybe [] $ label st)
                          ( map (\pos -> case pos of
                                  OnSegment i phi -> (i, phi)
                                  Around _ phi -> (0, phi)
                                )
                            $ fromMaybe [] $ labelPosition st
                          )
                     )
          , ";"
          ]
        Just (begin, end) = time st
    in tikz
  draw universe conf (Element eid (Curve ps cs) st att) =
    let arrowHead =  fmap (localDraw conf) $ arrow st
        tikzAtts' = listStyle conf st ++ listAttributes att
        tikzAtts = fromMaybe (tikzAtts') $ fmap (:tikzAtts') $ arrowHead
        tikz = concat
          ["\\path"
          , if null tikzAtts then "" else "[" ++ csl tikzAtts ++ "]"
          , "\n"
          , "\t" ++
            drawCurve universe conf ps cs
                     (zip (fromMaybe [] $ label st)
                          ( map (\pos -> case pos of
                                  OnSegment i phi -> (i, phi)
                                  Around _ phi -> (0, phi)
                                )
                            $ fromMaybe [] $ labelPosition st
                          )
                     )
          , ";"
          ]
        Just (begin, end) = time st
    in tikz
  draw universe conf (Element eid (Shape pos shape) st att) = 
    let Id i iStr = eid
        tikzAtts = (localDraw conf shape) : (labelAtt ++ listStyle conf st ++ listAttributes att)
        nodeLabels = filter (\(l,_) ->
          case l of
            [T.Text "" _] -> False
            otherwise -> True            
          ) $ zip (fromMaybe [] $ label st) (fromMaybe [] $ labelPosition st)
        outsideLabels = filter (\(_, p) ->
          case p of
            Around r phi
              | r > 1 -> True
              | otherwise -> False
            _ -> False
            ) nodeLabels
        labelAtt = case map snd outsideLabels of
          ((Around r phi):_) -> if r <= 1 then [] else
            let labelPosI = (1 + (floor $ 4*(phi - pi/8) / pi)) `mod` 8 
                labelPos  = ["right", "above right", "above", "above left", "left", "below left", "below", "below right"] !! labelPosI
            in ["label = " ++ labelPos ++ ":{" ++ escapeLines (localDraw conf (fst $ head $ outsideLabels)) ++ "}"]
          [] -> []
        insideLabels = filter (\(_, p) ->
          case p of
            Around r phi
              | r <= 1 -> True
              | otherwise -> False
            _ -> False
            ) nodeLabels
        insideLabel = case map snd insideLabels of
          ((Around r _):_) -> if r > 1 then "{}" else
            drawLabel conf st (fst $ head $ insideLabels)
          [] -> "{}"
        tikz = concat
          [ "\\node["
          , csl tikzAtts
          , "]\n"
          , "\t(" ++ (escapeTikzId iStr) ++ ") at " ++ draw universe conf pos
          , insideLabel ++ ";"
          ]
        Just (begin, end) = time st
    in tikz
  draw universe conf (Element eid (Paragraph pos t) st att) = 
    let Id i iStr = eid
        tikzAtts = listStyle conf st ++ listAttributes att
        tikz = concat
          ["\\node["
          , csl tikzAtts
          , "]\n"
          , "\t(" ++ iStr ++ ") at " ++ draw universe conf pos
          , drawLabel conf st t
          , ";"
          ]
    in tikz
  drawOverlay universe conf el = 
    let st = getStyle el
        Just (begin, end) = time st
    in if isNothing $ time st then draw universe conf el
       else uncover (fmap show begin) (fmap show end) (draw universe conf el)

instance (LocalDrawable a, Floating a, Show a, Real a) => LocalDrawable (Universe a) where
  localDraw conf universe =
    let elements = A.elems $ uElements universe
        (ox, oy) = uOrigin universe
        (sx, sy) = uScale universe
        sv = uAttributeScale universe
        elements' =
          [ e -- fPos (\pos -> case pos of Point (x,y) -> Point (x*sx + ox, y*sy + oy); ElementRef x -> pos) e
          | e' <- elements
          , let e = case e' of
                  Container _ _ _ _ -> e'
                  Element eid d st att ->
                    Element eid d
                      st{thickness = fmap (*sv) $ thickness st
                        , dashPattern = fmap (map (*sv)) $ dashPattern st
                        }
                      att
          ]
        nodes = filter (\e -> case e of Element _ d _ _ -> not $ isLine d ; _ -> True)  elements'
        edges = filter (\e -> case e of Element _ d _ _ -> isLine d  ; _ -> False) elements'
        drawF = if confBeamer conf == Just True then drawOverlay else draw
    in    (intercalate "\n" $ map (drawF universe conf) nodes)
       ++ "\n" ++ (intercalate "\n" $ map (drawF universe conf) edges)

getEnumStr maybeM e = do
  m <- maybeM
  M.lookup (fromEnum e) m

uncover begin end element = concat
  [ "\\uncover"
  , "<" ++ fromMaybe [] begin ++ "-" ++ fromMaybe [] end ++ ">"
  , "{" ++ element ++ "}\n"
  ]

parseConfig conf = foldM parseConfig' noConfiguration tikzD
  where
    tikzD = concatMap De.values $ filter (\c -> map toLower (De.name c) == "tikz output") conf
    parseConfig' cf (attr, value) =
      case attr of
        "beamer" -> case map toLower value of
          "true"  -> Right cf{confBeamer = Just True}
          "false" -> Right cf{confBeamer = Just False}
          _ -> parseError attr value "true | false"
        --"label position" -> case map (map toLower) $ words value of
        --  ["inside"] -> Right cf{confLabelPosition = Just Inside}
        --  ["left"]   -> Right cf{confLabelPosition = Just West}
        --  ["right"]  -> Right cf{confLabelPosition = Just East}
        --  ["above"]  -> Right cf{confLabelPosition = Just North}
        --  ["below"]  -> Right cf{confLabelPosition = Just South}
        --  ["smart", percentStr] -> do
        --    percent <- either (\x -> Left [x]) Right $ readEither percentStr :: Either [String] Double
        --    return $ cf{confLabelPosition = Just $ Smart percent}
        --  _ -> parseError attr value "inside | left | right | above | below | smart <percent>"
        "standalone" -> case map toLower value of
          "true"  -> Right cf{confStandalone = Just True}
          "false" -> Right cf{confStandalone = Just False}
          _ -> parseError attr value "true | false"
        "rename nodes" -> case map toLower value of
          "no"       -> Right cf{confRenameNodes = Nothing}
          "by label" -> Right cf{confRenameNodes = Just ByLabel}
          "by order" -> Right cf{confRenameNodes = Just ByOrder}
          _ -> parseError attr value "no | by label | by order"
        "node name" ->
          let replaceN str x = case str of  
                ('$':'n':rs) -> show x ++ replaceN rs x
                ('$':'$':rs) -> '$' : replaceN rs x
                (c:rs) -> c : replaceN rs x
                [] -> []
          in Right cf{confNodeName = Just $ replaceN value}
        "shape circle" -> case confShapeMap cf of
          Nothing -> Right cf{confShapeMap = Just $ M.singleton (fromEnum $ Ge.Ellipse 0 0) value }
          Just m -> Right cf{confShapeMap = Just $ M.insert (fromEnum $ Ge.Ellipse 0 0) value m}
        "shape rectangle" -> case confShapeMap cf of
          Nothing -> Right cf{confShapeMap = Just $ M.singleton (fromEnum $ Ge.Rectangle 0 0) value }
          Just m -> Right cf{confShapeMap = Just $ M.insert (fromEnum $ Ge.Rectangle 0 0) value m}
        "arrow to" -> case confArrowMap cf of
          Nothing -> Right cf{confArrowMap = Just $ M.singleton (fromEnum $ ArrowTo) value }
          Just m -> Right cf{confArrowMap = Just $ M.insert (fromEnum $ ArrowTo) value m }
        "arrow from" -> case confArrowMap cf of
          Nothing -> Right cf{confArrowMap = Just $ M.singleton (fromEnum $ ArrowFrom) value }
          Just m -> Right cf{confArrowMap = Just $ M.insert (fromEnum $ ArrowFrom) value m }
        "arrow both" -> case confArrowMap cf of
          Nothing -> Right cf{confArrowMap = Just $ M.singleton (fromEnum $ ArrowBoth) value }
          Just m -> Right cf{confArrowMap = Just $ M.insert (fromEnum $ ArrowBoth) value m }
        "edge" -> case confArrowMap cf of
          Nothing -> Right cf{confArrowMap = Just $ M.singleton (fromEnum $ ArrowNone) value }
          Just m -> Right cf{confArrowMap = Just $ M.insert (fromEnum $ ArrowNone) value m }
        x -> parseError "TikZ Output" x "beamer | standalone | rename nodes | node name | shape circle | shape rectangle | arrow to | arrow both | arrow from | edge"

output universe conf =
  let conf' = updateConfig defaultConfiguration conf
      universe' = normalizeUniverse universe conf
  in
  case confStandalone conf' of
    Just True  -> standaloneTikz (confBeamer conf' == Just True) (localDraw conf' universe')
    Just False -> localDraw conf' universe

normalizeUniverse universe conf = 
  universe{uElements = A.array (A.bounds $ uElements universe) els'}
  where
    els = A.elems $ uElements universe
    labeledEls = filter hasLabel els
    labelF = case confNodeName conf of
      Nothing -> \i -> show i ++ "v"
      Just f -> f
    newLabels = A.array (A.bounds $ uElements universe) $ case confRenameNodes conf of
      Just ByLabel ->
        zipWith (\j (i,_) -> (i, labelF j)) [0..] $
          sortBy (\e1 e2 -> compare (snd e1) (snd e2)) $
            map (\e -> let Id i str = getId e in (i,str))
              labeledEls
      Just ByOrder -> zipWith (\j e -> let Id i _ = getId e in (i, labelF j)) [0..] labeledEls
    setLabel = case confRenameNodes conf of
      Nothing -> \_ str -> str
      _ -> \i _ -> (newLabels :: A.Array Int String) A.! i
    els' = map (\e -> let Id i _ = getId e in (i, case e of
      Element (Id i str) s@(Shape _ _) st att -> Element (Id i (setLabel i str)) s st att
      Element (Id i str) s@(Paragraph _ _) st att -> Element (Id i (setLabel i str)) s st att
      _ -> e)) els

hasLabel (Element _ (Shape _ _) _ _) = True
hasLabel (Element _ (Paragraph _ _) _ _) = True
hasLabel _ = False

tikzpicture universe conf d = concat
  [ "\\begin{tikzpicture}\n"
  , draw universe conf d
  , "\\end{tikzpicture}\n"]

standaloneTikz beamer picture = intercalate "\n"
  [ if beamer then "\\documentclass{beamer}"else "\\documentclass{standalone}"
  , "\\usepackage{tikz}"
  , "\\tikzset{"
  , "\tpictikz-circle/.style={"
  , "\tinner sep=1.0pt,"
  , "\tcircle,"
  , "\tdraw,"
  , "\tradius=3pt,"
  , "\tminimum size=6pt"
  , "\t},"
  , "\tpictikz-rectangle/.style={"
  , "\trectangle,"
  , "\tdraw,"
  , "\tinner sep=3pt,"
  , "\tminimum width=2cm"
  , "\t},"
  , "\tpictikz-edgeto/.style={"
  , "\t->,"
  , "\tshorten <=2pt,"
  , "\tshorten >=2pt,"
  , "\t},"
  , "\tpictikz-edgeboth/.style={"
  , "\t<->,"
  , "\tshorten <=2pt,"
  , "\tshorten >=2pt,"
  , "\t},"
  , "\tpictikz-edgefrom/.style={"
  , "\t<-,"
  , "\tshorten <=2pt,"
  , "\tshorten >=2pt,"
  , "\t},"
  , "\tpictikz-thick/.style={"
  , "\tvery thick"
  , "\t},"
  , "\tpictikz-dashed/.style={"
  , "\tthick,"
  , "\tdashed,"
  , "\t},"
  , "\tpictikz-dotted/.style={"
  , "\tthick,"
  , "\tdotted,"
  , "\t}"
  , "}"
  , ""
  , "\\definecolor{pictikz-black}{RGB}{0, 0, 0}"
  , "\\definecolor{pictikz-white}{RGB}{255, 255, 255}"
  , "\\definecolor{pictikz-gray}{RGB}{128, 128, 128}"
  , "\\definecolor{pictikz-red}{RGB}{255, 83, 83}"
  , "\\definecolor{pictikz-green}{RGB}{101, 237, 101}"
  , "\\definecolor{pictikz-blue}{RGB}{123, 168, 255}"
  , "\\definecolor{pictikz-yellow}{RGB}{246, 246, 82}"
  , "\\definecolor{pictikz-cyan}{RGB}{87, 239, 239}"
  , "\\definecolor{pictikz-purple}{RGB}{225, 103, 255}"
  , "\\definecolor{pictikz-light-green}{RGB}{178, 255, 66}"
  , "\\definecolor{pictikz-orange}{RGB}{255, 166, 61}"
  , "\\definecolor{pictikz-pink}{RGB}{255, 141, 255}"
  , ""
  , "\\begin{document}"
  , if beamer then "\\begin{frame}{}" else ""
  , "\\begin{tikzpicture}"
  , picture
  , "\\end{tikzpicture}"
  , if beamer then "\\end{frame}" else ""
  , "\\end{document}"
  ]

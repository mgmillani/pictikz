module Pictikz.Dot.Input (load, Configuration, defaultConfiguration, noConfiguration, updateConfig, parseConfig) where

import qualified Language.Dot.Parser as D
import qualified Language.Dot.Utils  as D
import qualified Language.Dot.Graph  as D
import qualified Data.DescriLo as De
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.Array.IArray as A
import           Data.List
import           Data.Char
import           Data.Maybe
import           Control.Monad

import           Pictikz.Elements
import qualified Pictikz.Geometry as Ge
import qualified Pictikz.Text as T
import           Pictikz.Parser
import           Pictikz.Utils

data Configuration = Configuration
  { confColors :: Maybe (Either [(Color, String)] String) }


defaultConfiguration =
  Configuration
  { confColors     = Just $ Left defaultColors
  }

noConfiguration =
  Configuration
  { confColors     = Nothing
  }

updateConfig old new =
  Configuration
  { confColors     = combineMaybe (confColors old)     (confColors new)
  }

parseConfig conf = foldM parseConfig' noConfiguration dotD
  where
    dotD = concatMap De.values $ filter (\c -> map toLower ( De.name c) == "dot input") conf
    parseConfig' cf (attr, value) =
      case map toLower attr of
        "colours" -> parseColors attr (words value) cf
        "colors"  -> parseColors attr (words value) cf
        x -> parseError "DOT Input" x "colours"

-- TODO: Make this independent of configuration and put it into a separate module (this is also used by Input.SVG).
parseColors attr [] cf = parseError attr "" "file <name> | rgb <r> <g> <b> | hsv <h> <s> <v>."
parseColors attr (v:vs) cf
  | map toLower v == "file" && not (null vs) = Right cf{confColors = Just $ Right $ head vs}
  | otherwise = do
    colourList <- onParseError (attr ++ " = " ++ v ++ " [...]") $ parseColorList (v:vs)
    return $ cf{confColors = Just $ Left colourList}

parseColorList [] = return []
parseColorList cs =
  let (colourName, colourDef) = span (\w -> map toLower w `notElem` ["rgb", "hsl"]) cs
  in case colourDef of
    (code:c1:c2:c3:cs) -> case map toLower code of
      "hsl" -> do
        let parsing = concat [code,c1,c2,c3]
        h <- onParseError parsing $ readIntDoubleRange 0 360 c1
        s <- onParseError parsing $ readIntDoubleRange 0 100 c2
        l <- onParseError parsing $ readIntDoubleRange 0 100 c3
        rs <- parseColorList cs
        return $ (fromHSL (fromIntegral h) (fromIntegral s) (fromIntegral l), unwords colourName) : rs
      "rgb" -> do
        let parsing = concat [code,c1,c2,c3]
        r <- onParseError parsing $ readIntDoubleRange 0 255 c1
        g <- onParseError parsing $ readIntDoubleRange 0 255 c2
        b <- onParseError parsing $ readIntDoubleRange 0 255 c3
        rs <- parseColorList cs
        return $ (RGB (fromIntegral r) (fromIntegral g) (fromIntegral b), unwords colourName) : rs
      x -> parseError (unwords  colourName) code "rgb | hsl"
    _ -> parseError (unwords colourName) (unwords colourDef) "rgb <r> <g> <b> | hsl <h> <s> <l>"

closestColor _ (NamedColor c) = NamedColor c
closestColor colorList color =
  let dists = map (\(c', n) -> (rgbDist c' color, n)) colorList
      (_, cname) = minimumBy (\(c0, _) (c1, _) -> compare c0 c1) dists
  in NamedColor cname

readPoint str = 
  let (xStr, yStr)   = span (/=',') str
  in (read xStr, read $ tail yStr)

fPoint f e = fPos (\p -> case p of Point (x,y) -> Point $ f (x,y) ; ElementRef x -> ElementRef x) e

parseColor ('#':str) = readColor $ '#':str 
parseColor (c:str)
  | isNumber c = 
    let [hS,sS,vS] = splitBy (\c -> c == ' ' || c == ',') (c:str)
        [h,s,v] = [read hS, read sS, read vS]
        l = (v - v*s/2)
        s' = if l == 0 || l == 1 then 0 else (v-l) / (min l (1 - l))
    in fromHSL h s' l
  | otherwise = NamedColor $ c:str

nodeDefaultStyle = noStyle
  { strokeColor = Just $ NamedColor "black" }

edgeDefaultStyle = noStyle
  { strokeColor = Just $ NamedColor "black"
  }

load dot conf = do
  ast <- D.parse dot
  let (strict, gType, _ , _) = ast
      edgeStr = if gType == D.Graph then "--" else "->"
      (nodes, edges) = D.adjacency ast
      (nodes', i) = makeNodes nodes
      (elements, n) = makeEdges edgeDefaultStyle{arrow = if gType == D.Graph then Just ArrowNone else Just ArrowTo} edgeStr i nodes' edges
      nodeList = M.elems nodes'
      points = [(x,y) | e <- nodeList, let Point (x,y) = getPos e, isPoint $ getPos e]
      (minX, minY) = (minimum $ map fst points, minimum $ map snd points)
      (maxX, maxY) = (maximum $ map fst points, maximum $ map snd points)
      dx = if null points then 0 else maxX - minX
      dy = if null points then 0 else maxY - minY
      s' = max dx dy
      s = if s' == 0 then 1 else s'
      universe = Universe
        { uOrigin = (minX, minY)
        , uScale = (s,s)
        , uAttributeScale = 1
        , uSize = (dx/s, dy/s)
        , uElements = A.array (0, n-1)
          [ (i, Element (Id i str) d st att)
          | (Element (Id i str) d' st att) <- M.elems elements
          -- | ((Element (Id _ str) d' st att), i) <- zip (M.elems elements) [0..]
          , let d = case d' of
                      Shape (Point (x,y)) (Ge.Ellipse rx ry) ->
                        Shape (Point ((x - minX)/s, (y - minY)/s)) (Ge.Ellipse (rx/s) (ry/s))
                      Shape (Point (x,y)) (Ge.Rectangle w  h) ->
                        Shape (Point ((x - minX)/s, (y - minY)/s)) (Ge.Rectangle (w/s)  (h/s))
                      _ -> d'
          ]
        }
  return $ universe
  where
    Just (Left colors) = confColors conf
    makeNodes ns = makeNodes' 0 ns M.empty
    makeNodes' i [] nodes = (nodes, i)
    makeNodes' i ((D.Node nameStr attributes):ns) nodes = 
      let 
          node0 = fromMaybe
            (Element (Id i nameStr)
                     (Shape (Point (0,0)) (Ge.Ellipse 1 1))
                     nodeDefaultStyle
                      { label = Just [[T.Text nameStr T.noFormat ]]
                      , labelPosition = Just [Around 0 0]}
                     M.empty)
            $ M.lookup nameStr nodes
          node = parseAttributes node0 $ map (\(var, val) -> (show var, show val)) attributes
          i' = if nameStr `M.member` nodes then i else i+1
          nodes' = M.insert nameStr node nodes
      in makeNodes' i' ns nodes'
    makeEdges edgeStyle edgeStr i elements [] = (elements, i)
    makeEdges edgeStyle edgeStr i elements ((D.Edge v u attributes):es) = 
      let Element eid (PolyLine _) st att = parseAttributes edge0 $ map (\(var, val) -> (show var, show val)) attributes
          (Id vi _) = getId $ elements M.! v
          (Id ui _) = getId $ elements M.! u
          eStr = v ++ edgeStr ++ u
          edge0 = fromMaybe (Element (Id i eStr) (PolyLine []) edgeStyle M.empty) $ M.lookup eStr elements
          i' = if eStr `M.member` elements then i else i+1
          edge = Element eid (PolyLine [ElementRef vi, ElementRef ui]) st att
          elements' = M.insert eStr edge elements
      in makeEdges edgeStyle edgeStr i' elements' es
    parseAttributes el atts =
      let st = getStyle el
          (el', insideL, outsideL) = foldl parseAttribute
              (el
              , liftM2 (,) (fmap (head . head) $ label st) (fmap head $ labelPosition st)
              , Nothing) atts
          st' = getStyle el'
      in setStyle el' st'
        { label = Just $ maybe [] (\(l,_) -> [[l]]) insideL ++ maybe [] (\(l,_) -> [[l]]) outsideL
        , labelPosition = Just $ maybe [] ( (:[]) . snd) insideL ++ maybe [] ( (:[]) . snd) outsideL
        }
      where
        parseAttribute :: (Read a, Fractional a)
          => (Element a, Maybe (T.Text, Position), Maybe (T.Text, Position) )
          -> (String, String)
          -> (Element a, Maybe (T.Text, Position), Maybe (T.Text, Position) )
        parseAttribute (el@(Element eid d st att), mInside, mOutside) (var, val) = 
          let Id x elName = eid
              elVar = case d of Shape _ _ -> "\\N" ; PolyLine _ -> "\\E" ; _ -> "\\N"
          in
          case map toLower var of
            "label" -> 
              ( el
              , Just (T.Text (replaceStr elVar elName val) T.noFormat, Around 0 0)
              , mOutside)
            "xlabel" -> 
              let oldP = maybe (Around 10 0) snd mOutside
              in ( el
                 , mInside
                 , Just (T.Text (replaceStr elVar elName val) T.noFormat, oldP)
                 )
            "xlp" -> 
              let (x,y) = readPoint val
                  (r,phi) = Ge.polarDistance (0,0) (x,y)
                  oldT = maybe (T.Text "" T.noFormat) fst mOutside
              in (el, mInside, Just (oldT, Around r phi))
            "fillcolor" -> (Element eid d st{fillColor = Just $ closestColor colors $ parseColor val} att, mInside, mOutside)
            "color" -> (Element eid d st{strokeColor = Just $ closestColor colors $ parseColor val} att, mInside, mOutside)
            "pos" ->
              let (x,y) = readPoint val
              in (Element eid (fPoint (\_ -> (x,y)) d) st att, mInside, mOutside)
            "penwidth" -> 
              let t = read val
              in (Element eid d st{thickness = Just t} att, mInside, mOutside)
            "dir" -> (Element eid d st{arrow =
              case map toLower val of
                "forward" -> Just ArrowTo
                "back" -> Just ArrowFrom
                "both" -> Just ArrowBoth
                _ -> Nothing}
              att, mInside, mOutside)
            "shape" -> 
              let Shape p s0 = d
                  (w', h') = case d of
                    Shape _ (Ge.Rectangle w h) -> (w,h)
                    Shape _ (Ge.Ellipse rx ry) -> (2*rx, 2*ry)
                    _ -> (0,0)
                  (s', att') = case map toLower val of
                    "circle"    -> (Ge.Ellipse (w'/2) (h'/2), att)
                    "rectangle" -> (Ge.Rectangle w' h', att)
                    s -> (s0, M.insert "shape" (Just s) att)
              in (Element eid (Shape p s') st att', mInside, mOutside)
            "height" ->
              let h = read val
                  d' = case d of
                    Shape p (Ge.Rectangle w _) -> Shape p (Ge.Rectangle w h)
                    Shape p (Ge.Ellipse rx _) -> Shape p (Ge.Ellipse rx (h/2))
                    _ -> d
              in (Element eid d' st att, mInside, mOutside)
            "width" ->
              let w = read val
                  d' = case d of
                    Shape p (Ge.Rectangle _ h) -> Shape p (Ge.Rectangle w h)
                    Shape p (Ge.Ellipse _ ry) -> Shape p (Ge.Ellipse (w/2) ry)
                    _ -> d
              in (Element eid d' st att, mInside, mOutside)
            _ -> (Element eid d st att, mInside, mOutside) -- TODO: do something with the remaining attributes
            --_ -> Element eid d st $ M.insert var (if val == "" then Nothing else Just val) att

{- Potentially relevant DOT attributes
  arrowhead
  arrowsize
  arrowtail
  bgcolor -- color of canvas
  color
  decorate?
  dir -- direction of arrows. One of none, both, back, forward
  fillcolor
  fixedsize -- true (size is given by attributes width and height), false (size is given by label), shape.
  fontcolor
  fontsize
  height
  id
  label
  labelangle
  labeldistance
  labelfontcolor
  labelfontsize
  layer
  layerlistsep
  layers
  pos
  regular
  root
  rotation
  shape
  sides
  size
  splines
  style
  width
  xlabel
  xlp -- xlabel position
-}

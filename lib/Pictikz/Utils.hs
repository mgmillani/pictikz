module Pictikz.Utils where

import Text.Read (readEither)
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad.Trans.State
import Control.Monad
import Data.List

average xs = realToFrac $ (sum xs) / genericLength xs

epsilon = 1e-6

csl [] = []
csl [x] = x
csl (x:xs) = x ++ concatMap (\l -> ',':' ': l ) xs

combineMaybe a Nothing = a
combineMaybe a b = b

readEither' str = either (\x -> Left [x]) Right $ readEither str

interleave (x:xs) (y:ys) = x:y: interleave xs ys
interleave [] ys = ys
interleave xs [] = xs

replaceStr pattern sub [] = []
replaceStr pattern sub str = 
  case replaceHere pattern str of
    Just str' -> sub ++ replaceStr pattern sub str'
    Nothing -> (head str) : replaceStr pattern sub (tail str)
  where
    replaceHere [] str = Just str
    replaceHere _ [] = Nothing
    replaceHere (p:ps) (s:ss)
      | p == s = replaceHere ps ss
      | otherwise = Nothing

lookupClosest s v = 
  let v0 = M.lookupLE v s
      v1 = M.lookupGE v s
  in case (v0,v1) of
      (Nothing, Just (_,u1)) -> Just u1
      (Just (_,u0), Nothing) -> Just u0
      (Just (k0,u0), Just (k1,u1)) -> if v - k0 > k1 - v then Just u1 else Just u0
      (Nothing, Nothing) -> Nothing

lookupClosestSet s v =
  let v0 = S.lookupLE v s
      v1 = S.lookupGE v s
  in case (v0,v1) of
      (Nothing, Just u1) -> Just u1
      (Just u0, Nothing) -> Just u0
      (Just u0, Just u1) -> if v - u0 > u1 - v then Just u1 else Just u0
      (Nothing, Nothing) -> Nothing
 
-- | Whether two numbers are almost the same.
-- Useful for comparing doubles, where equal numbers may be slightly
-- different due to floating-point calculation errors.
almostEqual t x y = (1 + abs (x - y)) / (1 + (min (abs x) (abs y))) < (1 + t)

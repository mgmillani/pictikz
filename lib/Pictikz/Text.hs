--  Copyright 2018 Marcelo Garlet Millani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.Text where

import Pictikz.Utils

data Position = Subscript | Superscript | Normalscript deriving (Eq, Show)
data AlignmentType = LeftAligned | RightAligned | Centered deriving (Eq, Show)

data Format =
  Format
    { alignment :: Maybe AlignmentType
    , bold :: Maybe Bool
    , color :: Maybe String
    , italics :: Maybe Bool
    , position :: Maybe Position
    }
  deriving (Eq, Show)

data Text = Text String Format deriving (Eq, Show)

noFormat = Format 
  { alignment = Nothing
  , bold      = Nothing
  , color     = Nothing
  , italics   = Nothing
  , position  = Nothing
  }
defaultFormat = Format 
  { alignment = Just LeftAligned
  , bold      = Just False
  , color     = Just ""
  , italics   = Just False
  , position  = Just Normalscript
  }

updateFormat old new = 
  Format
    { alignment = combineMaybe (alignment old) (alignment new)
    , bold      = combineMaybe (bold old) (bold new)
    , color     = combineMaybe (color old) (color new)
    , italics   = combineMaybe (italics old) (italics new)
    , position  = combineMaybe (position old) (position new)
    }


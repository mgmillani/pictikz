module Pictikz.Clustering where

import Pictikz.Utils

import qualified Data.Array.Unboxed as U
import qualified Data.Set as S

import Data.List
import Data.Fixed

data ClusterValue a = Unbounded a | Pinned a | Cyclic a a deriving (Show)

getClusterValue (Unbounded x) = x
getClusterValue (Pinned x) = x
getClusterValue (Cyclic x _) = x

isPinned (Pinned _) = True
isPinned _ = False

instance (Eq a, Ord a, Num a) => Num (ClusterValue a) where
  (Pinned a) + _ = Pinned a
  _ + (Pinned a) = Pinned a
  (Unbounded a ) + (Unbounded b) = Unbounded $ a + b
  (Cyclic x mx ) + (Cyclic y _)  =
    let z = x + y
    in Cyclic (if z >= mx then z - mx else z) mx
  (Cyclic x mx ) * (Cyclic y _)  =
    let z = x * y
    in Cyclic (if z >= mx then z - mx else z) mx
  (Unbounded a ) * (Unbounded b) = Unbounded $ a * b
  _ * (Pinned a) = Pinned a
  (Pinned a) * _ = Pinned a
  abs (Unbounded x) = Unbounded $ abs x
  abs x@(Cyclic _ _) = x
  abs (Pinned x) = Pinned $ abs x
  signum (Unbounded x) = Unbounded $ signum x
  signum (Cyclic x mx) = Cyclic (signum x) mx
  signum (Pinned x) = Pinned $ signum x
  fromInteger x = Unbounded $ fromInteger x
  negate (Unbounded x) = Unbounded $ negate x
  negate (Cyclic x mx) = Cyclic (mx - x) mx
  negate (Pinned x) = Pinned $ negate x
  a - (Cyclic x mx) =
    let y = getClusterValue a
    in Cyclic (if y - x < 0 then y - x + mx else y - x) mx
  (Pinned x) - a = Pinned $ x - getClusterValue a
  (Unbounded x) - a = Unbounded $ x - getClusterValue a
  (Cyclic x mx) - a =
    let y = getClusterValue a
    in Cyclic (if x - y < 0 then x - y + mx else x - y) mx

instance Eq a => Eq (ClusterValue a) where
  x == y = (getClusterValue x) == (getClusterValue y)

instance (Eq a, Ord a) => Ord (ClusterValue a) where
  compare a b = compare (getClusterValue a) (getClusterValue b)

instance (Eq a, Ord a, Fractional a) => Fractional (ClusterValue a) where
  fromRational x = Unbounded $ fromRational x
  recip (Unbounded x) = Unbounded $ recip x
  recip (Pinned x) = Pinned x
  recip (Cyclic x mx) = Cyclic (recip x) mx

instance (Eq a, Ord a, Real a) => Real (ClusterValue a) where
  toRational x = toRational $ getClusterValue x

toClusterAngle phi = phi `mod'` (pi/2)
quadrant phi = fromIntegral $ phi `div'` (pi/2)

data CurveClusteringInfo a = 
  CurveClusteringInfo
  { clusterValue :: a
  , valueIndex :: Int
  , previousNodeValue :: a
  , previousNodeIndex :: Int
  , clusterStep :: a
  , clusterSize :: Int
  } deriving (Show, Eq)


clusterValuesBy groupf d vs = 
  let vs' = sort vs
      dn = max 1e-6 $ d * (maximum vs' - minimum vs')
      clustered = concat $ groupf dn vs'
  in S.fromList clustered

genGroup f d [] = []
genGroup f d as =
  let (bs, r1) = f d as in bs : genGroup f d r1

distanceGroup d0 [] = ([], [])
distanceGroup d0 (a:as) = group' d0 d0 0 a (a:as)
  where
    group' d d0 l a0 as =
      let (g, rest) = span (\x -> x - a0 < d0) as
          a1 = average g
          l1 = genericLength g
          d1 = 0.55 * d
      in if null g then (replicate l a0, rest) else group' d d1 (l + l1) a1 rest

isometricGroup d0 as =
  let gs = genGroup distanceGroup d0 as
      gmin = minimum $ head gs
      gmax = maximum $ last gs
      n = fromIntegral $ length gs
      step = if n == 1 then 0 else (gmax - gmin) / (n-1)
  in zipWith (\g i -> map (\x -> i) g) gs [gmin,gmin+step..]


gridOnGridCurveClustering step cci = (ni - 1) * step + i * (step / (c + 1))
  where
    ni = fromIntegral $ previousNodeIndex cci
    i = fromIntegral $ valueIndex cci
    c = fromIntegral $ clusterSize cci

gridOnClusterCurveClustering cci = n + i * step 
  where
    n = previousNodeValue cci
    i = fromIntegral $ valueIndex cci
    step = clusterStep cci

regularClustering nClusters v0 vn = (\(i, (v,ci)) -> (i, v0 + (ci-1) * vStep))
  where
    vStep
      | nClusters == 1 = 1
      | otherwise = (vn - v0) / (nClusters - 1)

regularAngleClustering :: (Floating a, Fractional a) => CurveClusteringInfo a -> a
regularAngleClustering cci
  | clusterSize cci == 0 = clusterValue cci
  | otherwise = (previousNodeValue cci + step * (fromIntegral $ valueIndex cci))
  where
    step = (clusterStep cci) / (fromIntegral $ (clusterSize cci))

simpleClustering (i, (v,_)) = (i, v)

numberAttributes getAttr os = numberAttributes' 0 os
  where
    numberAttributes' i [] = ([], [], i)
    numberAttributes' i (o:os) = ([i..i+j - 1] : oi, (zip [i..] a) ++ as, n)
      where
        (j, a) = getAttr o
        (oi, as, n) = numberAttributes' (i+j) os

slidingWindow maxD decay vals = slidingWindow' 1 vals'
  where
    vals' = sortBy (\(x, v0) (y, v1) -> compare v0 v1) vals
    slidingWindow' c [] = ([], c, [])
    slidingWindow' c ((x,v) : vals) = 
      let (cluster, r) = slideWindow maxD v vals
          v' = average $ v : map snd cluster
          cluster' = map (\(x,v) -> (x,(v',c))) cluster
          (clusters, c', clusterVals) = slidingWindow' (c+1) r
      in if null r then ((x,(v',c)):cluster', c, [v']) else (((x,(v',c)):cluster') ++ clusters, c', v' : clusterVals)
    slideWindow d v vals = 
      let (cluster, r) = span (\(x,v') -> (v' - v < d)) vals
          v' = average $ map snd cluster
          d' = decay * d
          (cluster', r') = slideWindow d' v' r
      in if null cluster then ([],r) else (cluster ++ cluster', r')

clusterValues ::
    (Real b, Fractional a, Fractional b, Num d)
     => b
     -> ((c, (a, d)) -> (Int, e))
     -> Int
     -> [(c, b)]
     -> (U.Array Int e, d, [a])
clusterValues maxD updateF n vals = 
  let (vals',c, clusterVals) = slidingWindow maxD 0.55 vals
      vals'' = map updateF vals'
      arrVal = U.array (0, n - 1 :: Int) vals''
  in (arrVal, c, clusterVals)

clusterValuesWithPinned ::
  (Real b1, Fractional b1, Fractional a1, Num b2, Ord a1)
     => a1
     -> b1
     -> (CurveClusteringInfo a1 -> e)
     -> Int
     -> [(Int, ClusterValue b1)]
     -> [b1]
     -> (U.Array Int e, b2, [a1])
clusterValuesWithPinned w maxD clusterF n vals pinned = 
  let pinned' = map (\v -> (-1, Pinned v)) pinned
      maxD' = Unbounded maxD
      (vals', c, clusterVals) = slidingWindow maxD' 0.55 (pinned' ++ vals)
      realV = [ (i, getClusterValue v) | (i, (v, _)) <- vals', i /= (-1) ]
      pinnedV = [ getClusterValue v | (i, (v, _)) <- vals', i == (-1) ]
      vals'' = fitBetweenPinned w realV pinnedV
      arrV = U.array (0, n - 1 :: Int) [ (i :: Int, clusterF cci) | (i, cci) <- vals'', i /= (-1)]
  in (arrV, c, map getClusterValue clusterVals)

regularClusterValues maxD n vals = 
  let (v0, vn) = (minimum $ map snd vals, maximum $ map snd vals)
      (vals', nClusters, _) = clusterValues maxD (regularClustering nClusters v0 vn) n vals
  in vals'

fitBetweenPinned :: (Fractional a, Ord a) => a -> [(b, a)] -> [a] -> [(b, CurveClusteringInfo a)]
fitBetweenPinned w realVals pinnedVals = 
  let defaultCurveClusteringInfo = CurveClusteringInfo
        { clusterValue = 0
        , valueIndex = 0
        , previousNodeValue = 0
        , previousNodeIndex = 0
        , clusterStep = 0
        , clusterSize = 0
        }
      w0 = case pinnedVals of
        (v0:v1:_) -> v1 - v0
        _ -> w
      fitBetweenPinned' _ [] _ _ = []
      fitBetweenPinned' w vs cci (pv:pvr) = 
        let (vs0, vr) = span ((< pv) . snd) vs
            group0 = groupBy (\o0 o1 -> (snd o0) == (snd o1)) vs0
            l0 = length group0
            step = w / (fromIntegral $ 1 + l0)
            vs0' =
              concat $ zipWith
                (\xs i -> map
                  (\(o,v) -> (o, cci
                    {clusterValue = v, valueIndex = i, clusterStep = step, clusterSize = l0}))
                  xs)
                group0
                [1..]
            (vrCorner, vr') = span ((==pv) . snd) vr
            vrCorner' = map (\(o,v) ->
              (o, cci
                { clusterValue = v
                , valueIndex = 0
                , previousNodeValue = pv
                , previousNodeIndex = 1 + previousNodeIndex cci
                , clusterStep = 0
                , clusterSize = 0
                })) vrCorner
            w' = if null pvr then w else (head pvr) - pv
        in vs0' ++ vrCorner' ++ fitBetweenPinned' w' vr' cci{previousNodeValue = pv, previousNodeIndex = 1 + previousNodeIndex cci} pvr
      fitBetweenPinned' w vs cci [] = 
        let group0 = groupBy (\o0 o1 -> (snd o0) == (snd o1)) vs
            l0 = length group0
            step = w / (fromIntegral $ 1 + l0)
        in concat $ zipWith
          (\xs i -> map
            (\(o,v) -> (o, cci
              { clusterValue = v
              , valueIndex = i
              , clusterStep = step
              , clusterSize = l0
              }))
            xs)
          group0
          [1..]
  in fitBetweenPinned' w0 realVals defaultCurveClusteringInfo{ previousNodeValue = (- w0), previousNodeIndex = 0} pinnedVals


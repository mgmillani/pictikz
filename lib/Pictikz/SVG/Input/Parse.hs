module Pictikz.SVG.Input.Parse where

import Pictikz.SVG.Input.Configuration

import           Pictikz.Elements hiding (CurveSegment(..))
import qualified Pictikz.Elements as E (CurveSegment(..))
import           Pictikz.Parser
import           Pictikz.XML
import qualified Pictikz.Geometry as Ge
import qualified Pictikz.Text  as T

--import qualified Data.Array.IArray  as A
--import qualified Data.Array.Unboxed as U
import Data.Char
import Data.List
import Data.Matrix hiding (trace)
import Data.Maybe
import qualified Data.Map as M
import Control.Monad
import Control.Monad.Trans.State

import qualified Text.XML.Light as X

-- defaultText = T.Text "" T.noFormat
defaultPar  = Paragraph (Point (0,0)) [] -- [T.Text "" T.noFormat]
defaultRectangle = (Ge.Rectangle 0 0, (0,0), "",  noStyle, identity 3)
defaultEllipse  = (Ge.Ellipse  0 0, (0,0), "",  noStyle, identity 3)

parseElements conf matrix svg = parseElements' matrix svg
  where
    Just (Left colors) = confColours conf
    parseElements' matrix (X.Elem element)
      -- Objects
      | X.qName ( X.elName element) == "rect" = do
        let (shape, pos, eid, style, m2) = foldl parseShape defaultRectangle $ X.elAttribs element
            (Ge.Rectangle rw rh) = shape
        (i, rs) <- get
        put $ (i+1, updateRefSize rs $ (rw + rh)/2)
        return [Object $ transform (matrix * m2) (Element (Id i eid) (Shape (Point pos) shape) style M.empty)]
      | X.qName ( X.elName element) `elem` ["ellipse", "circle"] = do
        let (shape, pos, eid, style, m2) = foldl parseShape defaultEllipse $ X.elAttribs element
            (Ge.Ellipse rw rh) = shape
        (i, rs) <- get
        put $ (i+1, updateRefSize rs $ rw + rh)
        return [Object $ transform (matrix * m2) (Element (Id i eid) (Shape (Point pos) shape) style M.empty)]
      -- Transformations
      | X.qName ( X.elName element) == "defs" = return []
      | X.qName ( X.elName element) == "g" = do
        let matrix' = foldl parseG matrix $ X.elAttribs element
        rest' <- mapM (parseElements' matrix' ) $ X.elContent element
        let rest = concat rest'
        return $ if satisfyAttrib element "id" (\v -> "layer" `isPrefixOf` map toLower v) || satisfyAttrib element "groupmode" (=="layer")
           then [Group rest]
           else rest
      -- Edges
      | X.qName ( X.elName element) == "path" = do
        (i, rs) <- get
        let el = foldl parseEdge (Element (Id i "") (PolyLine []) noStyle M.empty) $ X.elAttribs element
            st = getStyle el
            t = fromMaybe 0 $ thickness st
        put $ (i+1, updateRefSize rs $ 5*t)
        return [Object $ transform matrix el]
      -- Text
      | X.qName ( X.elName element) == "text" = do
        (i, s) <- get
        put $ (i + 1, s)
        return [Object $ transform matrix $ parseParagraph (Element (Id i "") defaultPar noStyle M.empty) (X.Elem element)]
      | otherwise = do
        rest <- mapM (parseElements' matrix ) $ X.elContent element
        return $ concat rest
    parseElements' _ _ = return []
    transform matrix (Element eid drawing style atts) =
      let drawing' = case drawing of
            PolyLine coords -> PolyLine $ map (transformCoordinate matrix) coords
            Curve coords segs -> Curve (map (transformCoordinate matrix) coords) (map (transformControlPoint matrix) segs)
            Shape p shape -> Shape (transformCoordinate matrix p) shape
            Paragraph p text -> Paragraph (transformCoordinate matrix p) text
      in Element eid drawing' style atts
    transformCoordinate _ c@(ElementRef _) = c
    transformCoordinate matrix (Point (x,y)) = Point (x',y')
      where
        [x', y', _] = toList $ matrix * fromList 3 1 [x,y,1]
    transformControlPoint matrix (E.Cubic (x1, y1) (x2, y2)) = E.Cubic (x1', y1') (x2', y2')
      where
        [x1', x2', y1', y2', _, _] = toList $ matrix * fromList 3 2 [x1,x2,y1,y2,1,1]
    transformControlPoint _ s = s
    parseG matrix attr
      | "transform" == (X.qName $ X.attrKey attr) = matrix * parseTransform ( X.attrVal attr)
      | otherwise = matrix
    parseShape (Ge.Rectangle w h, (x,y), id, style, matrix) attr
      | "id" == X.qName ( X.attrKey attr) = (Ge.Rectangle w h, (x,y), (X.attrVal attr), style,  matrix)
      | "x"  == X.qName ( X.attrKey attr) = (Ge.Rectangle w h, (read $ X.attrVal attr, y), id, style, matrix)
      | "y"  == X.qName ( X.attrKey attr) = (Ge.Rectangle w h, (x, read $ X.attrVal attr), id, style, matrix)
      | "height" == X.qName ( X.attrKey attr) = (Ge.Rectangle w (read $ X.attrVal attr ), (x,y), id,  style, matrix)
      | "width"  == X.qName ( X.attrKey attr) = (Ge.Rectangle (read $ X.attrVal attr ) h, (x,y), id, style, matrix)
      | "transform"  == X.qName ( X.attrKey attr) = (Ge.Rectangle w h, (x,y), id, style, matrix * parseTransform (X.attrVal attr))
      | "style"      == X.qName ( X.attrKey attr) =
        let fields = parseStyle $ X.attrVal attr
            style' = foldl makeStyle noStyle fields
        in (Ge.Rectangle w h, (x,y), id, updateStyle style style', matrix)
      | otherwise = (Ge.Rectangle w h, (x,y), id, style, matrix)
    parseShape (Ge.Ellipse rx ry, (x,y), id, style, matrix) attr
      | "id" == X.qName (X.attrKey attr)  = (Ge.Ellipse rx ry, (x,y), X.attrVal attr,  style, matrix)
      | "cx" == X.qName (X.attrKey attr)  = (Ge.Ellipse rx ry, (read $ X.attrVal attr, y), id,  style, matrix)
      | "cy" == X.qName (X.attrKey attr)  = (Ge.Ellipse rx ry, (x, read $ X.attrVal attr), id,  style, matrix)
      | "rx" == X.qName (X.attrKey attr)  = (Ge.Ellipse (read $ X.attrVal attr ) ry, (x,y), id,  style, matrix)
      | "ry" == X.qName ( X.attrKey attr) = (Ge.Ellipse rx (read $ X.attrVal attr ), (x,y), id,  style, matrix)
      | "r"  == X.qName ( X.attrKey attr) = (Ge.Ellipse (read $ X.attrVal attr ) (read $ X.attrVal attr ), (x,y), id,  style, matrix)
      | "transform"  == X.qName (X.attrKey attr) = (Ge.Ellipse rx ry, (x,y), id,  style, matrix * (parseTransform $ X.attrVal attr))
      | "style"      == X.qName (X.attrKey attr) =
        let fields = parseStyle $ X.attrVal attr
            style' = foldl makeStyle noStyle fields
        in (Ge.Ellipse rx ry, (x,y), id,  updateStyle style style', matrix)
      | otherwise = (Ge.Ellipse rx ry, (x,y), id, style, matrix)
    -- Paragraph
    parseParagraph (Element eid (Paragraph _ text) st atts) (X.Elem element) =
      let (x,y, matrix) = foldl parseCoordinates (0,0, identity 3) $ X.elAttribs element
          style = fromMaybe [] $ X.findAttr (X.blank_name{X.qName = "style"}) element
          format' = foldl parseFormat T.noFormat $ parseStyle style
          text'   = concatMap (parseName (T.Text "" format') y) $ X.elContent element
      in transform matrix $ Element eid (Paragraph (Point (x, y)) (text ++ text')) st atts
    -- undefined cases are listed here so we get a warning in case there are new constructors
    parseParagraph (Element _ (PolyLine _) _ _) _ = undefined
    parseParagraph (Element _ (Shape _ _) _ _) _ = undefined
    parseParagraph (Element _ (Paragraph _ _) _ _) _ = undefined
    parseName (T.Text str format) _ (X.Text text) = [T.Text (str ++ X.cdData text) format]
    parseName (T.Text str format) y (X.Elem element) =
      let style = fromMaybe [] $ X.findAttr (X.blank_name{X.qName = "style"}) element
          y' = maybe y read $ X.findAttr (X.blank_name{X.qName = "y"}) element
          newline = if y' == y then "" else "\n"
          format' = foldl parseFormat T.defaultFormat $ parseStyle style
          rs = X.elContent element
          str' = dropWhile isSpace str
          text' = if null str' then [] else [T.Text (newline ++ str) (T.updateFormat format format')]
      in if null rs then text'
         else
          text' ++ map (\(T.Text s fm) -> T.Text (newline ++ s) fm) ( concatMap (parseName (T.Text "" (T.updateFormat format format')) y') rs)
    parseName (T.Text _ _) _ _ = undefined
    parseFormat format (var, val)
      | var == "text-align" =
          format{T.alignment = case val of
            "center" -> Just T.Centered
            "end"    -> Just T.RightAligned
            "start"  -> Just T.LeftAligned
            _ -> Nothing
          }
      | var == "font-weight" = format{T.bold = Just $ val == "bold"}
      | var == "font-style"  = format{T.italics = Just $ val == "italic"}
      | var == "baseline-shift" =
          format{T.position = case val of
            "sub"      -> Just T.Subscript
            "super"    -> Just T.Superscript
            "baseline" -> Just T.Normalscript
            _ -> Nothing
          }
      | var == "fill" =
          format{T.color = case val of
            "none" -> Nothing
            color -> Just $ closestColor color colors
          }
      | otherwise = format
    -- Edge
    parseEdge (Element eid curve st att) attr
      | "d" == X.qName (X.attrKey attr) =
        let path = parsePath $ X.attrVal attr
        in Element eid path st att
      | "style"        == X.qName (X.attrKey attr) =
        let fields = parseStyle $ X.attrVal attr
            style = foldl makeStyle noStyle fields
        in Element eid curve (updateStyle st style) att
      | otherwise = Element eid curve st att
    -- undefined cases are listed here so we get a warning in case there are any new constructors
    parseEdge (Container _ _ _ _) _ = undefined
    makeStyle :: Style Double -> (String, String) -> Style Double
    makeStyle st x = case x of
      ("marker-end", "none")        -> st{arrow = Just $ joinArrow (fromMaybe ArrowNone $ arrow st) ArrowNone }
      ("marker-end", _)             -> st{arrow = Just $ joinArrow (fromMaybe ArrowNone $ arrow st) ArrowTo   }
      ("marker-start", "none")      -> st{arrow = Just $ joinArrow (fromMaybe ArrowNone $ arrow st) ArrowNone }
      ("marker-start", _)           -> st{arrow = Just $ joinArrow (fromMaybe ArrowNone $ arrow st) ArrowFrom }
      ("fill", "none")              -> st
      ("fill", color)               -> st{fillColor = Just $ NamedColor $ closestColor color colors}
      ("stroke", "none")            -> st
      ("stroke", color)             -> st{strokeColor = Just $ NamedColor $ closestColor color colors}
      ("stroke-width", len)         -> st{thickness = Just $ readLength len}
      ("stroke-dasharray", "none")  -> st
      ("stroke-dasharray", dashes)  -> let dash = splitBy (==',') dashes in st{dashPattern = Just $ map read dash}
      _ -> st 
    parseCoordinates (x,y, matrix) attr
      | "x" == X.qName (X.attrKey attr) = (read $ X.attrVal attr, y, matrix)
      | "y" == X.qName (X.attrKey attr) = (x,(read $ X.attrVal attr), matrix)
      | "transform"  == X.qName (X.attrKey attr) = (x,y, matrix * parseTransform  (X.attrVal attr))
      | otherwise = (x,y, matrix)

closestColor colorName colorList =
  let color = readColor colorName
      dists = map (\(c', n) -> (rgbDist c' color, n)) colorList
      (_, cname) = minimumBy (\(c0, _) (c1, _) -> compare c0 c1) dists
  in cname

readLength len =
  let (n,u) = span (\x -> isNumber x || x == '.') len in read n * mmFactor u

parseStyle style = map (\f -> let (k,v) = span (/=':') f in (k,tail v) ) $ splitBy (==';') style

parseTransform = evalState parseTransform'
  where
    --  parseTransform' :: Floating a => State String (Matrix a)
    parseTransform' = do
      f <- function
      ps <- parameters
      let m = buildTransform  f ps
      rest <- whenNotEmpty (identity 3) parseTransform'
      return $ m * rest
    function :: State String String
    function = do
      consumeWhile (\x -> x `elem` [' ','\t'])
      f <- consumeWhile isAlpha
      return $ map toLower f
    --  parameters :: Floating a => State String [a]
    parameters = do
      consumeWhile  (`elem` [' ', '(', '\t'])
      ps <- consumeWhile (/= ')')
      consumeWhile  (`elem` [' ', ')', '\t'])
      let params = splitBy (`elem` ", ") ps
      return $ map read params
    buildTransform :: Floating a => String -> [a] -> Matrix a
    buildTransform "scale" [sx]     = Ge.scale sx sx
    buildTransform "scale" [sx,sy]  = Ge.scale sx sy
    buildTransform "translate" [tx]     = Ge.translate tx 0
    buildTransform "translate" [tx, ty] = Ge.translate tx ty
    buildTransform "rotate" [a]        = Ge.rotate (pi * a / 180)
    buildTransform "rotate" [a,cx ,cy] = Ge.translate cx cy * Ge.rotate (pi * a / 180) * Ge.translate (-cx) (-cy)
    buildTransform "matrix" [a,b,c,d,e,f] = fromList 3 3 [a,c,e,b,d,f,0,0,1]
    buildTransform "skewx" [a] = Ge.skewx a
    buildTransform "skewy" [a] = Ge.skewy a
    buildTransform _ _ = identity 3

data Segment a b = 
    CubicBezier a a a -- ^ Last point and both control points
  | QuadraticBezier a a   -- ^ Last point and one control point
  | Arc a a b Bool Bool
  | Line a

isCurveSegment (Line _) = False
isCurveSegment _ = True

convertToCurve :: (Ord a, Floating a) => [Segment (a,a) a] -> Drawing a
convertToCurve path = Curve ps (tail ss) -- $ fixControlPoints ps $ tail ss
  where
    Curve ps ss = foldr convert (Curve [] []) path
    {-fixControlPoints (p0:p3:ps) ((E.Cubic (x1, y1) (x2, y2)):ss) = 
      E.Cubic v1 v2 : fixControlPoints (p3:ps) ss
      where
        Point (x0,y0) = p0
        Point (x3,y3) = p3
        v1 = (x1 - x0, y1 - y0)
        v2 = (x2 - x3, y2 - y3)
    fixControlPoints (p0:p3:ps) (s:ss) = 
      s : fixControlPoints (p3:ps) ss
    fixControlPoints _ _ = []-}
    convert (Line q) (Curve ps ss) =
      Curve (Point q : ps) (E.Line : ss)
    convert (CubicBezier p3 p1 p2) (Curve ps ss) =
      Curve (Point p3 : ps) (E.Cubic p1 p2 : ss)
    convert (QuadraticBezier p2 p1) (Curve ps ss) =
      Curve (Point p2 : ps) (E.Cubic p1 p1 : ss)
    convert (Arc p r phi largeArc sweep) (Curve (Point q:ps) ss) =
      Curve (Point p:Point q:ps) (arcFromEndpoints p q r phi largeArc sweep : ss)

parsePath str
  | any isCurveSegment path = convertToCurve path
  | otherwise = PolyLine $ map (\(Line p) -> Point p) path  
  where
    path = evalState parsePath' str
    parsePath' = do
      c <- command
      processCommand (0,0) (Right (0,0)) Nothing c
    processCommand (cx,cy) first q2 cmd
      | cmd == "m" || cmd == "l" = do
        (x,y) <- coordPair
        c <- command
        let this = (x + cx, y + cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      | cmd == "M" || cmd == "L" = do
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      | cmd == "H" = do
        x <- value
        c <- command
        let this = (x, cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      | cmd == "h" = do
        x <- value
        c <- command
        let this = (cx + x, cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      | cmd == "V" = do
        y <- value
        c <- command
        let this = (cx, y)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      | cmd == "v" = do
        y <- value
        c <- command
        let this = (cx, cy + y)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Line this : rest
      -- Cubic Bezier
      | cmd == "c" = do
        (x1, y1) <- coordPair
        p2@(x2, y2) <- coordPair
        (x,y) <- coordPair
        c <- command
        let this = (x + cx, y + cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (CubicBezier this (x1 + cx, y1 + cy) (x2 + cx, y2 + cy) ): rest
      | cmd == "C" = do
        p1 <- coordPair 
        p2 <- coordPair
        p0 <- coordPair
        c <- command
        let this = p0
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (CubicBezier p0 p1 p2) : rest
      | cmd == "s" = do
        (x2, y2) <- coordPair
        (x,y) <- coordPair
        c <- command
        let this = (x + cx, y + cy)
            p1 = case q2 of
              Nothing -> (cx, cy)
              Just (x1,y1) -> 
                let dx = x1 - cx
                    dy = y1 - cy
                in (cx - dx, cy - dy)
            p2 = (x2 + cx, y2 + cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (CubicBezier this p1 p2): rest
      | cmd == "S" = do
        p2 <- coordPair
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
            p1 = case q2 of
              Nothing -> (cx, cy)
              Just (x1, y1) ->
                let dx = x1 - cx
                    dy = y1 - cy
                in (cx - dx, cy - dy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (CubicBezier this p1 p2) : rest
      -- Quadratic Bezier
      | cmd == "Q" = do
        p2 <- coordPair
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (QuadraticBezier this p2) : rest
      | cmd == "q" = do
        (x2, y2) <- coordPair
        (x,y) <- coordPair
        c <- command
        let this = (x + cx, y + cy)
            p2 = (x2 + cx, y2 + cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p2) (if c == "" then cmd else c)
        return $ (QuadraticBezier this p2): rest
      | cmd == "T" = do
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
            p1 = case q2 of
              Nothing -> (cx, cy)
              Just (x1, y1) ->
                let dx = x1 - cx
                    dy = y1 - cy
                in (cx - dx, cy - dy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p1) (if c == "" then cmd else c)
        return $ (QuadraticBezier this p1) : rest
      | cmd == "t" = do
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
            p1 = case q2 of
              Nothing -> (cx, cy)
              Just (x1, y1) ->
                let dx = x1 - cx
                    dy = y1 - cy
                in (cx - dx, cy - dy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) (Just p1) (if c == "" then cmd else c)
        return $ (QuadraticBezier this p1) : rest
      -- Elliptical arc
      | cmd == "A" = do
        rx <- value
        ry <- value
        rot <- value
        largearc <- value
        sweep <- value
        (x,y) <- coordPair
        c <- command
        let this = (x, y)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Arc this (rx, ry) rot (largearc == 1) (sweep == 1) : rest
      | cmd == "a" = do
        rx <- value
        ry <- value
        rot <- value
        largearc <- value
        sweep <- value
        (x,y) <- coordPair
        c <- command
        let this = (x + cx, y + cy)
        rest <- whenNotEmpty [] $
          processCommand this (first >> Left this) Nothing (if c == "" then cmd else c)
        return $ Arc this (rx, ry) rot (largearc == 1) (sweep == 1) : rest
      -- Close curve
      | cmd == "z" || cmd == "Z" = return $ either (\p -> [Line p]) (\p -> [Line p]) first
      | otherwise = error $ "Unknown command '" ++ cmd ++ "' found when parsing line."
    --  coordPair :: Floating a => State String (a, a)
    coordPair = do
      consumeWhile (`elem` " \t,")
      x <- consumeWhile (\x -> x == 'e' || not (x `elem` " \t," || isAlpha x))
      consumeWhile (`elem` " \t,")
      y <- consumeWhile (\x -> x == 'e' || not  (x `elem` " \t," || isAlpha x))
      return (read x, read y)
    command :: State String String
    command = do
      consumeWhile (`elem` " \t,")
      c <- consumeWhile isAlpha
      return $ c
    --  value :: Floating a => State String a
    value = do
      consumeWhile (`elem` " \t,")
      x <- consumeWhile (\x -> not $ x `elem` " \t," || isAlpha x)
      return $ read x
    skipNValue :: Int -> State String ()
    skipNValue n = replicateM_ n value

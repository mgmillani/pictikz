--  Copyright 2017-2020 Marcelo Garlet Milani
--  This file is part of pictikz.

--  pictikz is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  pictikz is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with pictikz.  If not, see <http://www.gnu.org/licenses/>.

module Pictikz.SVG.Input.Process where

import Pictikz.Elements hiding (CurveSegment(..))
import Pictikz.SVG.Input.Configuration
import Pictikz.Utils
import qualified Pictikz.Elements  as E (CurveSegment(..))
import qualified Pictikz.Geometry as Ge

import Control.Monad
import Control.Monad.Trans.State
import Data.Char
import Data.DescriLo as De
import Data.Either
import Data.List
import Data.Matrix hiding (trace)
import Data.Maybe
import Geom2D.CubicBezier.Approximate
import qualified Data.Array.IArray   as A
import qualified Data.Array.Unboxed  as U
import qualified Data.Map            as M
import qualified Data.Set            as S
import qualified Data.Vector.Unboxed as V
import qualified Geom2D.CubicBezier  as B
import Text.Read (readMaybe, readEither)

import qualified Debug.Trace as D (trace)

textAsNodes elements = fst $ textAsNodes' elements 1
  where
    textAsNodes' [] i = ([], i)
    textAsNodes' (e:es) i =
      case e of
        Object (Element (Id oid _) (Paragraph (Point (x, y)) ts) st atts) ->
          let (rs, n) = textAsNodes' es (i+1)
          in ((Object $ Element (Id oid ("text-" ++ show i)) (Shape (Point (x,y)) Ge.Point) st{label = Just [ts], labelPosition = Just [Around 0 0]} atts) : rs, n)
        Group elems ->
          let (rs, i')  = textAsNodes' es i
              (elems', n) = textAsNodes' elems i'
          in (Group elems' : rs, n)
        x ->
          let (rs, n) = textAsNodes' es i
          in (x : rs, n)

renameNodes elements = fst $ renameNodes' elements 1
  where
    renameNodes' [] i = ([], i)
    renameNodes' (e:es) i =
      case e of
        Object (Element (Id x _) drawing st atts) ->
          let (rs, n) = renameNodes' es (i+1)
          in ((Object $ Element (Id x ("v" ++ show i)) drawing st atts) : rs, n)
        Group elems ->
          let (rs, i')  = renameNodes' es i
              (elems', n) = renameNodes' elems i'
          in (Group elems' : rs, n)


tableClosure t x
  | t U.! x == x = []
  | otherwise =
    let ys = tableClosure t (t U.! x)
        (_, y) = head ys
    in if null ys then [(x, t U.! x)] else (x, y):ys
updTable t [] = t
updTable t (x:xs) = 
  let ys = tableClosure t x
      y = head ys
  in if null $ drop 1 ys then updTable t xs else updTable (t U.// ys) xs

fixId :: U.Array Int Int -> [Element a] -> [Element a]
fixId table es = map updateElement (filter (\e -> let Id i _ = getId e in table U.! i == i) es)
  where
    (n0,nn) = U.bounds table
    keptIds = [i | e <- es, let Id i _ = getId e, table U.! i == i]
    deletedIds = filter (\i -> table U.! i /= i) $ U.indices table
    newIds = zip keptIds [0..]
    nn' = (length newIds) - 1
    newIdsTable = U.array (n0,nn) newIds :: U.Array Int Int
    table' = updTable table [n0..nn]
    table'' = table' U.// [(i, newIdsTable U.! (table' U.! i)) | i <- keptIds ++ deletedIds ]
    updateId (Id x str) = (Id (table'' U.! x) str)
    updateCoordinate (ElementRef x) = ElementRef (table'' U.! x)
    updateCoordinate c = c
    updateElement (Element   eid d   st att) = Element (updateId eid) (updateDrawing d) st att
    updateElement (Container eid els st att) = Container (updateId eid) (map (table'' U.!) (filter (\i -> table U.! i == i ) els)) st att
    updateDrawing d = case d of
      Shape p se -> Shape (updateCoordinate p) se
      PolyLine cs -> PolyLine $ map updateCoordinate cs
      Curve cs ss -> Curve (map updateCoordinate cs) ss
      Paragraph p t -> Paragraph (updateCoordinate p) t

normalizeCoordinates es = 
  let els = concatMap getBasicElements es
      coords = filter isPoint $ concatMap
        (\e -> case e of
          Element _ (PolyLine cs) _ _ -> cs
          Element _ (Curve ps _) _ _ -> ps
          Element _ d _ _ -> [getPos d]
          Container _ _ _ _ -> []) els
      maxX = maximum $ map (\(Point (x,y)) -> x) coords
      minX = minimum $ map (\(Point (x,y)) -> x) coords
      maxY = maximum $ map (\(Point (x,y)) -> (-y)) coords -- Y-axis is flipped in SVG files
      minY = minimum $ map (\(Point (x,y)) -> (-y)) coords
      w = maxX - minX
      h = maxY - minY
      d' = if null coords then 1 else min w h
      d = if d' == 0 then 1 else d'
      fitPoint (x,y) = ((x - minX) / d, (-minY - y) / d)
      fitCoord (Point (x,y)) = Point $ fitPoint (x,y)
      fitCoord c = c
      fitShape (Ge.Rectangle w h) = Ge.Rectangle (w/d) (h/d)
      fitShape (Ge.Ellipse rx ry) = Ge.Ellipse (rx/d) (ry/d)
      fitShape p = p
      fitObject (Object e) = 
        let Element eid dr st att = fPos fitCoord e
        in Object $ Element
             eid
             (case dr of 
               Shape (Point (x,y)) s@(Ge.Rectangle w h) -> 
                let s'@(Ge.Rectangle w' h') = fitShape s
                in Shape (Point (x, y - h')) s'
               Shape p s -> Shape p (fitShape s)
               Curve ps cs -> Curve ps (normalizeSegments fitPoint ps cs)
               _ -> dr)
             st{ thickness   = fmap (/d) $ thickness st
               , dashPattern = fmap (map (/d)) $ dashPattern st
               }
             att
      fitObject (Group els) = Group $ map fitObject els
        
  in ((minX, minY), w/d, h/d, d, map fitObject es)

normalizeSegments fitPoint (Point (x0,y0):Point (x3,y3):ps) (E.Cubic (x1,y1) (x2,y2):cs) =
  E.Cubic (Ge.polarDistance (x0,y0) (fitPoint (x1,y1)))
        (Ge.polarDistance (x3,y3) (fitPoint (x2,y2)))
  : normalizeSegments fitPoint (Point (x3,y3) : ps) cs
normalizeSegments fitPoint (p:ps) (c:cs) = c : normalizeSegments fitPoint ps cs
normalizeSegments _ _ [] = []

getPointCoord universe p =
  case p of
    (ElementRef x) -> getPointCoord universe $ getPos $ universe M.! x
    (Point (x,y)) -> (x,y)

polarDistanceToSegment q0 q1 q2 universe =
  let p0 = getPointCoord universe q0
      p1 = getPointCoord universe q1
      p2 = getPointCoord universe q2
  in Ge.polarDistanceToSegment p0 p1 p2

attachCoordinate t universe (ElementRef x) = ElementRef x
attachCoordinate t universe (Point (x,y)) = 
  let neighbors = [(i, (r, phi))
                  | e <- M.elems universe
                  , let (Element (Id i str) d st att) = e
                  , let (Shape pos se) = d
                  , let (x0,y0) = getPointCoord universe pos
                  , let (r, phi) = Ge.polarDistanceToShape (x,y) (x0, y0) se
                  , isShape d && r < t
                  ]
      (closestI, (r,phi)) = minimumBy (\a b -> compare (fst $ snd a) (fst $ snd b)) neighbors
  in if null neighbors then Point (x,y) else (ElementRef closestI)

attachParagraph t (Element (Id pid str) (Paragraph pos txt) st att) universe =
  let threshold = t 
      pos' = getPointCoord universe pos
      neighbors =  [ (e', proximity)
                   | e <- M.elems universe
                   , let Element eid d st1 att1 = e
                   , let (e', proximity) =
                            case d of
                              Shape pe se -> 
                                let (r,phi) = Ge.polarDistanceToShape pos' (getPointCoord universe pe) se
                                    (Ge.Ellipse rb _) = Ge.boundingCircle se
                                    --r' = if rb == 0 then 0 else r / rb
                                    -- r' = r - rb
                                in (Element eid d
                                            st1{label = Just [txt]
                                               , labelPosition = Just $ [Around ((r + rb)/rb) phi]}
                                            att, r)
                              PolyLine cs -> 
                                let segmentDists =
                                      zipWith (\p1 (p2, i) ->
                                                (i, polarDistanceToSegment pos p1 p2 universe))
                                              cs (zip (tail cs) [0..])
                                    (closestI, (r, phi)) =
                                      minimumBy (\a b -> compare (fst $ snd a) (fst $ snd b))
                                                segmentDists
                                    e' = Element eid d
                                                 st1{label = Just [txt]
                                                    , labelPosition = Just $ [OnSegment closestI phi]}
                                                 att1
                                in (e', r)
                              _ -> (e, threshold + 1)
                   , proximity <= threshold
                   ]
      (closest, _) = minimumBy (\a b -> compare (snd a) (snd b)) neighbors
  in if null neighbors then Nothing
     else Just closest

-- | Attach text and lines to other elements if they are close enough.
attachElements t es = es'
  where
    es' = evalState (attachElements' es S.empty) (M.fromList [ (i, e) | e <- concatMap getBasicElements es, let Element (Id i str) d st att = e])
    threshold = t
    attachElements' [] internal = do
      universe <- get
      return $ [Object e | i <- S.toList internal, let e = universe M.! i]
    attachElements' (Group els:es) internal = do
      g1 <- attachElements' els S.empty
      gs <- attachElements' es internal
      return $ (Group g1) : gs
    attachElements' (Object (Element (Id x str) d st att):es) internal = do
      universe <- get
      let internal' = S.insert x internal
      case d of
        Shape p s     -> attachElements' es internal'
        PolyLine cs   -> do 
          universe <- get
          put $ M.insert x (Element (Id x str) (PolyLine $ map (attachCoordinate threshold universe) cs) st att) universe
          attachElements' es internal'
        Curve cs ss  -> do 
          universe <- get
          put $ M.insert x
            (Element (Id x str)
              (Curve
                (map (attachCoordinate threshold universe) cs)
                ss
              )
              st att) universe
          attachElements' es internal'
        Paragraph p txt -> do
          universe <- get
          case attachParagraph t (Element (Id x str) d st att) universe of
            Just (Element (Id x' str') d' st' att') -> do
              put $ M.insert x' (Element (Id x' str') d' st' att') universe
              attachElements' es internal
            Nothing -> attachElements' es internal'
            
simplifyLines refSize minLen maxAngle els = {-D.trace (show (els, refSize, minLen')) -}els'
  where
    fixLen (PolyLine cs) = restrictLength minLen' (PolyLine cs)
    fixLen s = s
    fixAngle (PolyLine cs) = PolyLine $ smoothAngle maxCos cs
    fixAngle s = s
    changeShape f (Object (Element eid d st att)) = Object $ Element eid (f d) st att
    changeShape f (Group objs) = Group $ map (changeShape f) objs
    polyLineF = changeShape $
                   (if isNothing maxAngle then id else fixAngle)
                 . (if isNothing minLen then id else fixLen)
    curveF = changeShape (simplifyCurve $ refSize)
    objF (Group objs) = Group $ map objF objs
    objF o@(Object (Element _ (PolyLine _) _ _)) = polyLineF o
    objF o@(Object (Element _ (Curve _ _) _ _)) = curveF o
    objF o@(Object _) = o
    els' = map objF els
    minLen' = (fromJust minLen) * refSize
    maxCos = cos $ pi - (fromJust maxAngle)

simplifyCurve eps (Curve ps ss) = Curve ps' ss'
  where
    (ps', ss') = simplifyCurve' ps ss
    simplifyCurve' ps [] = (ps, [])
    simplifyCurve' (p:ps) (a@(E.Arc _ _ _) : ss) = (p:psr, a:ssr)
      where
        (psr, ssr) = simplifyCurve' ps ss
    simplifyCurve' ps ss = (ps1 : psr, ss1 : ssr)
      where
        ((ps1, ss1), (ps2, ss2), _) = maximumBy (\(_,_,na) (_,_,nb) -> compare na nb) $ 
          [ simplifyAsBezier 0 eps ps ss
          , simplifyAsLine 0 eps ps ss
          ]
        (psr, ssr) = simplifyCurve' ps2 ss2

simplifyAsLine n eps (Point p0:Point p3:ps) (c1@(E.Cubic p1 p2):cs)
  | err < eps = simplifyAsLine (n+1) eps (Point p0:Point p3:ps) (E.Line:cs)
  | otherwise = ((Point p0, c1), (Point p3:ps, cs), n)
  where
    q1 = evalSegment p0 p3 c1 0.25 
    q2 = evalSegment p0 p3 c1 0.75 
    err = Ge.averageSquareDistanceToLine p0 p3 [q1,q2]
simplifyAsLine n eps (Point p0:Point p3:Point p6:ps) (c1:c2:cs)
  | err < eps = simplifyAsLine (n+1) eps (Point p0:Point p6:ps) (E.Line:cs)
  | otherwise = ((Point p0, c1), (Point p3:Point p6:ps, c2:cs), n)
  where
    q1 = evalSegment p0 p3 c1 0.25 
    q2 = evalSegment p0 p3 c1 0.75 
    q3 = p3
    q4 = evalSegment p3 p6 c2 0.25 
    q5 = evalSegment p3 p6 c2 0.75 
    err = Ge.averageSquareDistanceToLine p0 p6 [q1,q2,q3,q4,q5]
simplifyAsLine n _ ps cs = ((head ps, head cs), (tail ps, tail cs), n)

simplifyAsBezier n eps (Point p0:Point p3:Point p6:ps) (c1:c2:cs)
  -- 5 is the number of samples for fitting
  | err / 5 < eps = simplifyAsBezier (n+1) eps (Point p0:Point p6:ps) (E.Cubic p1' p4':cs)
  | otherwise = ((Point p0, c1), (Point p3:Point p6:ps, c2:cs), n)
  where
    -- best bezier fit
    point = uncurry B.Point
    s1 = joinSegmentsAsCubic p0 c1 p3 c2 p6
    q1 = point $ evalSegment p0 p3 c1 0.25 
    q2 = point $ evalSegment p0 p3 c1 0.75 
    q3 = point p3
    q4 = point $ evalSegment p3 p6 c2 0.25 
    q5 = point $ evalSegment p3 p6 c2 0.75 
    (s2, err) = approximateCubic s1 (V.fromList [q1,q2,q3,q4,q5]) Nothing 10
    B.Point x1 y1 = B.cubicC1 s2
    B.Point x2 y2 = B.cubicC2 s2
    p1' = (x1, y1)
    p4' = (x2, y2)
simplifyAsBezier n _ ps cs = ((head ps, head cs), (tail ps, tail cs), n)

joinSegmentsAsCubic p0 c1 p3 c2 p6 = 
  B.CubicBezier
    { B.cubicC0 = point p0
    , B.cubicC1 = point p1
    , B.cubicC2 = point p5
    , B.cubicC3 = point p6
    }
  where
    point = uncurry B.Point
    p1 = case c1 of
      E.Line -> (p0 + p3) / (2,2)
      E.Cubic q1 _ -> q1
    p5 = case c2 of
      E.Line -> (p3 + p6) / (2,2)
      E.Cubic _ q2 -> q2

smoothAngle maxCos (p0:p1:p2:ps) = 
  if angleCos > maxCos then smoothAngle maxCos (p0:p2:ps)
  else p0 : smoothAngle maxCos (p1:p2:ps)
  where
    Point (x0, y0) = p0
    Point (x1, y1) = p1
    Point (x2, y2) = p2
    dx1 = x1 - x0
    dy1 = y1 - y0
    dx2 = x2 - x1
    dy2 = y2 - y1
    l1 = sqrt $ dx1 * dx1 + dy1 * dy1
    l2 = sqrt $ dx2 * dx2 + dy2 * dy2
    ux1 = dx1 / l1
    uy1 = dy1 / l1
    ux2 = dx2 / l2
    uy2 = dy2 / l2
    angleCos = ux1*ux2 + uy1*uy2
smoothAngle _ ps = ps

restrictLength minLen (PolyLine ps) = PolyLine $ restrictLength' ps
  where
    minSquareLen = minLen * minLen
    joinSegments p0 (p1:p2:ps) = {-D.trace (show (minSquareLen, l1,l2,l3, p0, p1, p2, t)) $ -}
      if      l1 >= minSquareLen then (p1, p2:ps)
      else if l2 < minSquareLen then joinSegments p0 (p2:ps)
      else result
      where
        Point (x0, y0) = p0
        Point (x1, y1) = p1
        Point (x2, y2) = p2
        dx1 = x1 - x0
        dy1 = y1 - y0
        dx2 = x2 - x0
        dy2 = y2 - y0
        dx21 = x2 - x1
        dy21 = y2 - y1
        l1 = dx1*dx1 + dy1*dy1
        l2 = dx2*dx2 + dy2*dy2
        l3 = sqrt $ dx21 * dx21 + dy21 * dy21 -- distance between p1 and p2
        ux = dx21 / l3 -- unit vector from p1 to p2
        uy = dy21 / l3
        -- point p' will be given by (t*ux + x1, t*uy + y1)
        -- we search for a t such that at^2 + bt + c = 0
        a = 1 -- (ux*ux + uy*uy)
        b = 2 * (dx1*ux + dy1*uy)
        c = l1 - minSquareLen
        t = (-b + (sqrt $ b*b - 4*a*c)) / 2*a
        x' = t * ux + x1
        y' = t * uy + y1
        p' = Point (x', y')
        dx' = x2 - x'
        dy' = y2 - y'
        l' = dx'*dx' * dy'*dy'
        result =
          if null ps then
            if l' >= minSquareLen then (p', p2:ps)
            else (p2, ps)
          else (p', p2:ps)
    joinSegments p0 (p1:ps) = (p1, ps)
    restrictLength' (p0:p1:ps) = p0 : restrictLength' (p':ps')
      where
        (p', ps') = joinSegments p0 (p1:ps)
    restrictLength' ps = ps

preprocess conf refSize elements
  | confTextAsNodes conf == Just True = preprocess conf{confTextAsNodes = Just False} refSize $ textAsNodes elements
  | confRenameNodes conf == Just True = preprocess conf{confRenameNodes = Just False} refSize $ renameNodes elements
  | otherwise = simplifyLines refSize (confLineMinLength conf) (confLineMaxAngle conf) elements

equalCoord t (ElementRef x) (ElementRef y) = x == y
equalCoord t (Point (x0,y0)) (Point (x1,y1)) = almostEqual t x0 x1 && almostEqual t y0 y1
equalCoord t _ _ = False

getBasicElements (Group els) = concatMap getBasicElements els
getBasicElements (Object e) = [e]


joinGroups ((Group els0), universe, t, table) (Group els1') = 
  (Group (map Object els1), newEls0 ++ universe, if t == Nothing then Just 1 else fmap (+1) t, table')
  where
      els1 = concatMap getBasicElements els1'
      elsPairing = [ (Element eid d st{time = Just (beginE, t)} att, sim)
                   | Object (Element eid d st att) <- els0
                   , let (beginE, endE) = fromMaybe (Nothing, Nothing) $ time st
                   , let sim = similarElement (Element eid d st att) els1
                   ]
      newEls0 = [e | (e,se) <- elsPairing, isNothing se]
      table' = table A.// [ (i, i')
                          | (e, se) <- elsPairing
                          , let (Element (Id i str0) d0 st0 att0) = e
                          , let Just (Element (Id i' str1) d1 st1 att1) = se
                          , isJust se
                          ] :: U.Array Int Int
      similarElement e0 [] = Nothing
      similarElement e0 (e1:es) = 
        let (Element eid0 d0 st0 att0) = e0
            (Element eid1 d1 st1 att1) = e1
        in if st0 /= st1 || att0 /= att1 then similarElement e0 es
           else case (d0,d1) of
                  (Shape p0 s0, Shape p1 s1) ->
                    case (s0, s1) of
                      (Ge.Ellipse rx0 ry0, Ge.Ellipse rx1 ry1) ->
                        if equalCoord 0.1 p0 p1 && almostEqual 0.1 rx0 rx1 && almostEqual 01 ry0 ry1 then Just e1
                        else similarElement e0 es
                      (Ge.Rectangle w0 h0, Ge.Rectangle w1 h1) ->
                        if equalCoord 0.1 p0 p1 && almostEqual 0.1 w0 w1 && almostEqual 0.1 h0 h1 then Just e1
                        else similarElement e0 es
                      (Ge.Point, Ge.Point) -> 
                        if equalCoord 0.1 p0 p1 then Just e1
                        else similarElement e0 es
                      _ -> similarElement e0 es
                  (PolyLine co0, PolyLine co1) -> if and $ zipWith (equalCoord 0.1) co0 co1 then Just e1 else similarElement e0 es
                  (Paragraph (Point (x0, y0)) t0, Paragraph (Point (x1, y1)) t1) ->
                    if t0 == t1 && almostEqual 0.1 x0 x1 && almostEqual 0.1 y0 y1 then Just e1 else similarElement e0 es
                  _ -> similarElement e0 es

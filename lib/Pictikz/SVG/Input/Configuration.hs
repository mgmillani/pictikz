module Pictikz.SVG.Input.Configuration where

import Pictikz.Utils
import Pictikz.Parser
import Pictikz.Elements
import Data.Char
import Data.Maybe
import Text.Read (readMaybe, readEither)
import Control.Monad

import Data.DescriLo as De

data Object a = Object (Element a) | Group [Object a] deriving (Eq, Show)

isGroup (Group _) = True
isGroup _ = False
isObject (Object _) = True
isObject _ = False

data RefSize = RefSize
  { rfShapeSize :: Double
  , rfNumShapes :: Int
  , rfThickness :: Double
  , rfNumThickness :: Int
  } deriving (Eq, Show)

updateRefThickness refSize t =
  let n = rfNumThickness refSize
      t' = rfThickness refSize
  in refSize
  { rfNumThickness = 1 + n
  , rfThickness = (t + t'*(fromIntegral $ n+1)) / (fromIntegral $ 1 + n)}
updateRefSize refSize s = 
  let n = rfNumShapes refSize
      s' = rfShapeSize refSize
  in refSize{rfNumShapes = n+1, rfShapeSize = ((fromIntegral n)*s' + s) / (fromIntegral $ n + 1)}

noRefSize = RefSize{rfShapeSize = 0.0, rfNumShapes = 0, rfThickness = 0, rfNumThickness = 0}

data Configuration =
  Configuration
  { confTextAsNodes :: Maybe Bool
  , confStartTime   :: Maybe Int
  , confRenameNodes :: Maybe Bool
  , confColours     :: Maybe (Either [(Color, String)] FilePath)
  , confLineMinLength :: Maybe Double
  , confLineMaxAngle :: Maybe Double
  } deriving (Show, Eq)

defaultConfiguration =
  Configuration
  { confTextAsNodes = Just False
  , confStartTime   = Nothing
  , confRenameNodes = Just True
  , confColours     = Just $ Left defaultColors
  , confLineMinLength = Nothing
  , confLineMaxAngle = Nothing
  }

noConfiguration =
  Configuration
  { confTextAsNodes = Nothing
  , confStartTime   = Nothing
  , confRenameNodes = Nothing
  , confColours     = Nothing
  , confLineMinLength = Nothing
  , confLineMaxAngle = Nothing
  }

updateConfig old new =
  Configuration
  { confTextAsNodes   = combineMaybe (confTextAsNodes old)   (confTextAsNodes new)
  , confStartTime     = combineMaybe (confStartTime old)     (confStartTime new)
  , confRenameNodes   = combineMaybe (confRenameNodes old)   (confRenameNodes new)
  , confColours       = combineMaybe (confColours old)       (confColours new)
  , confLineMinLength = combineMaybe (confLineMinLength old) (confLineMinLength new)
  , confLineMaxAngle  = combineMaybe (confLineMaxAngle old)  (confLineMaxAngle new)
  }

parseConfig conf = foldM parseConfig' noConfiguration svgD
  where
    svgD = concatMap De.values $ filter (\c -> map toLower ( De.name c) == "svg input") conf
    parseConfig' cf (attr, value) =
      case map toLower attr of
        "text as nodes" -> case map toLower value of
          "true"  -> Right cf{confTextAsNodes = Just True}
          "false" -> Right cf{confTextAsNodes = Just False}
          x -> parseError attr x "true | false"
        "start time"   ->
          case readMaybe value :: Maybe Integer of
            Nothing -> parseError attr value "Integer"
            Just t -> Right $ cf{confStartTime = Just $ fromIntegral t}
        "rename nodes" -> case map toLower value of
          "true"  -> Right cf{confRenameNodes = Just True}
          "false" -> Right cf{confRenameNodes = Just False}
          x -> parseError attr x "true | false"
        "minimum line segment length" ->
          case readMaybe value :: Maybe Double of
            Nothing -> parseError attr value "Double"
            Just l -> Right $ cf{confLineMinLength = Just l}
        "maximum line internal angle" -> case words $ map toLower value of
          [degStr, "degrees"] -> do
            deg <- either (\x -> Left [x]) Right $ readEither degStr
            return $ cf{confLineMaxAngle = Just (deg * pi / 180)}
          [radStr, "radians"] -> do
            rad <- either (\x -> Left [x]) Right $ readEither radStr
            return $ cf{confLineMaxAngle = Just rad}
          x -> parseError attr value "<angle> degrees | <angle> radians"
        "colours" -> parseColours attr (words value) cf
        "colors"  -> parseColours attr (words value) cf
        x -> parseError "SVG Input" x "text as nodes | start time | rename nodes | colours | maximum line internal angle | minimum line segment length"

parseColours attr [] cf = parseError attr "" "file <name> | rgb <r> <g> <b> | hsv <h> <s> <v>."
parseColours attr (v:vs) cf
  | map toLower v == "file" && not (null vs) = Right cf{confColours = Just $ Right $ head vs}
  | otherwise = do
    colourList <- onParseError (attr ++ " = " ++ v ++ " [...]") $ parseColourList (v:vs)
    return $ cf{confColours = Just $ Left colourList}

parseColourList [] = return []
parseColourList cs =
  let (colourName, colourDef) = span (\w -> map toLower w `notElem` ["rgb", "hsl"]) cs
  in case colourDef of
    (code:c1:c2:c3:cs) -> case map toLower code of
      "hsl" -> do
        let parsing = concat [code,c1,c2,c3]
        h <- onParseError parsing $ readIntDoubleRange 0 360 c1
        s <- onParseError parsing $ readIntDoubleRange 0 100 c2
        l <- onParseError parsing $ readIntDoubleRange 0 100 c3
        rs <- parseColourList cs
        return $ (fromHSL (fromIntegral h) (fromIntegral s) (fromIntegral l), unwords colourName) : rs
      "rgb" -> do
        let parsing = concat [code,c1,c2,c3]
        r <- onParseError parsing $ readIntDoubleRange 0 255 c1
        g <- onParseError parsing $ readIntDoubleRange 0 255 c2
        b <- onParseError parsing $ readIntDoubleRange 0 255 c3
        rs <- parseColourList cs
        return $ (RGB (fromIntegral r) (fromIntegral g) (fromIntegral b), unwords colourName) : rs
      x -> parseError (unwords  colourName) code "rgb | hsl"
    _ -> parseError (unwords colourName) (unwords colourDef) "rgb <r> <g> <b> | hsl <h> <s> <l>"

mmFactor "in" = 25.4
mmFactor "cm" = 10
mmFactor "pt" = 2.834646
mmFactor "pc" = 0.2362205
mmFactor _    = 1

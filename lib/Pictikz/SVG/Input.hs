module Pictikz.SVG.Input
  (module Pictikz.SVG.Input.Configuration
  ,module Pictikz.SVG.Input.Parse
  ,module Pictikz.SVG.Input
  ,module Pictikz.SVG.Input.Process)
where

import Pictikz.SVG.Input.Configuration
import Pictikz.SVG.Input.Parse
import Pictikz.SVG.Input.Process

import           Pictikz.Elements

import qualified Data.Array.IArray  as A
import Data.Matrix hiding (trace)
import Control.Monad.Trans.State

import qualified Text.XML.Light as X

getObjID (Object o) = [getId o]
getObjID (Group g) = concatMap getObjID g

load svg conf =  
  Universe
  { uOrigin = o
  , uSize  = (w, h)
  , uScale = (s, s)
  , uAttributeScale = s
  , uElements =  
      A.array (0, (length elements2) - 1)
              [ (i, e)
              | e <- elements2
              , let Element (Id i _) _ _ _ = e]
  }
  where
    contents = X.parseXML svg
    conf' = updateConfig defaultConfiguration conf
    (elements', (n, refSize')) =  runState (mapM (parseElements conf' (identity 3)) contents) (0, noRefSize)
    refSize1 = if rfNumShapes refSize' > 0 then
                rfShapeSize refSize'
              else if rfNumThickness refSize' > 0 then
                rfThickness refSize'
              else 1
    (o, w, h, s, elements) = normalizeCoordinates $ preprocess conf' refSize1 $ concat $ elements'
    refSize2 = refSize1 / s
    dt = confStartTime conf'
    groups = concatMap (\g -> attachElements (1.0 * refSize2) [g]) $ filter isGroup elements
    group0 = attachElements (1.0 * refSize2) $ filter (not . isGroup) elements
    (Group elements1, universe, t, idTable) = foldl joinGroups
      ( Group group0, []
      , if null group0 then fmap (\x -> x - 1) dt else dt
      , A.array (0,n) $ zip [0..n] [0..n] :: A.Array Int Int
      )
      groups
    elements2 = fixId idTable $ (map (\(Object e) -> e) elements1) ++ universe



# Revision history for pictikz

## 2.7.0.0 -- 2020.08.29
* Added support for curves.
  * Both Bézier and elliptical arcs are supported.
  * Curves can be simplified into the most compact representation.
  * Clustering for curves.
## 2.6.2.0 -- 2020.08.23
* Fixed a bug when working with polar coordinates.
* Fixed a bug where Tikz output would always use italics and bold.
## 2.6.1.0 -- 2020.08.15
* Fixed a bug when parsing SVG files that would cause pictikz to crash.
## 2.6.0.0 -- 2020.07.08
* Fixed labeling of edges and node in DOT files.
* Fixed computation of the relative position of a node's label.
* Nodes can be translated so that the origin is a specific value.
## 2.5.1.0 -- 2020.06.29
* Fixed a bug when scaling attributes.
## 2.5.0.0 -- 2020.01.06
* Lines can now have several segments and labels over each segment.
* Support for smoothing hand-drawn lines.
* Allow for lines that do not connect vertices.
* Added support for Dot input.
* Clustering no longer depends on dimensions of bounding box, but on size of smallest node.
* Refactored internal structures.
## 2.1.2.0 -- 2019.10.26
* Improved sliding window algorithm for clustering nodes.
* Flip y axis after reading SVG file instead of when making the graph.
## 2.1.1.0 -- 2019.10.13
* Fixed computation of relative position of the label of a node.
## 2.1.0.0 -- 2019.10.03
* Position labels outside node as well.
## 2.0.0.0 -- 2019.07.13
* Added external configuration file.
* Ability to control several aspects of Tikz output, like arrow heads, node ids, thickness of lines, dash pattern...
* Floating points values are rounded before output.
* Added a script for interactive use of pictikz with inkscape.
* Polar coordinates.
* Scale, rotate and translate before and after changing coordinates.
## 1.5.1.0 -- 2018.09.19
* Fixed a bug where transform was being applied twice.
## 1.5.0.0 -- 2018.08.16
* Treat text as individual nodes (option --text-as-nodes).

## 1.4.0.0 -- 2018.08.03
* Organize vertices by setting a minimal distance (option --min-dist).
* Rename vertices.

## 1.3.0.0 -- 2018.07.13

* Proper support for text
  * Subscript, superscript, bold, italics, left alignment, right alignment, centered, and multiple lines.
## 1.2.3.1 -- 2018.01.28

* Fixed a bug where transformations were not being applied to text.

## 1.2.3.0 -- 2017.11.21

* Special case for inkscape layers.

## 1.2.2.0  -- 2017.11.08

* Added support for temporal graphs.

## 1.2.1.0  -- 2017.09.08

* Fixed a bug when using a grid with a single column or row.
* Fixed domain range for HSL.
* Replaced Float with Double.

## 1.2.0.0  -- 2017.09.03

* Fixed a bug with fitting coordinates into a box.
* Added colour support.

## 1.1.0.1  -- 2017.04.14

* Fixed a bug where -s was being treated as -f.

## 1.1.0.0  -- 2017.04.09

* Added style for nodes.

## 1.0.0.1  -- 2017.04.02

* Fixed small problem with the .cabal file.

## 1.0.0.0  -- 2017.04.02

* First version. Released on an unsuspecting world.

{ mkDerivation, array, base, containers, cubicbezier, descrilo
, directory, happy-dot, HUnit, lib, matrix, transformers, vector
, xml
}:
mkDerivation {
  pname = "pictikz";
  version = "2.7.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryHaskellDepends = [
    array base containers cubicbezier descrilo directory happy-dot
    matrix transformers vector xml
  ];
  executableHaskellDepends = [
    array base containers descrilo directory happy-dot matrix
    transformers xml
  ];
  testHaskellDepends = [
    array base containers descrilo directory happy-dot HUnit matrix
    transformers xml
  ];
  doHaddock = false;
  description = "Converts a svg image to tikz code";
  license = lib.licenses.gpl3Only;
}

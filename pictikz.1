.\" Manpage for pictikz.
.\" Contact marcelogmillani@gmail.com to correct errors or typos.
.TH pictikz 1 "2020.08.29" "2.7.0.0" "pictikz man page"
.SH NAME
pictikz \- a tool for converting SVG images into tikz code.
.SH Synopsis
.IP
.nf
\f[C]
pictikz [OPTIONS...] FILE
\f[R]
.fi
.SH Description
.PP
Tikz is often used to draw graphs (i.e., networks) in LaTeX.
Even though the resulting image can be very clean, manually writing tikz
code can be time consuming, especially when the picture needs to be
modified afterwards.
.PP
On the other side of the spectrum, drawing with graphical tools like
inkscape is easy, but getting a clean-looking result can be complicated.
.PP
With pictikz you get the best of both worlds: You draw using a graphical
tool and pictikz converts the resulting SVG file to tikz code, making
some automatic style adjustments if you desire.
.SH Features
.IP \[bu] 2
Directed and undirected graphs.
.IP \[bu] 2
Coloured, dashed, dotted and bold vertices and edges.
.IP \[bu] 2
Temporal graphs (for beamer).
.IP \[bu] 2
Labels for vertices.
.SH Options
.PP
\f[B]\f[CB]-c, --colours FILE\f[B]\f[R]
.PP
Load colour definitions from FILE.
See \f[I]Colours\f[R] section below.
.PP
\f[B]\f[CB]--config FILE\f[B]\f[R]
.PP
Load configuration form FILE.
.PP
\f[B]\f[CB]-f, --fit WIDTH HEIGHT\f[B]\f[R]
.PP
Fit coordinates into a box of size WIDTH x HEIGHT without changing
aspect ratio.
.PP
\f[B]\f[CB]-g, --grid [PERCENT]\f[B]\f[R]
.PP
Fit coordinates into a grid (implies \[en]uniform [PERCENT]).
By default PERCENT = 20.
.PP
\f[B]\f[CB]-h, --help\f[B]\f[R]
.PP
Show help.
.PP
\f[B]\f[CB]--latex-colours\f[B]\f[R]
.PP
Output colours in LaTeX.
.PP
\f[B]\f[CB]--min-dist X Y\f[B]\f[R]
.PP
Scale coordinates such that the minimum distance in the x-axis is X and
the minimum distance in the y-axis is Y.
.PP
\f[B]\f[CB]-o, --output FILE\f[B]\f[R]
.PP
Write output into FILE instead of stdout.
.PP
\f[B]\f[CB]--rename\f[B]\f[R]
.PP
Rename vertices to v1, v2,\&... (default)
.PP
\f[B]\f[CB]--no-rename\f[B]\f[R]
.PP
Do not rename vertices, use the same IDs as in the SVG file.
.PP
\f[B]\f[CB]-s, --scale WIDTH HEIGHT\f[B]\f[R]
.PP
Scale coordinates into a box of size WIDTH x HEIGHT.
.PP
\f[B]\f[CB]--standalone\f[B]\f[R]
.PP
Output standalone Tikz, ready to compile.
.PP
\f[B]\f[CB]-t, --temporal [START]\f[B]\f[R]
.PP
Treat SVG layers as frames, using overlay specifications in the output.
.PP
\f[B]\f[CB]--text-as-nodes\f[B]\f[R]
.PP
Treat text as individual nodes instead of labels.
.PP
\f[B]\f[CB]-u, --uniform [PERCENT]\f[B]\f[R]
.PP
Group coordinates by distance.
Maximum distance for grouping is PERCENT of the axis in question.
.PP
\f[B]\f[CB]-v, --version\f[B]\f[R]
.PP
Output version and exit
.SH SVG Conversion
.PP
The SVG standard uses the concept of shapes in its definition.
That is, there are circles, rectangles, lines and so on instead of
simply pixels like bitmap formats.
This allows a program like \f[C]pictikz\f[R] to infer what a user meant
and automatically produce a prettier output.
.PP
\f[C]pictikz\f[R] allows for customization of individual styles.
You do not need to precisely control thickness or appearance of dashes
in the SVG file, for example, , as this properties can be converted by
\f[C]pictikz\f[R] to match the desired style.
.SS Nodes
.PP
Pictikz differentiate between ellipsis (or circles) and rectangles,
allowing for different output in each case.
.PP
Text objects in SVG can become labels for nodes or nodes themselves,
depending on the configuration.
For each text object, the nearest node is taken.
.PP
Stroke and fill colours are also recognized.
.SS Edges
.PP
A path is converted to an edge from its initial point to its
destination.
Intermediary points of the path are irrelevant.
The nearest nodes are used as endpoints of the edge.
.PP
If the marker-end attribute of an edge is set, it is converted to be an
\f[I]arrow to\f[R] (see configuration options).
The attribute marker-start produces an \f[I]arrow from\f[R] instead, and
both together produce \f[I]arrow both\f[R].
The exact appearance of arrows can be configured depending on the output
format.
.PP
Stroke styling like thickness and dash patterns is also considered and
can be adjusted in the configuration.
.SS Frames
.PP
For temporal graphs (also known as multi-layer graphs), SVG groups are
used.
Groups with an \f[C]id\f[R] starting with \f[C]layer\f[R] are treated as
frames.
The order of the frames is the same as in the file.
Vertices which are close to each other in subsequent layers and are
equal will be merged into one vertex.
The same is valid for edges.
.SH Colours
.PP
Colours can be specified in a file where each line is in one of the
following formats
.IP
.nf
\f[C]
<NAME> RGB [0 - 255] [0 - 255] [0 - 255]
<NAME> RGB #RRGGBB
<NAME> RGB #RGB
<NAME> HSL [0 - 359] [0 - 100] [0 - 100]
\f[R]
.fi
.PP
Values can also be specified as floats between 0 and 1.
In this case, they are interpreted as a percentage of their range.
.SH Configuration File
.PP
Besides command-line options, it is possible to specify a configuration
file with \f[C]pictikz --config FILE\f[R].
This file follows a INI-like format.
Sections are written as \f[C][Section name]\f[R] and attributes as
\f[C]attribute = value\f[R].
The possible sections and their respective attributes are described
below.
.PP
Files are searched for in the following locations:
.IP "1." 3
Current directory.
.IP "2." 3
$XDG_CONFIG_HOME/pictikz
.IP "3." 3
$HOME/.pictikz
.IP "4." 3
/usr/share/pictikz
.SS SVG Input
.PP
How an SVG file is parsed.
Possible attributes are
.PP
\f[B]\f[CB]text as nodes = true | false\f[B]\f[R]
.PP
Create extra nodes for text.
.PP
\f[B]\f[CB]rename nodes = true | false\f[B]\f[R]
.PP
Rename nodes to v1, v2\&... instead of using the names in the input
file.
.PP
\f[B]\f[CB]colours = none | file <file name> | [<colour name> <rgb | hsv>  <r|h> <g|s> <b|v> ...]\f[B]\f[R]
.PP
Define a list of named colours.
Colours in the input file will be named according to this list.
.PP
\f[B]\f[CB]start time = <time>\f[B]\f[R]
.PP
Treat first layer as layer \f[C]<time>\f[R].
.SS Organizer
.PP
How nodes are manipulated in order to produce a nicer output.
Possible attributes are
.PP
\f[B]\f[CB]pre scale = none | plain <x> <y> | min dist <x> <y> | fit <w> <h>\f[B]\f[R]
.PP
Scale positions before changing coordinate system.
.IP \[bu] 2
\f[C]plain\f[R] simply scales each axis;
.IP \[bu] 2
\f[C]min dist\f[R] scales each axis in such a way that the minimum
distance in the x-axis is \f[C]<x>\f[R] and the minimum distance in the
y-axis is \f[C]<y>\f[R];
.IP \[bu] 2
\f[C]fit\f[R] maintains aspect ratio in order to fit all positions into
a box with the given dimensions.
.PP
\f[B]\f[CB]post scale = plain <x> <y> | min dist <x> <y> | fit <w> <h>\f[B]\f[R]
.PP
Same as \f[C]pre scale\f[R], but after changing coordinate system.
The meaning of and may change depending on the coordinate system used.
.PP
\f[B]\f[CB]rotate = <angle> degrees | <angle> radians\f[B]\f[R]
.PP
Rotate positions around the origin.
This is always applied with cartesian coordinates and before changing
the coordinate system.
.PP
\f[B]\f[CB]pre translate = <x> <y>\f[B]\f[R]
.PP
Translate each axis by the given amount.
Applied before changing coordinate system.
.PP
\f[B]\f[CB]post translate = <x> <y>\f[B]\f[R]
.PP
Translate each axis by the given amount.
Applied after changing coordinate system.
.PP
\f[B]\f[CB]node grouping = none | regular <percent> | cluster <percent>\f[B]\f[R]
.PP
Group nodes which have similar coordinates.
\f[C]<percent>\f[R] describes the maximum distance for grouping as a
percentage of the dimensions of the bounding box.
.IP \[bu] 2
\f[C]cluster\f[R] simply groups nearby points.
.IP \[bu] 2
\f[C]regular\f[R] groups nearby points and shift them in such a way that
all distances become multiples of some unit.
.PP
\f[B]\f[CB]coordinate systems = cartesian | polar\f[B]\f[R]
.PP
Convert coordinate system before grouping.
Note that transformations after changing coordinate system will just use
the first and second coordinates, without knowing what they mean.
.IP \[bu] 2
\f[C]cartesian\f[R] is the usual (x,y) coordinate system.
.IP \[bu] 2
\f[C]polar\f[R] converts (x,y) to (radius, angle).
.PP
\f[B]\f[CB]thickness scale = range <min> <max> | clip <min> <max> | min dist <d>\f[B]\f[R]
.PP
Scale thickness values
.IP \[bu] 2
\f[C]range\f[R] fits all values between and .
.IP \[bu] 2
\f[C]clip\f[R] sets values smaller than to and those greater than to .
.IP \[bu] 2
\f[C]min dist\f[R] scales values such that the minimum difference
between two values is .
This is always done after grouping.
.PP
\f[B]\f[CB]thickness grouping = none | cluster <percent> | regular <percent> | fixed <name> <value> [<name> <value>]\f[B]\f[R]
.IP \[bu] 2
\f[C]cluster\f[R] simply groups nearby values.
.IP \[bu] 2
\f[C]regular\f[R] groups values in an evenly-distributed way.
.IP \[bu] 2
\f[C]fixed\f[R] replaces values with the nearest named one in the list
of predefined values.
.PP
\f[B]\f[CB]dashed scale = range <min> <max> | clip <min> <max> | min dist <d>\f[B]\f[R]
.PP
See thickness scale.
.PP
\f[B]\f[CB]dashed grouping = none | cluster <percent> | regular <percent> | fixed <name> <value> [<name> <value>]\f[B]\f[R]
.PP
Similar to \f[C]thickness grouping\f[R], but fixed values are given as a
list of strokes and gaps, e.g.\ \f[C]fixed dotted 3 10\f[R].
## TikZ Output
.PP
How the TikZ output is written.
Possible attributes are
.PP
**\f[C]standalone = true | false**\f[R]
.PP
Produce a standalone TeX file which can be directly compiled.
If false, only a TikZ picture is generated.
.PP
\f[B]\f[CB]beamer = true | false\f[B]\f[R]
.PP
Use overlays which work with the beamer document class.
.PP
\f[B]\f[CB]label position = left | right | above | below | smart <percent>\f[B]\f[R]
.PP
Where to position the label of a node.
Smart guesses the direction based on text position, and puts the text
inside the node if the distance is smaller than \f[C]<percent>\f[R] of
the smallest axis.
.PP
\f[B]\f[CB]shape circle = <attribute>\f[B]\f[R]
.PP
Add to circular nodes.
.PP
\f[B]\f[CB]shape rectangle = <attribute>\f[B]\f[R]
.PP
Add to rectangular nodes.
.PP
\f[B]\f[CB]arrow to = <attribute>\f[B]\f[R]
.PP
Add to arrows going from the first node to the second.
\f[B]\f[CB]arrow from = <attribute>\f[B]\f[R]
.PP
Add the following attribute to arrows going from the second node to the
first.
\f[B]\f[CB]arrow both = <attribute>\f[B]\f[R]
.PP
Add the following attribute to arrows going in both directions.
\f[B]\f[CB]edge = <attribute>\f[B]\f[R]
.PP
Add the following attribute to undirected edges.
.PP
\f[B]\f[CB]rename nodes = no | by order | by label\f[B]\f[R]
.PP
Rename nodes before output.
- \f[C]by order\f[R] assigns indices based on the order on the input.
- \f[C]by label\f[R] sorts nodes by label before assigning indices.
.PP
\f[B]\f[CB]node name = <pattern>\f[B]\f[R]
.PP
Use when renaming nodes.
Replaces \f[C]$n\f[R] by the index of the node.
.SH Tools
.PP
The repository contains a script called \f[C]pictikz-interactive\f[R]
which allows you to draw a picture in inkscape and simultaneously see
the tikz output as pdf.
.SH Bugs
.PP
No known bugs.
.SH Author
.PP
Marcelo Garlet Millani (marcelogmillani\[at]gmail.com)

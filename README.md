![On the left side there is a sketchy handdraw grid, while on the right side it looks perfect.](example.png?raw=true "Example of what pictikz can do.")

## Compiling

Install `ghc` and `cabal`. Then run

    cabal update
    cabal sandbox init
    cabal install --dependencies-only
    cabal build

This will generate the binary `picktiz` in `dist/build/pictikz/`.

# Synopsis

    pictikz [OPTIONS...] FILE

# Description

Tikz is often used to draw graphs (i.e., networks) in LaTeX. Even though the resulting image can be very clean, manually writing tikz code can be time consuming, especially when the picture needs to be modified afterwards.

On the other side of the spectrum, drawing with graphical tools like inkscape is easy, but getting a clean-looking result can be complicated.

With pictikz you get the best of both worlds: You draw using a graphical tool and pictikz converts the resulting SVG file to tikz code, making some automatic style adjustments if you desire.

# Features

  - Directed and undirected graphs.
  - Coloured, dashed, dotted and bold vertices and edges.
  - Temporal graphs (for beamer).
  - Labels for vertices.

# Options

  **`-c, --colours FILE`**

  Load colour definitions from FILE. See *Colours* section below.

  **`--config FILE`**

  Load configuration form FILE.

  **`-f, --fit WIDTH HEIGHT`**

  Fit coordinates into a box of size WIDTH x HEIGHT without changing aspect ratio.

  **`-g, --grid [PERCENT]`**

  Fit coordinates into a grid (implies --uniform [PERCENT]). By default PERCENT = 20.

  **`-h, --help`**

  Show help.

  **`--latex-colours`**

  Output colours in LaTeX.

  **`--min-dist X Y`**

  Scale coordinates such that the minimum distance in the x-axis is X and the minimum distance in the y-axis is Y.
  
  **`-o, --output FILE`**

  Write output into FILE instead of stdout.

  **`--rename`**

  Rename vertices to v1, v2,... (default)

  **`--no-rename`**

  Do not rename vertices, use the same IDs as in the SVG file.

  **`-s, --scale WIDTH HEIGHT`**

  Scale coordinates into a box of size WIDTH x HEIGHT.

  **`--standalone`**

  Output standalone Tikz, ready to compile.

  **`-t, --temporal [START]`**

  Treat SVG layers as frames, using overlay specifications in the output.

  **`   --text-as-nodes`**

  Treat text as individual nodes instead of labels.

  **`-u, --uniform [PERCENT]`**

  Group coordinates by distance. Maximum distance for grouping is PERCENT of the axis in question.

  **`-v, --version`**

  Output version and exit

# SVG Conversion

The SVG standard uses the concept of shapes in its definition. That is, there are circles, rectangles, lines and so on instead of simply pixels like bitmap formats.
This allows a program like `pictikz` to infer what a user meant and automatically produce a prettier output.

`pictikz` allows for customization of individual styles. You do not need to precisely control thickness or appearance of dashes in the SVG file, for example, , as this properties can be converted by `pictikz` to match the desired style.

## Nodes

  Pictikz differentiate between ellipsis (or circles) and rectangles, allowing for different output in each case.

  Text objects in SVG can become labels for nodes or nodes themselves, depending on the configuration. For each text object, the nearest node is taken.

  Stroke and fill colours are also recognized.

## Edges

  A path is converted to an edge from its initial point to its destination. Intermediary points of the path are irrelevant. The nearest nodes are used as endpoints of the edge.

  If the marker-end attribute of an edge is set, it is converted to be an *arrow to* (see configuration options). The attribute marker-start produces an *arrow from* instead, and both together produce *arrow both*. The exact appearance of arrows can be configured depending on the output format.

  Stroke styling like thickness and dash patterns is also considered and can be adjusted in the configuration.

## Frames

  For temporal graphs (also known as multi-layer graphs), SVG groups are used.
  Groups with an `id` starting with `layer` are treated as frames.
  The order of the frames is the same as in the file.
  Vertices which are close to each other in subsequent layers and are equal will be merged into one vertex.
  The same is valid for edges.

# Colours

Colours can be specified in a file where each line is in one of the following formats

    <NAME> RGB [0 - 255] [0 - 255] [0 - 255]
    <NAME> RGB #RRGGBB
    <NAME> RGB #RGB
    <NAME> HSL [0 - 359] [0 - 100] [0 - 100]

Values can also be specified as floats between 0 and 1. In this case, they are interpreted as a percentage of their range.

# Configuration File

Besides command-line options, it is possible to specify a configuration file with `pictikz --config FILE`.
This file follows a INI-like format. Sections are written as `[Section name]` and attributes as `attribute = value`.
The possible sections and their respective attributes are described below.

Files are searched for in the following locations:

  1. Current directory.
  2. $XDG_CONFIG_HOME/pictikz
  3. $HOME/.pictikz
  4. /usr/share/pictikz

## SVG Input

How an SVG file is parsed. Possible attributes are

  **`text as nodes = true | false`**

  Create extra nodes for text.

  **`rename nodes = true | false`**

  Rename nodes to v1, v2... instead of using the names in the input file.

  **`colours = none | file <file name> | [<colour name> <rgb | hsv>  <r|h> <g|s> <b|v> ...]`**
  
  Define a list of named colours.
  Colours in the input file will be named according to this list.

  **`start time = <time>`**
  
  Treat first layer as layer `<time>`.

## Organizer

How nodes are manipulated in order to produce a nicer output.
Possible attributes are

  **`pre scale = none | plain <x> <y> | min dist <x> <y> | fit <w> <h>`**

  Scale positions before changing coordinate system.

  - `plain` simply scales each axis;
  - `min dist` scales each axis in such a way that the minimum distance in the x-axis is `<x>` and the minimum distance in the y-axis is `<y>`;
  - `fit` maintains aspect ratio in order to fit all positions into a box with the given dimensions.
    
  **`post scale = plain <x> <y> | min dist <x> <y> | fit <w> <h>`**
  
  Same as `pre scale`, but after changing coordinate system.
  The meaning of <x> and <y> may change depending on the coordinate system used.
  
  **`rotate = <angle> degrees | <angle> radians`**
  
  Rotate positions around the origin. This is always applied with cartesian coordinates and before changing the coordinate system.
  
  **`pre translate = <x> <y>`**
  
  Translate each axis by the given amount.
  Applied before changing coordinate system.

  **`post translate = <x> <y>`**
  
  Translate each axis by the given amount.
  Applied after changing coordinate system.

  **`node grouping = none | regular <percent> | cluster <percent>`**
  
  Group nodes which have similar coordinates.
  `<percent>` describes the maximum distance for grouping as a percentage of the dimensions of the bounding box.

  - `cluster` simply groups nearby points.
  - `regular` groups nearby points and shift them in such a way that all distances become multiples of some unit.

  **`coordinate systems = cartesian | polar`**

  Convert coordinate system before grouping.
  Note that transformations after changing coordinate system will just use the first and second coordinates, without knowing what they mean.

  - `cartesian` is the usual (x,y) coordinate system.
  - `polar` converts (x,y) to (radius, angle).

  **`thickness scale = range <min> <max> | clip <min> <max> | min dist <d>`**

  Scale thickness values

  - `range` fits all values between <min> and <max>.
  - `clip` sets values smaller than <min> to <min> and those greater than <max> to <max>.
  - `min dist` scales values such that the minimum difference between two values is <d>. This is always done after grouping.

  **`thickness grouping = none | cluster <percent> | regular <percent> | fixed <name> <value> [<name> <value>]`**

  - `cluster` simply groups nearby values.
  - `regular` groups values in an evenly-distributed way.
  - `fixed` replaces values with the nearest named one in the list of predefined values.

  **`dashed scale = range <min> <max> | clip <min> <max> | min dist <d>`**

  See thickness scale.

  **`dashed grouping = none | cluster <percent> | regular <percent> | fixed <name> <value> [<name> <value>]`**

  Similar to `thickness grouping`, but fixed values are given as a list of strokes and gaps, e.g. `fixed dotted 3 10`.
## TikZ Output

How the TikZ output is written.
Possible attributes are

  **`standalone = true | false**`
  
  Produce a standalone TeX file which can be directly compiled.
  If false, only a TikZ picture is generated.

  **`beamer = true | false`**
  
  Use overlays which work with the beamer document class.

  **`label position = left | right | above | below | smart <percent>`**

  Where to position the label of a node. Smart guesses the direction based on text position, and puts the text inside the node if the distance is smaller than `<percent>` of the smallest axis.

  **`shape circle = <attribute>`**

  Add <attribute> to circular nodes.

  **`shape rectangle = <attribute>`**

  Add <attribute> to rectangular nodes.

  **`arrow to = <attribute>`**

  Add <attribute> to arrows going from the first node to the second.
  **`arrow from = <attribute>`**

  Add the following attribute to arrows going from the second node to the first.
  **`arrow both = <attribute>`**

  Add the following attribute to arrows going in both directions.
  **`edge = <attribute>`**

  Add the following attribute to undirected edges.

  **`rename nodes = no | by order | by label`**

  Rename nodes before output.
  - `by order` assigns indices based on the order on the input.
  - `by label` sorts nodes by label before assigning indices.

  **`node name = <pattern>`**

  Use <pattern> when renaming nodes. Replaces `$n` by the index of the node.

# Tools

The repository contains a script called `pictikz-interactive` which allows you to draw a picture in inkscape and simultaneously see the tikz output as pdf.

# Bugs

No known bugs.

# Author

Marcelo Garlet Millani (marcelogmillani@gmail.com)
